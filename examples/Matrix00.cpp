// This little sample program creates a randomly filled 6 x 5 Matrix, adds a column,
// sets all the bits in that new column and then extracts the coefficients for the characteristic
// polynomial of the resulting 6 x 6 matrix. After each step it prints the result to `std::cout`.

// tag::doc[]
#include <GF2/GF2.h>
int
main()
{
    auto mat = GF2::Matrix<>::random(6);
    auto poly = GF2::characteristic_polynomial(mat);
    std::cout << "The GF2::Matrix:\n" << mat << "\n";
    std::cout << "has a characteristic polynomial with coefficients: " << poly << "\n\n";
    std::cout << "The characteristic polynomial sum of the matrix (should be all zeros):\n";
    std::cout << GF2::polynomial_sum(poly, mat) << "\n";
    return 0;
}
// end::doc[]
