include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= Construct From Integer Bits

Factory method to construct a bit-vector by copying the bit values _from_ some unsigned integer value.

[source,cpp]
----
static constexpr Vector from(std::unsigned_integral auto src);
----

This templated method returns a bit-vector constructed from the bits that make up the `src` word. 

The size of the bit-vector will be the `std::sizeof` for the specific `std::unsigned_integral` used as the argument. 
Note that there is _no_ requirement that argument type and `Block` are the same. 
The argument might be a 32-bit unsigned  which will create a bit-vector of size 32 packed into the default 64-bit block.

NOTE: This isn't a constructor because we don't want `src` to be treated as the _number_ of vector elements--instead it's the actual bits that we copy in to the bit-vector. 

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    uint16_t val = 65535;
    auto v1 = GF2::Vector<>::from(val);
    std::cout << "v1 = " << v1 << " has size " << v1.size() << std::endl;  
}
----

.Output
[source]
----
v1 = 1111111111111111 has size 16
----
