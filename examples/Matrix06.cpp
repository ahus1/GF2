// Fills a GF2::Matrix at random and try to find its inverse.
// If that works it prints the matrix, the inverse, the product of the two.

#include <GF2/GF2.h>
int
main()
{
    std::size_t N = 100;
    std::size_t trials = 1000;
    std::size_t fails = 0;
    for (std::size_t trial = 0; trial < trials; ++trial) {
        auto A = GF2::Matrix<>::random(N, N);
        auto B = GF2::invert(A);
        if (!B) ++fails;
    }
    auto p = GF2::Matrix<>::probability_singular(N);
    std::cout << "Matrix size: " << N << " x " << N << "\n"
              << "P[singular]: " << 100 * p << "%\n"
              << "Trials:      " << trials << "\n"
              << "No inverse:  " << fails << " times\n"
              << "Expected:    " << int(p * double(trials)) << " times\n";

    return 0;
}
