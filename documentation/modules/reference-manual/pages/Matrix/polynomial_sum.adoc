include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` - Polynomials of a Bit-Matrix.

Evaluates a polynomial over GF(2) using a _square_ bit-matrix as the argument.

[source,cpp]
----
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator> 
polynomial_sum(const Vector<Block, Allocator> &p, const Matrix<Block, Allocator> &M);
----

The polynomial's coefficients are stored in the bit-vector `p`. +
The function returns the value of the polynomial for the argument `M` where `M` is a _square_ bit-matrix:
[stem]
++++
p(M) = p_0 I + p_1 M + p_2 M^2 + \cdots + p_{n-1} M^{n-1}.
++++
`I` is the identity matrix that has the same dimensions as `M` and all arithmetic operations in GF(2) are performed mod 2.
The sum is performed using https://en.wikipedia.org/wiki/Horner%27s_method[Horner's method].

WARNING: The input matrix must be square.
That is checked using the xref:../assert/assert.adoc[gf2_assert] macro.
That check can be disabled by setting the `GF2_NDEBUG` flag at compile time.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Vector p1(16, [](size_t k) { return (k + 1) % 2; }); 
    GF2::Vector p2(17, [](size_t k) { return (k + 1) % 2; });
    auto m = GF2::Matrix<>::identity(6);
    std::cout << "Bit-matrix:\n" << m << '\n';
    std::cout << "as the argument for a polynomial of degree " << p1.size() - 1 << " "
              << "with coefficients " << p1 << " yields:\n"
              << polynomial_sum(p1, m) << '\n';
    std::cout << "as the argument for a polynomial of degree " << p2.size() - 1 << " "
              << "with coefficients " << p2 << " yields:\n"
              << polynomial_sum(p2, m) << '\n';
}
----

.Output
[source]
----
Bit-matrix:
100000
010000
001000
000100
000010
000001
as the argument for a polynomial of degree 15 with coefficients 1010101010101010 yields:
000000
000000
000000
000000
000000
000000
as the argument for a polynomial of degree 16 with coefficients 10101010101010101 yields:
100000
010000
001000
000100
000010
000001
----

.See Also
`xref:./pow.adoc[pow]` +
`xref:./pow.adoc[pow2]`