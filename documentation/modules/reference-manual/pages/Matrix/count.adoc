include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Bit Counts

The number of set/unset elements in a bit-matrix.

[source,cpp]
----
constexpr std::size_t count() const;              // <.>
constexpr std::size_t count_diagonal() const;     // <.>
constexpr bool trace() const;                     // <.>
----
<.> Returns the number of elements in the bit-matrix that are set to 1.
<.> Returns the number of elements on the bit-matrix diagonal that are set to 1.
<.> Returns `count_diagonal() % 2`--the "`sum`" of the diagonal elements.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m1("0000 0000 0000 0000");
    GF2::Matrix<> m2("0101 1010 0101 1010");
    GF2::Matrix<> m3("1111 1111 1111 1111");
 
    std::cout
        << "Matrix\t" << "count\t" << "diag\t" << "trace\n"
        << m1 << '\t' << m1.count() << '\t' << m1.count_diagonal() << '\t' << m1.trace() << '\n'
        << m2 << '\t' << m2.count() << '\t' << m2.count_diagonal() << '\t' << m2.trace() << '\n'
        << m3 << '\t' << m3.count() << '\t' << m3.count_diagonal() << '\t' << m3.trace() << '\n';
}
----

.Output
[source]
----
Matrix  count   diag    trace
0000
0000
0000
0000    0       0       0
0101
1010
0101
1010    8       0       0
1111
1111
1111
1111    16      4       0
----

.See Also
`xref:./size.adoc[rows]` +
`xref:./size.adoc[cols]` +
`xref:./size.adoc[size]`