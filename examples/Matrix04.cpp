/// @brief Get the characteristic polynomial of a user input `Matrix`

#include "common.h"
int
main()
{
    using Block = uint8_t;

    // Read a string and convert to a Matrix
    while (true) {
        std::string input;
        std::cout << "String input (q to quit)? ";
        std::getline(std::cin, input);
        if (input == "Q" || input == "q") break;

        try {
            GF2::Matrix<Block> mat(input);
            std::cout << "\"" << input << "\"  "
                 << "parsed as:\n"
                 << mat << std::endl;

            auto c = GF2::characteristic_polynomial(mat);
            std::cout << "Characteristic Polynomial: " << c << std::endl;

            // Let's see whether the matrix is a zero of characteristic polynomial?
            auto sum = polynomial_sum(c, mat);
            if (sum.none()) {
                std::cout << "And happily polynomial_sum(char_poly, mat) yielded the Zero matrix as expected.\n";
            }
            else {
                std::cout << "!!!Oops mat.polynomial_sum(char_poly) is not Zero!!!:\n";
                std::cout << sum << std::endl;
            }
        }
        catch (...) {
            std::cout << "Failed to parse \"" << input << "\" as a Matrix!" << std::endl;
        }
    }
    return 0;
}
