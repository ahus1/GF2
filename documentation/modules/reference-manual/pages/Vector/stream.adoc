include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Vector` -- Stream Operators

Methods to insert or extract a bit-vector from a stream.

[source,cpp]
----
template<std::unsigned_integral Block, typename Allocator>
std::ostream &
operator<<(std::ostream &s, const Vector<Block, Allocator> &rhs);   // <.>

template<std::unsigned_integral Block, typename Allocator>
std::istream &
operator>>(std::istream &s, Vector<Block, Allocator> &rhs);         // <.>
----
<.> Writes a binary string representation of a bit-vector to an output stream.
<.> Fill a bit-vector by reading bits encoded as a binary or hex string from a stream.

The input stream operator will throw a `std::invalid_argument` exception on parse failures.

NOTE: Binary strings are written or read as being in _vector-order_ where the least significant element is on the _left_, so as v~0~v~1~v~2~...

.Valid strings 
include::encoding.adoc[]

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    // Read from a stream until we have a parse error ...
    while (true) {
        GF2::Vector<> v;
        std::cout << "GF2::Vector? ";
        try {
            std::cin >> v;
            std::cout << "Parsed as " << v << std::endl;
        }
        catch (...) {
            std::cout << "Couldn't parse that input as a GF2::Vector! Quitting ..." << std::endl;
            break;
        }
    }
}
----

.Input and Output
[source]
----
GF2::Vector? 111
Parsed as 111
GF2::Vector? 0b111
Parsed as 111
GF2::Vector? 0x111
Parsed as 100010001000
GF2::Vector? 0x111_8
Parsed as 10001000100
GF2::Vector? 0x111_4
Parsed as 1000100010
GF2::Vector? 0x111_2
Parsed as 100010001
GF2::Vector? q
Couldn't parse that input as a GF2::Vector! Quitting ...
----

.See Also
`xref:./to_string.adoc[to_string]` .   +
`xref:./to_string.adoc[to_bit_order]` +
`xref:./to_string.adoc[to_hex]`