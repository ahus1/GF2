#include "common.h"

int
main()
{
    // Number of trials
    std::size_t trials = 100;

    // Each trial will run on a bit-matrix of this size
    std::size_t N = 100;

    // Number of non-singular matrices
    std::size_t singular = 0;

    // Start the trials ...
    for (std::size_t n = 0; n < trials; ++n) {

        // Create a random matrix & decompose it
        auto A = GF2::Matrix<>::random(N);
        auto LU = GF2::lu_decompose(A);

        // Extract L & U as their own bit-matrices & compute their product
        auto U = LU.U();
        auto L = LU.L();
        auto lu = GF2::dot(L, U);

        // Check whether P.A = L.U as it should. Exit early if not!
        LU.permute(A);
        if (A != lu) {
            std::cout << "P.A, L.U:\n";
            print(A, lu);
            std::cout << "THEY DON'T MATCH -- CODING ERROR. EXITING ...\n";
            exit(1);
        }

        // Count the number of singular matrices we come across
        if (LU.singular()) singular++;
    }

    // Stats ...
    auto p = GF2::Matrix<>::probability_singular(N);
    std::cout << "Matrix size:  " << N << " x " << N << "\n"
              << "Pr[singular]: " << 100 * p << "%\n"
              << "Trials:       " << trials << "\n"
              << "Singular:     " << singular << " times\n"
              << "Expected:     " << int(p * double(trials)) << " times\n"
              << "P.A == L.U  for ALL cases!\n";
    return 0;
}