include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Encode a Bit-Matrix

Methods to encode a bit-matrix as a string in a binary or hex format.

[source,cpp]
----
std::string to_string(const std::string& delim = "\n", 
                      bool bit_order = false, char off = '0', char on = '1') const; // <.>
std::string to_bit_order(const std::string& delim = "\n", 
                         char off = '0', char on = '1') const;                      // <.>
std::string to_hex(const std::string& delim = "\n", ) const;                        // <.>
----
<.> Get a binary-string representation for the bit-matrix using the given characters for set and unset elements.
<.> Get a binary-string representation for the bit-matrix with  the _rows_ in _bit-order_ using the given characters for set and unset elements.
<.> Get a hex-string representation for the bit-matrix.

These methods print the _rows_ of the bit-matrix as documented in xref:../Vector/to_string.adoc[`GF2::Vector.to_string()`].
The rows are separated by whatever the `delim` string is -- it defaults to newlines.

.Example: Binary encodings
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m(4, [](std::size_t i, std::size_t j) { return (i + j)%2; });
    std::cout << "to_string(): \n"                          
              << m.to_string()                     << '\n';
    std::cout << "to_string(\"; \"):                "                    
              << m.to_string("; ")                 << '\n';
    std::cout << "to_string(\"; \", true):          "              
              << m.to_string("; ", true)           << '\n';
    std::cout << "to_string(\"; \", true, '.', '-') "    
              << m.to_string("; ", true, '.', '-') << '\n';
}
----

.Output
[source]
----
to_string(): 
0101
1010
0101
1010
to_string("; "):                0101; 1010; 0101; 1010
to_string("; ", true):          1010; 0101; 1010; 0101
to_string("; ", true, '.', '-') -.-.; .-.-; -.-.; .-.-
----

.Example: Hex encodings
[source,cpp]
----
#include <GF2/GF2.h>

int main()
{   
    auto m3 = GF2::Matrix<>::ones(3);
    auto m4 = GF2::Matrix<>::ones(4);
    auto m5 = GF2::Matrix<>::ones(5);
    auto m6 = GF2::Matrix<>::ones(6);
    std::cout << "m3.to_hex(\"; \"): " << m3.to_hex("; ")  << '\n';
    std::cout << "m4.to_hex(\"; \"): " << m4.to_hex("; ")  << '\n';
    std::cout << "m5.to_hex(\"; \"): " << m5.to_hex("; ")  << '\n';
    std::cout << "m6.to_hex(\"; \"): " << m6.to_hex("; ")  << '\n';
}
----

.Output
[source]
----
m3.to_hex("; "): 7_8; 7_8; 7_8
m4.to_hex("; "): F; F; F; F
m5.to_hex("; "): F1_2; F1_2; F1_2; F1_2; F1_2
m6.to_hex("; "): F3_4; F3_4; F3_4; F3_4; F3_4; F3_4
----

.Example: Reconstituting bit-matrices from hex encodings
[source,cpp]
----
#include <GF2/GF2.h>

int main()
{   
    auto m3 = GF2::Matrix<>::random(3);     // <.>
    auto m4 = GF2::Matrix<>::random(4);
    auto m5 = GF2::Matrix<>::random(5);
    auto m6 = GF2::Matrix<>::random(6);

    auto s3 = m3.to_hex("; ");              // <.>
    auto s4 = m4.to_hex("; ");
    auto s5 = m5.to_hex("; ");
    auto s6 = m6.to_hex("; ");

    GF2::Matrix<> c3(s3);                   // <.>
    GF2::Matrix<> c4(s4);
    GF2::Matrix<> c5(s5);
    GF2::Matrix<> c6(s6);

    // <.>
    std::cout << "m3: " << s3 << '\n' << "c3: " << c3.to_hex("; ")  
              << (c3 == m3 ? " match!" : "FAIL") << '\n'; 
    std::cout << "m4: " << s4 << '\n' << "c4: " << c4.to_hex("; ")  
              << (c4 == m4 ? " match!" : "FAIL") << '\n'; 
    std::cout << "m5: " << s5 << '\n'  << "c5: " << c5.to_hex("; ")  
              << (c5 == m5 ? " match!" : "FAIL") << '\n'; 
    std::cout << "m6: " << s6 << '\n' << "c6: " << c6.to_hex("; ")  
              << (c6 == m6 ? " match!" : "FAIL") << '\n';
}
----
<.> Set up some bit-matrices of various sizes with random 50-50 fills.
<.> Convert the bit-matrices to hex-strings.
<.> Use the strings to construct bit-matrices.
<.> Check that the two sets of vectors match.

.Output
[source]
----
m3: 7_8; 3_8; 2_8
c3: 7_8; 3_8; 2_8 match!
m4: A; F; B; A
c4: A; F; B; A match!
m5: A1_2; B0_2; E0_2; 61_2; 50_2
c5: A1_2; B0_2; E0_2; 61_2; 50_2 match!
m6: 71_4; A3_4; C2_4; 60_4; F0_4; 53_4
c6: 71_4; A3_4; C2_4; 60_4; F0_4; 53_4 match!
----

.See Also
`xref:./stream.adoc[operator<<]` +
`xref:./stream.adoc[operator>>]`