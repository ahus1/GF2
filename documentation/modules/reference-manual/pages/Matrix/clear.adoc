include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Clear out a Bit-Matrix

Removes all elements from a bit-matrix.

[source,cpp]
----
constexpr Matrix &clear();
----

The bit-matrix's `rows()`, `cols()`, and `size()` all become 0, but the capacity is _not_ changed.
Method returns a reference to `*this` so it can be chained with other calls.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    auto m = GF2::Matrix<>::random(8ul, 16ul);
    std::cout << "Pre-clear:\n"  << m         << '\n';
    std::cout << "Post-clear:\n" << m.clear() << '\n';
    std::cout << "m.rows(): "    << m.rows()  << '\n';
    std::cout << "m.cols(): "    << m.cols()  << '\n';
    std::cout << "m.size(): "    << m.size()  << '\n';
}
----

.Output
[source]
----
Pre-clear:
0001000100010100
0010100101010101
1011010100101111
1001001011111101
0100001110011001
1010011111111001
1111011111001001
1101101000110011
Post-clear:

m.rows(): 0
m.cols(): 0
m.size(): 0
----

.See Also
`xref:./add-pop.adoc[add_row]` +
`xref:./add-pop.adoc[add_col]` +
`xref:./add-pop.adoc[pop_row]` +
`xref:./add-pop.adoc[pop_col]`