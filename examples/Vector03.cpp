/// @brief Checking on the creation of sub-vectors for both correctness and speed

#include "common.h"

/// @brief Simplest method to extract a sub-vector (to compare to our 'efficient' implementation)
/// @param begin Starting point to extract the sub-vector from
/// @param len The number of elements in the sub-vector
template<std::unsigned_integral Block, typename Allocator>
auto
simple_sub(const GF2::Vector<Block, Allocator> &src, std::size_t begin, std::size_t len)
{
    /// Trivial case?
    if (src.empty() || len == 0) return GF2::Vector<Block, Allocator>{};

    // Optionally check the begin index
    gf2_debug_assert(begin < src.size(), "begin = " << begin << ", src.size() = " << src.size());

    // Optionally check the end index (one beyond the last valid index)
    std::size_t end = begin + len;
    gf2_debug_assert(end <= src.size(), "end = " << end << ", src.size() = " << src.size());

    // Create the correct sized sub-vector all initialized to 0's
    GF2::Vector<Block, Allocator> retval(len);

    // Simplest element by element copy
    for (std::size_t i = begin; i < end; ++i)
        if (src.test(i)) retval.set(i - begin);

    return retval;
}

/// @brief Simplest method to extract a sub-vector (to compare to our 'efficient' implementation)
/// @param len Length to extract (positive means from start, negative means from end)
template<std::unsigned_integral Block, typename Allocator>
auto
simple_sub(const GF2::Vector<Block, Allocator> &src, int len)
{
    /// Trivial case?
    if (src.empty() || len == 0) return GF2::Vector<Block, Allocator>{};

    auto        alen = std::size_t(abs(len));
    std::size_t begin = len > 0 ? 0 : src.size() - alen;
    return simple_sub(src, begin, alen);
}

int
main()
{
    using block_type = uint64_t;

    std::size_t n = 1013;
    auto        v = GF2::Vector<block_type>::random(n);

    std::cout << "Checking sub-vector extraction from front for:\n";
    std::cout << v << "\n";
    bool all_ok = true;
    auto j_max = int(v.size());
    for (int j = 0; j <= j_max; ++j) {
        if (v.sub(j) != simple_sub(v, j)) {
            std::cout << "OOPS -- hit mismatch between v.sub(j) & simple_sub(v, j) for j = " << j << "\n";
            std::cout << "simple_sub(v, j): " << simple_sub(v, j) << "\n";
            std::cout << "v.sub(j):        " << v.sub(j) << "\n";
            std::cout << std::endl;
            all_ok = false;
            break;
        }
    }
    if (all_ok) std::cout << "Congratulations: No sub-vectors errors detected starting from front!\n";
    std::cout << std::endl;

    std::cout << "Checking sub-vector extraction from back for:\n";
    std::cout << v << "\n";
    all_ok = true;
    for (int j = 0; j <= j_max; ++j) {
        if (v.sub(-j) != simple_sub(v, -j)) {
            std::cout << "OOPS -- hit mismatch between v.sub(j) & simple_sub(v, j) for j = " << -j << "\n";
            std::cout << "simple_sub(v, j): " << simple_sub(v, -j) << "\n";
            std::cout << "v.sub(j):        " << v.sub(-j) << "\n";
            std::cout << std::endl;
            all_ok = false;
            break;
        }
    }
    if (all_ok) std::cout << "Congratulations: No sub-vectors errors detected starting from back!\n";
    std::cout << std::endl;

    std::cout << "Checking ALL sub-vector extractions:\n";
    std::cout << v << "\n";
    all_ok = true;
    std::size_t i;
    for (i = 0; i < v.size(); ++i) {
        for (std::size_t len = 0; len <= v.size() - i; ++len) {
            if (v.sub(i, len) != simple_sub(v, i, len)) {
                std::cout << "MISMATCH between v.sub(i,len) & simple_sub(v,i,len) for (i, len) = " << i << ", " << len
                          << "\n";
                std::cout << "simple_sub(v,i,len):  " << simple_sub(v, i, len) << "\n";
                std::cout << "v.sub(i,len):        " << v.sub(i, len) << "std::endl";
                std::cout << std::endl;
                all_ok = false;
                break;
            }
        }
    }
    if (all_ok) std::cout << "Congratulations: No sub-vectors errors detected starting ANYWHERE!!\n";
    std::cout << std::endl;

    n = 3013;
    v = GF2::Vector<block_type>::random(n);

    std::cout << "Timing test -- creating all sub-vectors from a vector of size: " << v.size() << "\n";
    StopWatch sw1;
    sw1.start();
    std::size_t sum1 = 0;
    for (i = 0; i < v.size(); ++i) {
        for (std::size_t len = 0; len <= v.size() - i; ++len) sum1 += simple_sub(v, i, len).count();
    }
    sw1.stop();

    StopWatch sw2;
    sw2.start();
    std::size_t sum2 = 0;
    for (i = 0; i < v.size(); ++i) {
        for (std::size_t len = 0; len <= v.size() - i; ++len) sum2 += v.sub(i, len).count();
    }
    sw2.stop();

    std::cout << "simple_sub(v, ...) took: " << sw1 << "\n";
    std::cout << "v.sub(...) took:         " << sw2 << "\n";
    std::cout << "respective counts:       " << sum1 << " & " << sum2 << "\n";
    std::cout << std::endl;

    return 0;
}
