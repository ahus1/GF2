include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= Summary

`GF2++` is a header only {cpp} library for performing linear algebra with boolean vectors and matrices (more correctly in mathematical jargon, for linear algebra over {gf2}). 

Even if you have no interest in the mathematics in question, or in {cpp}, you may still find the discussion of how we documented this small library to be of some use.

The library is documented in two ways:

.*_Short-form Documentation_*
Brief _in-source_ documentation is provided for all the publicly accessible methods, functions, and data members. 
This is done in the usual manner using code comments that are formatted with a minimal set of {doxygen} commands/tags. 
Modern code editors can parse those specially formatted comments and use them to present the library user with some useful _tool tips_ to remind them of method arguments etc.

.*_Long-form Documentation_*
The library also has long form documentation which is maintained external to the source itself though it does live in the same code repository. 
This documentation is written in {adoc}, a markup language that is richer than the ubiquitous {markdown} but still pretty light weight. 
{adoc} provides several features that make it ideal for generating technical documentation that spans many pages. 
Our AsciiDoc pages include small copyable example programs that exercise all the key functionality of the library.

The site generator {antora} marries those AsciiDoc content files with others that define a look and feel to produce the static documentation website.

You can navigate through the documentation strategy document in sections by clicking through the navigation menu on the left or as a single (long) document xref:./all-in-one.adoc[here].