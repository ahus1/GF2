/// @brief Check our char-poly computation vs. some pre-canned ones done by other means
/// The pre-canned results are in file with the format
///     matrix (row by row with the rows delimited by commas, white space, semi-colons etc.)
///     coefficients of characteristic polynomial as a Vector
/// There can many of these pairs of lines in the file and we iterate through them all
/// Blank lines and comment lines (which start with a "#") are ignored in the file.

#include "common.h"

#include <fstream>

int
main()
{
    // Get the name of the data file and open it ...
    std::string   data_file_name;
    std::ifstream data_file;
    while (true) {
        std::cout << "Data file name (x to exit ...): ";
        std::cin >> data_file_name;
        if (data_file_name == "x" || data_file_name == "X") exit(0);
        data_file.open(data_file_name);
        if (data_file.is_open()) break;
        std::cout << "Failed to open " << data_file_name << " but you can try again ..." << std::endl;
    }

    // Read matrices and characteristic polynomials from the file using our own get_line(...)
    // That skips blank lines and comment lines which start with a "#"
    std::string matrix_string;
    std::string coeffs_string;
    size_t      n_test = 0;
    while (get_line(data_file, matrix_string) && get_line(data_file, coeffs_string)) {
        try {
            GF2::Matrix m(matrix_string);
            GF2::Vector c(coeffs_string);
            auto        p = GF2::characteristic_polynomial(m);
            ++n_test;
            std::cout << ".";
            if (c != p) {
                std::cout << "TEST " << n_test << ": FAILED!!!";
                std::cout << "Matrix:\n" << m << std::endl;
                std::cout << "Computed vs. pre-canned: " << p << "\t" << c << std::endl;
                exit(1);
            }
        }
        catch (...) {
            std::cout << "Failed to parse data file: " << data_file_name << std::endl;
        }
    }
    data_file.close();

    // Only get here on success
    std::cout << std::endl;
    std::cout << "Congratulations: All " << n_test << " tests passed!" << std::endl;

    return 0;
}
