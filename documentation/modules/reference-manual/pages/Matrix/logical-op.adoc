include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Logical Op Operators()

Methods to perform element-by-element binary AND, XOR, OR, +, -, * between two equal sized bit-matrices.

[source,cpp]
----
template<std::unsigned_integral Block, typename Alloc>
constexpr GF2::Matrix<Block, Alloc>
operator&(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
operator^(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
operator|(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
operator+(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
operator-(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
operator*(const GF2::Matrix<Block, Alloc> &lhs, const GF2::Matrix<Block, Alloc> &rhs);    // <.>
----
<.> Returns a bit-matrix whose bits are the result of binary AND  between the corresponding pairs of bits of `rhs` and `lhs`.
<.> Returns a bit-matrix whose bits are the result of binary XOR  between the corresponding pairs of bits of `rhs` and `lhs`.
<.> Returns a bit-matrix whose bits are the result of binary OR   between the corresponding pairs of bits of `rhs` and `lhs`.
<.> Returns a bit-matrix whose bits are the result of binary XOR  between the corresponding pairs of bits of `rhs` and `lhs`.
In GF(2) addition corresponds to XOR.
<.> Returns a bit-matrix whose bits are the result of binary XOR  between the corresponding pairs of bits of `rhs` and `lhs`.
In GF(2) subtraction corresponds to XOR.
<.> Returns a bit-matrix whose bits are the result of binary AND  between the corresponding pairs of bits of `rhs` and `lhs`.
In GF(2) multiplication corresponds to AND.

WARNING: The two bit-matrices in question must be of the same size.
If the `GF2_DEBUG` flag is set at compile time this is checked and any violation will cause the program to abort with a helpful message.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m1(4,[](std::size_t i, std::size_t j) { return (i + j) % 2; }); 
    auto m2 = GF2::Matrix<>::ones(4);                             
                                 
    std::cout << "m1:\n" << m1  << '\n';
    std::cout << "m2:\n" << m2  << '\n';
    std::cout << "m1 & m2:\n" << (m1 & m2) << '\n';
    std::cout << "m1 | m2:\n" << (m1 | m2) << '\n';
    std::cout << "m1 ^ m2:\n" << (m1 ^ m2) << '\n';
}
----

.Output
[source]
----
m1:
0101
1010
0101
1010
m2:
1111
1111
1111
1111
m1 & m2:
0101
1010
0101
1010
m1 | m2:
1111
1111
1111
1111
m1 ^ m2:
1010
0101
1010
0101
----

.See Also
`xref:./logical-op-eq.adoc[operator&++=++]`  +
`xref:./logical-op-eq.adoc[operator^++=++]`  +
`xref:./logical-op-eq.adoc[operator\|++=++]` +
`xref:./logical-op-eq.adoc[operator+++=++]`  +
`xref:./logical-op-eq.adoc[operator-++=++]`  +
`xref:./logical-op-eq.adoc[operator*++=++]`  +
`xref:./logical-op-eq.adoc[operator~]`