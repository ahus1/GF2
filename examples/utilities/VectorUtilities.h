/// @file utilities.h
/// @brief A few generally useful  utility functions
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include <iostream>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

/// @brief Push the contents of a vector into a string
template<typename T>
inline std::string
to_string(const std::vector<T> &v)
{
    std::ostringstream ss;

    auto n = v.size();
    if (n == 0) { ss << "[]"; }
    else {
        ss << "[";
        for (std::size_t i = 0; i < n - 1; ++i) ss << v[i] << ", ";
        ss << v[n - 1] << "]";
    }
    return ss.str();
}

/// @brief Push the contents of a vector @c uint8_t into a string
/// @note By default any @c uint8_t prints as a character -- here we force them to print as integer values.
template<>
inline std::string
to_string(const std::vector<uint8_t> &v)
{
    std::ostringstream ss;

    auto n = v.size();
    if (n == 0) { ss << "[]"; }
    else {
        ss << "[";
        for (std::size_t i = 0; i < n - 1; ++i) ss << int(v[i]) << ", ";
        ss << int(v[n - 1]) << "]";
    }
    return ss.str();
}

/// @brief Print a vector to a stream which defaults to @c std::cout
template<typename T>
inline void
print(const std::vector<T> &v, std::ostream &strm = std::cout)
{
    strm << to_string(v) << "\n";
}
