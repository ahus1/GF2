#include <GF2/GF2.h>
int
main()
{
    using Block = uint8_t;

    std::size_t nu = 1;
    std::size_t nv = 7;

    GF2::Vector<Block> u(nu);
    GF2::Vector<Block> v(nv);
    v.flip();

    u.description("U");
    v.description("V");

    u.append(v);
    u.description("U|V");
    return 0;
}