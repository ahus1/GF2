/// @brief Parsing bit-matrices from strings and streams
#include "common.h"

int
main()
{
    // Read a string and convert to a Matrix
    while (true) {
        std::string input;
        std::cout << "String input (q to move on)? ";
        std::getline(std::cin, input);
        if (input == "Q" || input == "q") break;

        try {
            GF2::Matrix m(input);
            std::cout << "\"" << input << "\"  "
                 << "parsed as:\n"
                 << m << std::endl;
        }
        catch (...) {
            std::cout << "Failed to parse \"" << input << "\" as a Matrix!" << std::endl;
        }
    }

    // Read a Matrix directly from a stream
    while (true) {
        GF2::Matrix<> m;
        std::cout << "Stream input (any invalid Matrix to quit)? ";
        try {
            std::cin >> m;
            std::cout << "Parsed as:\n" << m << std::endl;
        }
        catch (...) {
            std::cout << "Couldn't parse that input as a Matrix! Quitting ..." << std::endl;
            break;
        }
    }

    return 0;
}
