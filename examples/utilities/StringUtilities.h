/// @file string.h
/// @brief Some utility functions that act on @c std::strings and @c std::string_views
///
/// Functions with simplest names (e.g. @c to_upper) do the conversions @b in-place. There are also versions of the
/// algorithms that leave the input string alone and instead return a new string that is an appropriately transformed
/// version of the input. E.g. the counterpart to @c to_upper(std::string&) is @c to_upper_copy(std::string_view).
/// Any of the functions that end in @c _copy work in the same manner leaving the input string untouched.
///
/// @copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include <algorithm>
#include <cctype>
#include <charconv>
#include <optional>
#include <regex>
#include <string>
#include <vector>
#include<ranges>

/// @brief Returns a string for a bool input
inline std::string
b2s(bool x)
{
    return x ? "TRUE" : "FALSE";
}

/// @brief Converts a string to all upper-case. Uses the standard C-library @c toupper(...) function with all the
/// same limitations (so will not work for wide character sets).
inline void
to_upper(std::string &str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(::toupper(c)); });
}

/// @brief Converts a std::string to all upper-case. Uses the standard C-library @c toupper(...) function with all the
/// same limitations (so will not work for wide character sets).
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
to_upper_copy(std::string_view input)
{
    std::string s{input};
    to_upper(s);
    return s;
}

/// @brief Converts a std::string to all upper-case. Uses the standard C-library @c tolower(...) function with all the
/// same limitations (so will not work for wide character sets).
inline void
to_lower(std::string &str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(::tolower(c)); });
}

/// @brief Converts a std::string to all upper-case. Uses the standard C-library @c tolower(...) function with all the
/// same limitations (so will not work for wide character sets).
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
to_lower_copy(std::string_view input)
{
    std::string s{input};
    to_lower(s);
    return s;
}

/// @brief Removes any leading white-space from a string.
inline void
trim_left(std::string &str)
{
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) { return !std::isspace(ch); }));
}

/// @brief Removes any leading white-space from a string.
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
trim_left_copy(std::string_view input)
{
    std::string s{input};
    trim_left(s);
    return s;
}

/// @brief Remove any trailing white-space from a string.
inline void
trim_right(std::string &str)
{
    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) { return !std::isspace(ch); }).base(), str.end());
}

/// @brief Removes any trailing white-space from a string.
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
trim_right_copy(std::string_view input)
{
    std::string s{input};
    trim_right(s);
    return s;
}

/// @brief Removes all leading & trailing white-space from a string.
inline void
trim(std::string &str)
{
    trim_left(str);
    trim_right(str);
}

/// @brief Removes all leading & trailing white-space from a string.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
trim_copy(std::string_view input)
{
    std::string s{input};
    trim(s);
    return s;
}

/// @brief Replace the first occurrence of a target substring with some other string
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
/// @param changeTo what we replace the first occurrence of the target with.
inline void
replace_first(std::string &str, std::string_view target, std::string_view changeTo)
{
    auto p = str.find(target);
    if (p != std::string::npos) str.replace(p, target.length(), changeTo);
}

/// @brief Replace the first occurrence of a target substring with some other string
/// @param input string to be be converted (unaltered on output).
/// @param target the target substring to hunt for.
/// @param changeTo what we replace the first occurrence of the target with.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
replace_first_copy(std::string_view input, std::string_view target, std::string_view changeTo)
{
    std::string s{input};
    replace_first(s, target, changeTo);
    return s;
}

/// @brief Replace the last occurrence of a target substring with some other string
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
/// @param changeTo what we replace the last occurrence of the target with.
inline void
replace_last(std::string &str, std::string_view target, std::string_view changeTo)
{
    auto p = str.rfind(target);
    if (p != std::string::npos) str.replace(p, target.length(), changeTo);
}

/// @brief Replace the last occurrence of a target substring with some other string
/// @param input string to be be converted (unaltered on output).
/// @param target the target substring to hunt for.
/// @param changeTo what we replace the first occurrence of the target with.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
replace_last_copy(std::string_view input, std::string_view target, std::string_view changeTo)
{
    std::string s{input};
    replace_last(s, target, changeTo);
    return s;
}

/// @brief Replace all occurrences of a target substring with some other string
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
/// @param changeTo what we replace all occurrences of the target with.
inline void
replace(std::string &str, std::string_view target, std::string_view changeTo)
{
    std::size_t p = 0;
    while ((p = str.find(target, p)) != std::string::npos) {
        str.replace(p, target.length(), changeTo);
        p += changeTo.length();
    }
}

/// @brief Replace all occurrences of a target substring with some other string
/// @param str string to be be converted (unaltered on output).
/// @param target the target substring to hunt for.
/// @param changeTo what we replace the first occurrence of the target with.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
replace_copy(std::string_view input, std::string_view target, std::string_view changeTo)
{
    std::string s{input};
    replace(s, target, changeTo);
    return s;
}

/// @brief Replace all contiguous white space sequences in a string.
/// @param with By default they are replaced with a single space character
/// @param do_trim By default any white space at the beginning and end is removed entirely
inline void
replace_spaces(std::string &s, const std::string &with = " ", bool do_trim = true)
{
    if (do_trim) trim(s);
    std::regex ws{R"(\s+)"};
    s = std::regex_replace(s, ws, with);
}

/// @brief Replace all contiguous white space sequences in a string.
/// @param with By default they are replaced with a single space character
/// @param do_trim By default any white space at the beginning and end is removed entirely
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
replace_spaces_copy(std::string_view input, const std::string &with = " ", bool do_trim = true)
{
    std::string s{input};
    replace_spaces(s, with, do_trim);
    return s;
}

/// @brief Erase the first occurrence of a target substring.
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
inline void
erase_first(std::string &str, std::string_view target)
{
    auto p = str.find(target);
    if (p != std::string::npos) str.erase(p, target.length());
}

/// @brief Erase the first occurrence of a target substring.
/// @param input string to be be converted (unaltered output).
/// @param target the target substring to hunt for.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
erase_first_copy(std::string_view input, std::string_view target)
{
    std::string s{input};
    erase_first(s, target);
    return s;
}

/// @brief Erase the last occurrence of a target substring.
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
inline void
erase_last(std::string &str, std::string_view target)
{
    auto p = str.rfind(target);
    if (p != std::string::npos) str.erase(p, target.length());
}

/// @brief Erase the last occurrence of a target substring.
/// @param input string to be be converted (unaltered output).
/// @param target the target substring to hunt for.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
erase_last_copy(std::string_view input, std::string_view target)
{
    std::string s{input};
    erase_last(s, target);
    return s;
}

/// @brief Erase all occurrences of a target substring.
/// @param str string to be be converted.
/// @param target the target substring to hunt for.
inline void
erase(std::string &str, std::string_view target)
{
    std::size_t p = 0;
    while ((p = str.find(target, p)) != std::string::npos) str.erase(p, target.length());
}

/// @brief Erase all occurrences of a target substring.
/// @param input string to be be converted (unaltered output).
/// @param target the target substring to hunt for.
/// @return A new @c std::string that is a transformed _copy of the @c input.
inline std::string
erase_copy(std::string_view input, std::string_view target)
{
    std::string s{input};
    erase(s, target);
    return s;
}

/// @brief Check if a string starts with a particular prefix string.
/// @param str the string to check.
/// @param prefix the substring to look for at the start.
inline bool
starts_with(std::string_view str, std::string_view prefix)
{
    return str.find(prefix) == 0;
}

/// @brief Check if a string ends with a particular suffix string.
/// @param str the string to check.
/// @param suffix the substring to look for at the end.
inline bool
ends_with(std::string_view str, std::string_view suffix)
{
    const auto pos = str.rfind(suffix);
    return (pos != std::string::npos) && (pos == (str.length() - suffix.length()));
}

/// @brief Tries to center a string within a fixed space, padding the front and rear with spaces.
/// @param str The string to center
/// @param space We will try to center @c str in this space. We do nothing if this is too small.
/// @param width C++ has no built-in facility for establishing the @e width of @c str!
/// Note that @c str.length() just returns the number of bytes used to store the string. Good for UTF8 characters but is
/// a problem for e.g. "♣️" which has "length" 6 even though the width is reasonably 1. Specify the @c width if
/// this is a problem (the default is to just use the standard @c str.length() method.
inline void
center(std::string &str, std::size_t space, std::size_t width = 0)
{
    // Length of the string in bytes
    std::size_t str_width = str.length();

    // Should we go the user's suggestion for the width instead?
    if (width > 0 && width < str_width) str_width = width;

    // Going to want to add at least one " " string at the start and the end so see if that is possible?
    if (space < str_width + 2) return;

    // Get the padding string on the left ...
    std::size_t n_left = (space - str_width) / 2;
    std::string left(n_left, ' ');

    // And on the right ...
    std::size_t n_right = space - (left.length() + str_width);
    std::string right(n_right, ' ');

    // Add the padding to our string on the left and the right.
    str = left + str + right;
}

/// @brief Tries to center a string within a fixed space, padding the front and rear with spaces.
/// @param input The string to center (unaltered on output)
/// @param space We will try to center @c str in this space. We do nothing if this is too small.
/// @param width C++ has no built-in facility for establishing the @e width of @c str!
/// Note that @c str.length() just returns the number of bytes used to store the string. Good for UTF8 characters but is
/// a problem for e.g. "♣️" which has "length" 6 even though the width is reasonably 1. Specify the @c width if
/// this is a problem (the default is to just use the standard @c str.length() method.
/// @return A new @c std::string that is the transformed _copy of the input.
inline std::string
center_copy(const std::string &input, std::size_t space, std::size_t width = 0)
{
    auto s = input;
    center(s, space, width);
    return s;
}

/// @brief Try to read a value of a particular type from a @c std::string. Uses @c std::from_chars(...) to retrieve a
/// possible integral type from a string so you might use this with code like @c x=possible<double>(str);
/// @return @c std::nullopt on failure.
template<typename T>
constexpr std::optional<T>
possible(std::string_view in, const char **next = nullptr)
{
    in.remove_prefix(in.find_first_not_of("+ "));
    T    retval;
    auto ec = std::from_chars(in.cbegin(), in.cend(), retval);
    if (next) *next = ec.ptr;
    if (ec.ec != std::errc{}) return std::nullopt;
    return retval;
}

/// @brief Given input text and delimiters, tokenize the text and pass the tokens to a function
/// @param b If the text is in `str` this parameter might be `cbegin(str)`
/// @param e If the text is in `str` this parameter might be `cend(str)`
/// @param db If the possible delimiters are in ``delims` this might be `cbegin(delims)`
/// @param de If the possible delimiters are in ``delims` this might be `cend(delims)`
/// @param function Will be called with a token like this `function(tokenBegin, tokenEnd)
/// @note Credit to [blog article](https://tristanbrindle.com/posts/a-quicker-study-on-tokenising/)
template<typename InputIt, typename ForwardIt, typename BinaryFunc>
constexpr void
for_each_token(InputIt ib, InputIt ie, ForwardIt db, ForwardIt de, BinaryFunc function)
{
    while (ib != ie) {
        const auto x = find_first_of(ib, ie, db, de); // Find a token in the input text
        function(ib, x);                              // Call the user supplied function on the token
        if (x == ie) break;                           // Stop if we hit the end of the input text
        ib = next(x);                                 // Otherwise go again past that token we just found
    }
}

/// @brief Tokenize a string and put the tokens into the passed output container.
/// @param input The string to tokenize
/// @param output You pass in this "STL" container which we fill with the tokens.
/// @param skip By default we ignore any empty tokens (e.g. two spaces in a row)
/// @param delimiters By default tokens are broken on white space, commas, semi-colons, and colons.
template<typename Container_t>
constexpr void
tokenize(const std::string &input, Container_t &output, const std::string &delimiters = "\t,;: ", bool skip = true)
{
    auto ib = cbegin(input);
    auto ie = cend(input);
    auto db = cbegin(delimiters);
    auto de = cend(delimiters);

    for_each_token(ib, ie, db, de, [&output, &skip](auto tb, auto te) {
        if (tb != te || !skip) { output.emplace_back(tb, te); }
    });
}

/// @brief Tokenize a string and return the tokens as a vector of strings
/// @param input The string to tokenize
/// @param delimiters By default tokens are broken on white space, commas, semi-colons, and colons.
/// @param skip By default we ignore any empty tokens (e.g. two spaces in a row)
/// @return std::vector<std::string> This is a vector with all the tokens
inline std::vector<std::string>
split(const std::string &input, const std::string &delimiters = "\t,;: ", bool skip = true)
{
    std::vector<std::string> output;
    output.reserve(input.size() / 2);
    tokenize(input, output, delimiters, skip);
    return output;
}

/// @brief Strip "surrounds" from a @c std::string so for example: (text) -> text. Multiple enclosures are also handled:
/// <<<text>>> -> text. The "surrounds" are only removed if they are correctly balanced.
inline void
strip_surrounds(std::string &s)
{
    std::size_t len = s.length();
    while (len > 1) {
        // If the first character is alpha-numeric we are done.
        char first = s[0];
        if (isalnum(first)) return;

        // First character is not alpha-numeric.
        // Grab the last character & check for a match.
        char last = s[len - 1];
        bool match = false;

        // Handle cases [text], {text}, <text>, and (text) and then all others
        switch (first) {
            case '(':
                if (last == ')') match = true;
                break;
            case '[':
                if (last == ']') match = true;
                break;
            case '{':
                if (last == '}') match = true;
                break;
            case '<':
                if (last == '>') match = true;
                break;
            default:
                if (last == first) match = true;
                break;
        }

        if (match) {
            // Shrink the string and continue
            s = s.substr(1, len - 2);
            len -= 2;
        }
        else {
            // No match => no surround so we can exit.
            return;
        }
    }
}

/// @brief Strip "surrounds" from a @c std::string so for example: (text) -> text. Multiple enclosures are also handled:
/// <<<text>>> -> text. The "surrounds" are only removed if they are correctly balanced.
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
strip_surrounds_copy(const std::string &input)
{
    auto s = input;
    strip_surrounds(s);
    return s;
}

/// @brief Turns a string like "[ hallo world ]" or "  Hallo World " into "HALLO WORLD"
inline void
standardize(std::string &s)
{
    trim(s);
    to_upper(s);
    strip_surrounds(s);
    trim(s);
}

/// @brief Turns a string like "[ hallo world ]" or "  Hallo World " into "HALLO WORLD"
/// @param input The string to be considered (left unaltered)
/// @return A new @c std::string that is a transformed _copy of @c input.
inline std::string
standardize_copy(std::string_view input)
{
    std::string s{input};
    standardize(s);
    return s;
}
