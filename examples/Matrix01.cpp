/// @brief Exercise some of the basic functionality for the `GF2::Matrix` class.

/// @note Not interested in speed for this particular example but *do* want bounds checking ...
#ifndef GF2_DEBUG
    #define GF2_DEBUG
#endif
#include "common.h"

int
main()
{
    using Block = uint64_t;

    std::size_t n = 9;
    std::size_t m = 9;

    GF2::Matrix<Block> v(n, m);
    v.description("Default construction");

    v.flip();
    v.description("After flip()");

    v.resize(6);
    v.description("After resize(6,6)");

    v.resize(10);
    v.description("After resize(10,10)");

    v.set_diagonal();
    v.description("After set_diagonal()");

    v.set_diagonal(1);
    v.description("After set_diagonal(1)");

    v.set_diagonal(-1);
    v.description("After set_diagonal(-1)");

    v.clear();
    v.description("After clear()");

    v.shrink_to_fit();
    v.description("After shrink_to_fit()");

    v.resize(5, 7);
    v.description("After resize(5, 7)");

    v = GF2::Matrix<Block>::random(7uL, 5uL);
    v.description("A random 7 x 5 GF2::Matrix");

    v.add_row();
    v.description("After add_row()");

    v.add_col();
    v.description("After add_col()");

    v.set_if([&v](size_t i, size_t) { return i == v.rows() - 1; });
    v.description("After setting last row with a lambda");

    v.set_if([&v](size_t, size_t j) { return j == v.cols() - 1; });
    v.description("After setting last col with a lambda");

    v.pop_row();
    v.description("After pop_row()");

    v.pop_col();
    v.description("After pop_col()");

    v.resize(12).reset();
    v.description("After resize(12).reset()");

    v.replace(GF2::Matrix<Block>::identity(6));
    v.description("After replace(GF2::Matrix<Block>::I(6))");

    v.replace(6, GF2::Matrix<Block>::identity(6));
    v.description("After replace(6,GF2::Matrix<Block>::I(6))");

    auto ur = v.to_vector();
    auto uc = v.to_vector(false);
    ur.description("As a vector by row");
    uc.description("As a vector by column");

    GF2::Matrix<Block>(ur, 12).description("GF2::Matrix<Block>(Vector,12)");
    GF2::Matrix<Block>(ur, 24).description("GF2::Matrix<Block>(Vector,24)");
    GF2::Matrix<Block>(uc, 12, false).description("GF2::Matrix<Block>(Vector,12,false)");
    GF2::Matrix<Block>(uc, 6, false).description("GF2::Matrix<Block>(Vector,6,false)");
    GF2::Matrix<Block>(ur).description("GF2::Matrix<Block>(Vector) -- a one row GF2::Matrix");
    GF2::Matrix<Block>(ur, false).description("GF2::Matrix<Block>(Vector, false) -- a one column GF2::Matrix");

    return 0;
}
