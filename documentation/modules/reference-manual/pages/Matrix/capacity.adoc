include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Capacity Queries

How many rows or columns can the bit-matrix accommodate with any more memory allocations?

[source,cpp]
----
constexpr std::size_t row_capacity() const;   // <.>
constexpr std::size_t col_capacity() const;   // <.>
----
<.> Returns the number of rows that be added without a memory allocation.
<.> Returns the number of columns that be added without a memory allocation.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m(3, 4);
    std::cout << "m.rows():         " << m.rows()           << '\n';
    std::cout << "m.cols():         " << m.cols()           << '\n';
    std::cout << "m.row_capacity(): " << m.row_capacity()   << '\n';
    std::cout << "m.col_capacity(): " << m.col_capacity()   << '\n';
}
----

.Output
[source]
----
m.rows():         3
m.cols():         4
m.row_capacity(): 3
m.col_capacity(): 64
----

.See Also
`xref:../Vector/capacity.adoc[GF2::Vector.capacity]` +
`xref:./description.adoc[description]`
