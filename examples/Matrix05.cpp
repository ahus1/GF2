// Fills a GF2::Matrix at random and reduces it to echelon form
// Prints the original and the echelon form matrix side-by-side

#include <GF2/GF2.h>
int
main()
{
    // Create a matrix and get its echelon & reduced echelon forms
    auto          A = GF2::Matrix<>::random(12);
    GF2::Vector<> pivots;
    std::cout << "Original, Row-Echelon, Reduced-Row-Echelon versions of a GF2::Matrix:\n";
    GF2::print(A, echelon_form(A), reduced_echelon_form(A, &pivots));

    // Analyze the rank of the matrix etc.
    auto n = A.rows();
    auto r = pivots.count();
    auto f = n - r;
    std::cout << "Matrix size:               " << n << " x " << n << '\n';
    std::cout << "Matrix rank:               " << r << '\n';
    std::cout << "Number of free variables:  " << f << "\n";
    if (f > 0) {
        std::cout << "Indices of free variables: ";
        pivots.flip().if_set_call([](std::size_t k) { std::cout << k << ' '; });
    }
    std::cout << std::endl;
    return 0;
}
