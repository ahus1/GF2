# CMake module that walks through a directory of source files where each file (plus any libraries) compiles 
# into a separate executable. The source files/executables are expected to follow a simple sequential naming scheme.
#
# A common scenario is to develop a library and alongside it develop a whole slew of simple command line 
# sample code that exercises various features in the library either as demos or tests. You might end up
# with an examples/ directory where the contents are along the lines Foo01.cpp, Foo02.cpp, Bar01.cpp, 
# Bar02.cpp, Bar03.cpp, Zippy.cpp ... These files are complete in themselves  so when they are compiled 
# and linked appropiriately they will yield executables  Foo01, Foo02, Bar01, Bar02, Bar03, Zippy
#
# The CMake function here nzzn_add_executables_from(examples, ...) will trawl through that examples/ 
# directory and create targets for Foo01, Foo02, Bar01, etc. It will also add another target Foo that 
# will expand to Foo01, Foo02, a target Bar that expands to Bar01, Bar02, Bar03 and finally a target 
# examples that will expand to all the the targets. Trailing arguments in the function call are assumed 
# to be  libraries the targets need to link to. 
#
# NOTE: The created targets are all EXCLUDE_FROM_ALL.

# nzzn_group_name_for(Foo03 groupTarget) will return Foo in the groupTarget argument
function(nzzn_group_name_for target groupTarget)

    # NOTE: CMake's regex abiliities are fairly basic but this seems to work OK
    string(REGEX REPLACE "(^[a-zA-Z]*)_*[0-9]+$" "\\1" group ${target})
    set(${groupTarget} ${group} PARENT_SCOPE)

endfunction()

# Private method to handle the situation where the user has say a target for library Foo and then tries to have 
# an executable Foo also in some examples directory -- cannot have two targets with the same name. We fudge it
# here by changing the target name for the conflict. All very inelegant partially because CMake doesn't seem
# to have an easy way to alter variables at SIBLING_SCOPE. So we use a global common but obviously private variable
set(__targetName "")
macro(__nzzn_resolve_any_target_conflicts)
    while(TARGET ${__targetName})
        set(newName ${__targetName}X)
        message(WARNING "Target '${__targetName}' exists twice (library and test?). Renaming the target to '${newName}'")
        set(__targetName ${newName})
   endwhile()
endmacro()

# The main event ...
# nzzn_add_executables_from(examples, ...) will walk through the examples/ directory and 
# create targets for Foo01, Foo02, Bar01, etc. It will also add a target Foo that will expand to
# Foo01, Foo02, a target Bar that expands to Bar01, Bar02, Bar03 and finally a target examples that
# will expand to all the the targets. Trailing arguments in the function call are assumed to be 
# libraries the targets need to link to. The created targets are EXCLUDE_FROM_ALL
function(nzzn_add_executables_from dir)

    # Trivial check ...
    if(NOT IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${dir})
        message(WARNING "Argument '${dir}' isn't a directory! (Spelling error?)")
        return()
    endif()

    # Grab all the source files from the directory in question (only handle C++/C as yet)
    file(GLOB sources LIST_DIRECTORIES false CONFIGURE_DEPENDS ${dir}/*.cpp ${dir}/*.cc ${dir}/*.c)
    set(allTargets "")

    # Add a target for each file 
    foreach(sourceFile ${sources})

        # File "Foo03.cpp" is expected to yield target "Foo03"
        get_filename_component(__targetName ${sourceFile} NAME_WLE)

        # Might have a target naming conflict (test program Foo linking to library Foo)
        __nzzn_resolve_any_target_conflicts()

        # Create that target (linking any libraries passed at the end of the function invocation)
        add_executable(${__targetName} EXCLUDE_FROM_ALL ${sourceFile})
        target_link_libraries(${__targetName} ${ARGN}) 

        # Add the new target to the list of all the target
        list(APPEND allTargets ${__targetName})

    endforeach()

    # Assuming there are targets to process we now create a target "dir" that expands to all the targets
    # and also group targets so that e.g. Bar will expand to Bar01 Bar02 and Foo to Foo01 Foo2 Foo03 etc.
    list(LENGTH allTargets allTargetsLen)
    if(${allTargetsLen} GREATER 0)

        # Want to have everything in nice order e.g. Bar0, Bar01, Bar03, Foo01, Foo02, Starter, ...
        list(SORT allTargets)
        
        # We will keep track of a group with a name like "Bar" and members "Bar01,Bar02,..."
        set(groupTarget "")     # Initial value
        set(groupList "")       # Initial value

        # Go through the targets and extract the groups and their members
        foreach(exec ${allTargets})

            # Given a name like Foo01 or Foo2 or Foo_12 the following regex will extract the string Foo
            nzzn_group_name_for(${exec} execGroup)

            # OK we got our "Foo". Do we need to close out the current group and start a new one?
            if(NOT ${execGroup} STREQUAL groupTarget)

                # How many targets are in the current group (say it's called Bar)?
                list(LENGTH groupList groupLen)

                # Create a target for Bar if that is worth our while (i.e. more than one sub-target)
                if(${groupLen} GREATER 1)
                    set(__targetName ${groupTarget})
                    __nzzn_resolve_any_target_conflicts()
                    add_custom_target(${__targetName}  DEPENDS ${groupList})
                endif()

                # Start the new group with no members in its corresponding list as yet
                set(groupTarget ${execGroup})
                set(groupList "")

            endif()

            # Have an appropriate group to add this particular target to
            list(APPEND groupList ${exec})
    
        endforeach()
        
        # Finally we create an overall cumulative target 'dir' that expands to Foo01, Foo02, Bar01, Bar02, Bar03 etc.
        set(__targetName ${dir})
        __nzzn_resolve_any_target_conflicts()
	    add_custom_target(${__targetName}  DEPENDS ${allTargets})

    endif()

endfunction()


