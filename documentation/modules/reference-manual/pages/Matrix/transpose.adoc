include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Transpose a Bit-Matrix

:stem: latexmath

Member function to transpose a _square_ bit-matrix _in place_ and a free function that transposes an arbitrary bit-matrix.

[source,cpp]
----
constexpr Matrix &to_transpose();                       // <.>

template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
transpose(const Matrix<Block, Allocator> &M);           // <.>
----
<.> Member function to transpose a _square_ bit-matrix in place.
<.> Free function that returns the transpose of an arbitrary bit-matrix.

The transpose of a matrix stem:[M] with elements stem:[M_{ij}] is the matrix stem:[M^T] whose elements are
[stem]
++++
M^T_{ij} = M_{ji}
++++

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m(4, [](std::size_t i, std::size_t) { return (i + 1)%2; });
    auto m1 = m;
    std::cout << "Original and transposed matrices:\n";
    GF2::print(m, m1.to_transpose());

    GF2::Matrix<> m2(4, 8, [](std::size_t i, std::size_t) { return (i + 1)%2; });
    std::cout << "Original and transposed matrices:\n";
    GF2::print(m2, GF2::transpose(m2));
}
----

.Output
[source]
----
Original and transposed matrices:
1111    1010
0000    1010
1111    1010
0000    1010
Original and transposed matrices:
11111111        1010
00000000        1010
11111111        1010
00000000        1010
                1010
                1010
                1010
                1010
----
