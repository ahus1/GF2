/// @brief Look at setting/resetting/flipping blocks of bits for the `Vector` class.
#include "common.h"

int
main()
{
    using Block = uint8_t;

    std::size_t n = 133;
    std::size_t i = 0;

    GF2::Vector<Block> v(n);

    std::cout << "Setting ranges of elements to 1:\n";
    v.reset();
    std::cout << "Starting with vector of size " << v.size() << ": " << v << "\n";
    for (i = 0; i < v.size(); ++i) {
        std::size_t maxLen = v.size() - i + 1;
        for (std::size_t len = 1; len < maxLen; ++len) {
            v.reset();
            std::cout << "Setting " << len << " element(s) starting at position: " << i << ": " << v.set(i, len) << "\n";
        }
    }
    std::cout << std::endl;

    std::cout << "Setting ranges of elements to 0:\n";
    v.set();
    std::cout << "Starting with vector of size " << v.size() << ": " << v << "\n";
    for (i = 0; i < v.size(); ++i) {
        std::size_t maxLen = v.size() - i + 1;
        for (std::size_t len = 1; len < maxLen; ++len) {
            v.set();
            std::cout << "Resetting " << len << " element(s) starting at position: " << i << ": " << v.reset(i, len) << "\n";
        }
    }
    std::cout << std::endl;

    std::cout << "Flipping ranges of elements from 1 to 0:\n";
    v.set();
    std::cout << "Starting with vector of size " << v.size() << ": " << v << "\n";
    for (i = 0; i < v.size(); ++i) {
        std::size_t maxLen = v.size() - i + 1;
        for (std::size_t len = 1; len < maxLen; ++len) {
            v.set();
            std::cout << "Flipping " << len << " element(s) starting at position: " << i << ": " << v.flip(i, len) << "\n";
        }
    }
    std::cout << std::endl;

    return 0;
}
