#include <GF2/GF2.h>
int 
main()
{
    std::size_t m = 12;

    auto A = GF2::Matrix<>::random(m);
    auto lu = GF2::lu_decompose(A);
    auto L = lu.L();
    auto U = lu.U();
    std::cout << "Matrix A, L, and U:\n";
    GF2::print(A, L, U);
    std::cout << "A is singular? " << (lu.singular() ? "YES" : "NO") << "\n";

    // Check that P.A = L.U
    auto PA = A;
    lu.permute(PA);
    auto LU = GF2::dot(L,U);
    std::cout << "P.A and L.U:\n";
    GF2::print(PA, LU);
    std::cout << "P.A == L.U? " << (PA == LU ? "YES" : "NO") << "\n";
}