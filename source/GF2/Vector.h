/// @file Vector.h
/// @brief A bit-vector class.
////
/// These are vectors over GF(2), the Galois field of two elements typically denoted 0 and 1 (or booleans
/// true, false, or bits set, unset). Arithmetic for GF(2) is defined mod 2 which has the effect that
/// addition/subtraction is replaced with `XOR` while multiplication/division becomes `AND`.
///
/// This class is a cross between a `std::vector` and a `std::bitset` along with some extra useful features.
///
/// However, while a `std::bitset` has a fixed size determined at compile time and by default, unsurprisingly prints in
/// bit-order (so e.g. if the bitset has size 4 it will print as `b3,b2,b1,b0`) our bit-vector class is dynamic
/// with a size determined at runtime and by default prints in vector-order (so in that last example `b0,b1,b2,b3`).
///
/// Vector elements here are packed in an array of some unsigned integer type that is a template parameter which
/// defaults to 64-bit words. Most of the methods defined in the bit-vector class operate on whole words at a time so
/// should be quite efficient.
///
/// See the full documentation for all the details.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include "assert.h"

#include <bit>
#include <bitset>
#include <chrono>
#include <concepts>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <vector>

namespace GF2 {

/// @brief A class for a vector over GF(2), the space of two elements {0, 1} with arithmetic done mod 2.
/// @tparam Block We pack the elements of the bit-vector in an array of these (some unsigned integer type).
/// @tparam Allocator is the memory manager that is used to allocate/deallocate space for the blocks as needed.
template<std::unsigned_integral Block = uint64_t, typename Allocator = std::allocator<Block>>
class Vector {

public:
    /// @brief A special `std::size_t` value that indicates a not-found/no-such-position failure for some methods below.
    static constexpr std::size_t npos = std::numeric_limits<std::size_t>::max();

    /// @brief Create a bit-vector of `n` elements all set to 0. Defaults to an empty size 0 vector.
    constexpr explicit Vector(std::size_t n = 0) : m_size(n), m_blocks(blocks_needed(n)) {}

    /// @brief Create a bit-vector from an initializer list of words.
    /// @tparam Src The type of the words for the source bits (could e.g. be unsigned or unsigned char etc.)
    /// @note The `Src` words need not be the same type as our storage type `Block`
    template<std::unsigned_integral Src = Block>
    constexpr explicit Vector(std::initializer_list<Src> src) : Vector()
    {
        append(src);
    }

    /// @brief Create a bit-vector from a `std::vector<Src>` of words
    /// @tparam Src The type of the words for the source bits (could e.g. be unsigned or unsigned char etc.)
    /// @note The `Src` words need not be the same type as our storage type `Block`
    template<std::unsigned_integral Src>
    constexpr explicit Vector(const std::vector<Src> &src) : Vector()
    {
        append(src);
    }

    /// @brief Create a bit-vector from an iteration of words treated as bits.
    /// @note The `value_type` associated with the iterator `Iter` should be an unsigned type
    template<typename Iter>
    constexpr Vector(Iter b, Iter e) : Vector()
    {
        append(b, e);
    }

    /// @brief Create a bit-vector from a `std::bitset`
    template<std::size_t N>
    explicit constexpr Vector(const std::bitset<N> &bs) : Vector()
    {
        append(bs);
    }

    /// @brief Create a bit-vector from calls to `f(i)` for `i = 0,...,n-1` where a non-zero return indicates a set bit.
    explicit constexpr Vector(std::size_t n, std::invocable<std::size_t> auto f) : Vector(n)
    {
        for (std::size_t i = 0; i < n; ++i)
            if (f(i) != 0) set(i);
    }

    /// @brief Create a bit-vector from bits encoded in a string (generally either all 0's & 1's or all hex chars).
    /// @param src The source string which optionally can be prefixed by "0b"/"0B", "0x"/"0X" for binary, hex.
    /// For hex strings there can also be a suffix (one of "_2", "_4", or "_8") which is interpreted as the base for the
    /// last character in the string, so `0x1 == 0b0001`, but `0x1_8 == 0b001`, `0x1_4 == 0b01`, and `0x1_2 == 0b1`.
    /// Without any suffix all hex-strings will give rise to vectors with a size divisible by 4.
    /// @param bit_order If true (default is false) the string will have the lowest bit on its right.
    /// The `bit_order` argument is completely IGNORED for hex strings!!
    /// @throw This method throws a `std::invalid_argument` exception if the string is not recognized
    explicit Vector(std::string_view src, bool bit_order = false) : Vector() { append(src, bit_order); }

    /// @brief Factory method to generate a bit-vector of size `n` where the elements are all 1.
    static constexpr Vector ones(std::size_t n)
    {
        Vector retval(n);
        retval.set();
        return retval;
    }

    /// @brief Factory method to generate a bit-vector of size `n` where the elements are all 0.
    static constexpr Vector zeros(std::size_t n)
    {
        Vector retval(n);
        return retval;
    }

    /// @brief Factor to generate a bit-vector of size `n` where the elements are either 1010101... or 0101010...
    static constexpr Vector checker_board(std::size_t n, int first = 0)
    {
        return first != 0 ? Vector(n, [](size_t k) { return (k + 1) % 2; }) : Vector(n, [](size_t k) { return k % 2; });
    }

    /// @brief Class method that returns the result of flipping a coin. Used by the `random(...)` method below etc.
    /// @param p is the probability of getting heads/true. Coin defaults to fair where p = 50%.
    static bool coin_flip(double p = 0.5)
    {
        // Need a valid probability ...
        if (p < 0 || p > 1) throw std::invalid_argument("Probability outside valid range [0,1]!");

        // Create a single simple Knuth style linear congruential generator seeded to a clock dependent state.
        using lcg = std::linear_congruential_engine<uint64_t, 6364136223846793005U, 1442695040888963407U, 0U>;
        static lcg rng(static_cast<lcg::result_type>(std::chrono::system_clock::now().time_since_epoch().count()));

        // Return the outcome of the coin flip.
        return std::bernoulli_distribution{p}(rng);
    }

    /// @brief Factory method to generate a bit-vector of size `n` where the elements are from independent coin flips.
    /// @param p The probability of the elements being 1 (defaults to a fair coin i.e. 50-50).
    static Vector random(std::size_t n, double p = 0.5)
    {
        return Vector(n, [p](std::size_t) { return coin_flip(p); });
    }

    /// @brief Factory method to create a bit-vector from any unsigned type word.
    /// @note This isn't a constructor because we don't want `src` to be treated as the number of vector elements!
    static constexpr Vector from(std::unsigned_integral auto src)
    {
        Vector retval;
        retval.append(src);
        return retval;
    }

    /// @brief Returns the number of elements in this bit-vector.
    constexpr std::size_t size() const { return m_size; }

    /// @brief Returns true if the  size of this bit-vector is zero.
    constexpr bool empty() const { return size() == 0; }

    /// @brief What is the capacity of this bit-vector (how many bits can it hold without allocating memory)?
    constexpr std::size_t capacity() const { return bits_per_block * m_blocks.capacity(); }

    /// @brief What is the size in bits of any unused capacity in this bit-vector?
    constexpr std::size_t unused() const { return capacity() - size(); }

    /// @brief Increase the capacity to hold @c n elements. Does nothing if @c n fits inside the current capacity.
    /// @note If capacity increases, then @c size() and old values stay the same, references etc. are invalidated.
    constexpr Vector &reserve(std::size_t n)
    {
        m_blocks.resize(blocks_needed(n));
        return *this;
    }

    /// @brief Request to minimize the unused/excess capacity
    constexpr Vector &shrink_to_fit()
    {
        m_blocks.resize(blocks_needed(size()));
        m_blocks.shrink_to_fit();
        return *this;
    }

    /// @brief Resize the bit-vector padding out any extra values with 0's.
    /// If `n < size()` the bit-vector is reduced to the first `n` elements.
    /// If `n > size()` then the extra appended values are zeros.
    constexpr Vector &resize(std::size_t n)
    {
        // Trivial case ...
        if (size() == n) return *this;

        // Perhaps we need to increase/decrease the size of the underlying data (increases are zeros)
        auto need = blocks_needed(n);
        if (auto have = m_blocks.size(); have != need) m_blocks.resize(need);

        // Record the new size and clean up the last occupied word if necessary.
        m_size = n;
        clean();
        return *this;
    }

    /// @brief Removes all elements from the bit-vector so `size() == 0`. Capacity is not changed!
    constexpr Vector &clear()
    {
        resize(0);
        return *this;
    }

    /// @brief Read access to the allocator for this bit-vector
    constexpr Allocator allocator() const { return m_blocks.allocator(); }

    /// @brief Read-write access to the underlying vector of blocks (use at you own risk!!)
    constexpr auto &blocks() { return m_blocks; }

    /// @brief Read-only access to the underlying vector of blocks (use at you own risk!!)
    constexpr const auto &blocks() const { return m_blocks; }

    /// @brief Swap the values of elements at locations i & j
    constexpr Vector &swap_elements(std::size_t i, std::size_t j)
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        gf2_debug_assert(j < m_size, "index j = " << j << " must be < m_size which is " << m_size);
        if (test(i) != test(j)) {
            flip(i);
            flip(j);
        }
        return *this;
    }

    /// @brief Swap the bits of this bit-vector with another
    constexpr Vector &swap(Vector &other) noexcept
    {
        std::swap(m_size, other.m_size);
        std::swap(m_blocks, other.m_blocks);
        return *this;
    }

    /// @brief Removes the last element from the bit-vector & shrinks it if possible
    constexpr Vector &pop()
    {
        if (!empty()) {

            // Zap that last element that we are about to lose to  keep things clean.
            reset(m_size - 1);

            // Shrink the bit-vector
            --m_size;

            // Perhaps we can also shrink the storage data ?
            if (m_blocks.size() > blocks_needed(m_size)) m_blocks.pop_back();
        }
        return *this;
    }

    /// @brief Adds a single element to the end of this bit-vector. Element will be 0 unless the argument is `true`
    constexpr Vector &push(bool one = false)
    {
        // Perhaps we need more storage?
        m_blocks.resize(blocks_needed(m_size + 1));

        // Increase the size of the bit-vector and set the new last bit if appropriate
        ++m_size;
        if (one) set(m_size - 1);
        return *this;
    }

    /// @brief We have several `append(...)` methods so add a synonym for the push method above
    constexpr Vector &append(bool b) { return push(b); }

    /// @brief Append another bit-vector to the end of this one
    constexpr Vector &append(const Vector &v)
    {
        // Trivial case?
        if (v.size() == 0) return *this;

        // Resize the underlying block store so we have enough space to accommodate the extra bits
        m_blocks.resize(blocks_needed(m_size + v.m_size));

        // Where is starting location for where the new bits need to go?
        auto ni = next_index();
        auto nb = next_bit();

        // Easy case: If we're at the start of a new block we can just copy the blocks in v over to the new space
        if (nb == 0) {
            for (std::size_t i = 0; i < v.m_blocks.size(); ++i) m_blocks[ni + i] = v.m_blocks[i];
            m_size += v.m_size;
            return *this;
        }

        // Otherwise we may have to put each block from v in two contiguous blocks here
        for (std::size_t i = 0; i < v.m_blocks.size(); ++i) {

            // Split the i'th block of v into a left and right group of bits
            // Note that it may be the case that what's left of v may fit into the left block alone!
            auto l = static_cast<Block>(v.m_blocks[i] << nb);
            auto r = static_cast<Block>(v.m_blocks[i] >> (bits_per_block - nb));

            // Add the left group to our current block 
            m_blocks[ni + i] |= l;
            
            // If necessary add the right group to the next block along in our store
            // Note that if it doesn't fit its not needed!
            if(ni + i + 1 < m_blocks.size()) m_blocks[ni + i + 1] = r;
        }

        // Record the new size and go home
        m_size += v.m_size;
        return *this;
    }

    /// @brief Append a whole word of bits to the end of this bit-vector.
    /// @tparam Src The type of the word for the source bits (could e.g. be unsigned or unsigned char etc.)
    /// @note The type of the `src` word need not match the the type of the blocks we are using as underlying storage.
    template<std::unsigned_integral Src>
    constexpr Vector &append(Src src)
    {
        // Resize the underlying data of blocks so we have enough space to accommodate the extra bits
        constexpr std::size_t bits_per_src = std::numeric_limits<Src>::digits;
        m_blocks.resize(blocks_needed(m_size + bits_per_src));

        if constexpr (bits_per_src > bits_per_block) {

            // Handle long source words by splitting them into shorter ones and recursing ...
            // The source needs to be some even multiple of the blocks in bits or something is off!
            static_assert(bits_per_src % bits_per_block == 0, "Cannot pack the source evenly into an array of blocks!");
            constexpr std::size_t blocks_per_src = bits_per_src / bits_per_block;

            // Mask out the appropriate number of bits in the source to create shorter words to push one at a time
            auto mask = static_cast<Src>(ones<Block>());
            for (std::size_t i = 0; i < blocks_per_src; ++i, src >>= bits_per_block) {
                auto tmp = static_cast<Block>(src & mask);
                append(tmp);
            }
            return *this;
        }
        else {
            // Here we know that sizeof(src) << sizeof(Block) so we can safely cast src to a Block
            auto tmp = static_cast<Block>(src);

            // Where are we putting things?
            auto ni = next_index();
            auto nb = next_bit();

            // If we're at the start of a new block we can just copy the src over.
            if (nb == 0) {
                m_blocks[ni] = tmp;
                m_size += bits_per_src;
                return *this;
            }

            // Otherwise push as many src bits as possible into the current block.
            std::size_t available = bits_per_block - nb;
            m_blocks[ni] |= Block(tmp << nb);

            // If there are bits left in the source word they go at the start of the next block
            if (bits_per_src > available) m_blocks[ni + 1] = tmp >> available;
            m_size += bits_per_src;
            return *this;
        }
    }

    /// @brief Add a whole list of source words as bits to the end of the bit-vector.
    /// @tparam Src The type of the words for the source bits (could e.g. be unsigned or unsigned char etc.)
    template<std::unsigned_integral Src = Block>
    constexpr Vector &append(std::initializer_list<Src> src)
    {
        return append(std::cbegin(src), std::cend(src));
    }

    /// @brief Add a `std::vector<Src>` of source words as bits to the end of the bit-vector.
    /// @tparam Src The type of the words for the source bits (could e.g. be unsigned or unsigned char etc.)
    template<std::unsigned_integral Src>
    constexpr Vector &append(const std::vector<Src> &src)
    {
        return append(std::cbegin(src), std::cend(src));
    }

    /// @brief Add an iterated collection of words as bits to the end of the bit-vector.
    /// @note The `value_type` of the `Iter` should be some unsigned integer type.
    template<typename Iter>
    constexpr Vector &append(Iter b, Iter e)
    {
        // How many words are we adding? Handle the trivial case of 0
        auto n_src = static_cast<std::size_t>(std::distance(b, e));
        if (n_src == 0) return *this;

        // Resize the underlying data of blocks so we have enough space so we can accommodate the extra bits
        using SrcType = typename std::iterator_traits<Iter>::value_type;
        std::size_t num_bits = n_src * std::numeric_limits<SrcType>::digits;
        m_blocks.resize(blocks_needed(m_size + num_bits));

        // Run through the source words and add each one
        for (auto it = b; it < e; ++it) append(*it);
        return *this;
    }

    /// @brief Add a `std::bitset` to the end of this bit-vector.
    /// @tparam N The size of the `std::bitset` which should be deduced in any case.
    template<std::size_t N>
    constexpr Vector &append(const std::bitset<N> &src)
    {
        // We do this the dumb way ...
        for (std::size_t i = 0; i < N; ++i) push(src[i]);
        return *this;
    }

    /// @brief Append bits that are encoded as string (generally either all 0's & 1's or all hex chars).
    /// @param src The source string which optionally can be prefixed by "0b"/"0B", "0x"/"0X" for binary, hex.
    /// For hex strings there can also be a suffix (one of "_2", "_4", or "_8") which is interpreted as the base for the
    /// last character in the string, so `0x1 == 0b0001`, but `0x1_8 == 0b001`, `0x1_4 == 0b01`, and `0x1_2 == 0b1`.
    /// Without any suffix all hex-strings will give rise to bit-vectors with a size divisible by 4.
    /// @param bit_order If true (default is false) the string will have the lowest bit on the right.
    /// The `bit_order` parameter is completely IGNORED for hex strings!
    /// @throw This method throws a `std::invalid_argument` exception if the string is not recognized
    Vector &append(std::string_view src, bool bit_order = false)
    {
        // Perhaps the string has a binary or hex marker in front?
        if (src.length() > 1) {
            if (src.starts_with("0b") || src.starts_with("0B")) return append_binary_string(src, bit_order);
            if (src.starts_with("0x") || src.starts_with("0X")) return append_hex_string(src);
        }

        // No prefix to give us a clue but if the contents are all 0's and 1's we assume it's a binary string
        if (src.find_first_not_of("01") == std::string::npos) return append_binary_string(src, bit_order);

        // No luck with any of that? It must be a hex string!
        return append_hex_string(src);
    }

    /// @brief Create a new bit-vector as a distinct sub-vector of this one.
    /// @param begin Starting point to extract the sub-vector from.
    /// @param len The number of elements in the sub-vector.
    /// @return A completely new bit-vector of size `len`
    constexpr Vector sub(std::size_t begin, std::size_t len) const
    {
        // Trivial case?
        if (empty() || len == 0) return Vector{};

        // GF2_DEBUG builds will check the starting position of the sub-vector
        gf2_debug_assert(begin < m_size, "begin = " << begin << ", m_size  = " << m_size);

        // GF2_DEBUG builds will check whether the sub-vector fits (the length of the sub-vector)
        std::size_t end = begin + len;
        gf2_debug_assert(end <= m_size, "begin = " << begin << ", len = " << len << " so end = " << end
                                                   << ", but m_size = " << m_size);

        // Create the right sized rub-vector all initialized to 0's
        Vector retval(len);

        // Where does the begin bit of the sub-vector lie in our block data?
        std::size_t fi = block_index(begin);
        std::size_t fb = bit_index(begin);
        std::size_t fb_complement = bits_per_block - fb;

        // Where does the last bit of the sub-vector lie in our block data?
        std::size_t last = end - 1;
        std::size_t li = block_index(last);

        if (fb == 0) {
            // Can copy over whole blocks and deal with any extra bits copied with a clean()
            for (std::size_t i = fi; i <= li; ++i) retval.m_blocks[i - fi] = m_blocks[i];
        }
        else {
            // Not working on word boundaries (each block in the sub-vector is a combination of two of our blocks)
            for (std::size_t i = fi; i < li; ++i) {
                auto l = static_cast<Block>(m_blocks[i] >> fb);
                auto r = static_cast<Block>(m_blocks[i + 1] << fb_complement);
                retval.m_blocks[i - fi] = l | r;
            }

            // Do we still need to grab the last few bits?
            std::size_t copied = (li - fi) * bits_per_block;
            if (copied < len) {
                for (std::size_t i = copied; i < len; ++i)
                    if (test(begin + i)) retval.set(i);
            }
        }

        // Might have over-copied on the last block so do a cleanup
        retval.clean();
        return retval;
    }

    /// @brief Create a new bit-vector as a distinct sub-vector of this one.
    /// @param len Length to extract (positive means from start, negative means from end).
    /// @return A completely new bit-vector of size `abs(len)`
    constexpr Vector sub(int len) const
    {
        // Trivial case?
        if (empty() || len == 0) return Vector{};

        // Optionally check the range
        auto alen = std::size_t(abs(len));

        std::size_t begin = len > 0 ? 0 : m_size - alen;
        return sub(begin, alen);
    }
    
    /// @brief Write access to an individual bit in a bit-vector is through this `Vector::reference` proxy class.
    /// It supports all the usual bit type operations (reading as a `bool`, setting, un-setting, flipping, ...)
    class reference {
    public:
        constexpr reference(Vector &v, std::size_t bit) : m_block_ref(v.block_ref(bit)), m_bit_mask(v.block_mask(bit))
        {
            // Empty body
        }

        // The default compiler generator implementations for most "rule of 5" methods will be fine ...
        constexpr reference(const reference &) = default;
        constexpr reference(reference &&) noexcept = default;
        ~reference() = default;

        // Cannot data a reference to a reference ...
        constexpr void operator&() = delete;

        // Have custom operator='s
        constexpr reference &operator=(const reference &rhs)
        {
            set_to(rhs.to_bool());
            return *this;
        }
        constexpr reference &operator=(reference &&rhs) noexcept
        {
            set_to(rhs.to_bool());
            return *this;
        }
        constexpr reference &operator=(bool rhs)
        {
            set_to(rhs);
            return *this;
        }
        constexpr reference &set_to(bool rhs)
        {
            rhs ? set() : reset();
            return *this;
        }
        constexpr reference &set()
        {
            m_block_ref |= m_bit_mask;
            return *this;
        }
        constexpr reference &reset()
        {
            m_block_ref &= Block(~m_bit_mask);
            return *this;
        }
        constexpr reference &flip()
        {
            m_block_ref ^= m_bit_mask;
            return *this;
        }

        // Bitwise operations with a right-hand-side bit
        constexpr reference &operator&=(bool rhs)
        {
            if (!rhs) reset();
            return *this;
        }
        constexpr reference &operator|=(bool rhs)
        {
            if (rhs) set();
            return *this;
        }
        constexpr reference &operator^=(bool rhs)
        {
            if (rhs) flip();
            return *this;
        }
        constexpr reference &operator-=(bool rhs)
        {
            if (rhs) reset();
            return *this;
        }
        constexpr reference &operator~() const { return flip(); }

        // Explicitly convert to a bool
        constexpr bool to_bool() const { return (m_block_ref & m_bit_mask); }

        // Actually you can always use a Vector::reference as a bool
        constexpr operator bool() const { return to_bool(); }

    private:
        Block &m_block_ref;
        Block  m_bit_mask;
    };

    /// @brief Read-write access to the element at index `i`
    constexpr reference element(std::size_t i)
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        return reference(*this, i);
    }

    /// @brief Read-only access to the element at index `i`
    constexpr bool element(std::size_t i) const
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        return test(i);
    }

    /// @brief Read-write access to the element at index `i`
    constexpr reference operator[](std::size_t i) { return element(i); }

    /// @brief Read-only access to the element at index `i`
    constexpr bool operator[](std::size_t i) const { return element(i); }

    /// @brief Read-write access to the element at index `i`
    constexpr reference operator()(std::size_t i) { return element(i); }

    /// @brief Read-only access to the element at index `i`
    constexpr bool operator()(std::size_t i) const { return element(i); }

    /// @brief Check whether the element at index `i` is set
    constexpr bool test(std::size_t i) const
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        return block_ref(i) & block_mask(i);
    }

    /// @brief Check whether all the elements in the bit-vector are set.
    constexpr bool all() const
    {
        // Handle empty vectors with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty vector is likely an error!");

        // Handling of empty vectors is a bit arbitrary but needs mu\st ...
        // Deliberately inconsistent returns from all(), any(), none() for empty vectors
        if (empty()) return true;

        auto nb = next_bit();
        if (nb == 0) {
            // Everything is stored in full blocks so check each one
            for (std::size_t i = 0; i < m_blocks.size(); ++i)
                if (m_blocks[i] != ones()) return false;
        }
        else {
            // Check the last partially used storage block
            auto ni = next_index();
            if (m_blocks[ni] != ones() >> (bits_per_block - nb)) return false;

            // And check any earlier full blocks also ...
            if (ni > 0) {
                for (std::size_t i = 0; i < ni - 1; ++i)
                    if (m_blocks[i] != ones()) return false;
            }
        }
        return true;
    }

    /// @brief Check whether any of the elements in the bit-vector are set.
    constexpr bool any() const
    {
        // Handle empty vectors with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty vector is likely an error!");

        // Note for an empty vector we return false but all() returns true! Deliberately inconsistent
        for (auto b : m_blocks)
            if (b != 0) return true;
        return false;
    }

    /// @brief Check whether none of of the elements in the bit-vector are set.
    constexpr bool none() const
    {
        // Handle empty vectors with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty vector is likely an error!");

        // Note for an empty vector we return true as does all()! Deliberately inconsistent
        return !any();
    }

    /// @brief Returns the number of set elements in the bit-vector.
    constexpr std::size_t count1() const
    {
        // NOTE: We have been careful to keep the excess bits in the last block all at 0
        std::size_t sum = 0;
        for (auto b : m_blocks) sum += static_cast<std::size_t>(std::popcount(b));
        return sum;
    }

    /// @brief Returns the number of unset elements in the bit-vector.
    constexpr std::size_t count0() const { return m_size - count1(); }

    /// @brief Returns the number of set elements in the bit-vector. This is a synonym for `count1()`
    constexpr std::size_t count() const { return count1(); }

    /// @brief Returns the parity of the bit-vector (number of set bits mod 2).
    /// @note Tested some other algorithms but `std::popcount` seems pretty good on our platforms
    constexpr bool parity() const { return count1() % 2; }

    /// @brief Returns the index of the first set element or `npos` if there are none set.
    constexpr std::size_t first() const
    {
        for (std::size_t i = 0; i < m_blocks.size(); ++i)
            if (m_blocks[i] != 0) return i * bits_per_block + lsb(m_blocks[i]);

        // No luck!
        return npos;
    }

    /// @brief Returns the index of the last set element or `npos` if there are none set.
    constexpr std::size_t last() const
    {
        std::size_t i = m_blocks.size();
        while (i != 0) {
            if (m_blocks[--i] != 0) return i * bits_per_block + msb(m_blocks[i]);
        }

        // No luck!
        return npos;
    }

    /// @brief Returns the index of the next set bit after the argument or `npos` if there are no more set bits
    constexpr std::size_t next(std::size_t pos) const
    {
        // Trivial case?
        if (empty() || pos >= m_size - 1) return npos;

        // Start our search at element (pos + 1)
        std::size_t p = pos + 1;
        std::size_t pi = block_index(p);
        std::size_t pb = bit_index(p);

        // Perhaps the block at pi has a more significant set bit?
        if (Block hi = m_blocks[pi] & Block(ones() << pb); hi != 0) return pi * bits_per_block + lsb(hi);

        // No luck? Try the remaining blocks in order
        for (std::size_t i = pi + 1; i < m_blocks.size(); ++i)
            if (m_blocks[i] != 0) return i * bits_per_block + lsb(m_blocks[i]);

        // Still no luck?
        return npos;
    }

    /// @brief Returns the index of the previous set bit before the argument or `npos` if there are none
    constexpr std::size_t prev(std::size_t pos) const
    {
        // Trivial case?
        if (empty() || pos == 0) return npos;

        // Silently fix a very large argument
        if (pos >= m_size) pos = m_size;

        // Start our search at element (pos - 1)
        std::size_t p = pos - 1;
        std::size_t pi = block_index(p);
        std::size_t pb = bit_index(p);

        // Perhaps the block at pi has a less significant set bit?
        if (Block lo = m_blocks[pi] & Block(ones() >> (bits_per_block - pb)); lo != 0)
            return pi * bits_per_block + msb(lo);

        // No luck? Try the remaining blocks in order
        std::size_t i = p;
        while (i != 0) {
            if (m_blocks[--i] != 0) return i * bits_per_block + msb(m_blocks[i]);
        }

        // Still no luck?
        return npos;
    }

    /// @brief Set the element at index `i` to 1.
    constexpr Vector &set(std::size_t i)
    {
        gf2_debug_assert(i < m_size, "index `i` = " << i << " must be < `m_size` which is " << m_size);
        block_ref(i) |= block_mask(i);
        return *this;
    }

    /// @brief Set `len` elements starting at `first` to 1.
    constexpr Vector &set(std::size_t first, std::size_t len)
    {
        // Check index ranges if appropriate and also handle the trivial case where len == 0
        gf2_debug_assert(first < m_size, "first = " << first << ", m_size = " << m_size);
        if (len == 0) return *this;
        std::size_t last = first + len - 1;
        gf2_debug_assert(last < m_size, "len = " << len << " so last = " << last << ", m_size = " << m_size);

        // Locations of the first and last elements in our block store.
        std::size_t fi = block_index(first);
        std::size_t li = block_index(last);

        // Useful masks
        auto  f_mask = static_cast<Block>(ones() << bit_index(first));
        Block l_mask = ones() >> (bits_per_block - bit_index(last) - 1);

        // Perhaps the range of interest lies in a single block?
        if (fi == li) {
            m_blocks[fi] |= (f_mask & l_mask);
            return *this;
        }

        // The range of interest lies over more than one block
        m_blocks[fi] |= f_mask;
        for (std::size_t i = fi + 1; i < li; ++i) m_blocks[i] = ones();
        m_blocks[li] |= l_mask;

        return *this;
    }

    /// @brief Set all the elements in the bit-vector to 1
    constexpr Vector &set()
    {
        for (auto &block : m_blocks) block = ones();
        clean();
        return *this;
    }

    /// @brief Reset the element at index `i` to 0
    constexpr Vector &reset(std::size_t i)
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        block_ref(i) &= Block(~block_mask(i));
        return *this;
    }

    /// @brief Reset `len` elements starting at `first` to 0.
    constexpr Vector &reset(std::size_t first, std::size_t len)
    {
        // Check index ranges if appropriate and also handle the trivial case where len == 0
        gf2_debug_assert(first < m_size, "first = " << first << ", m_size = " << m_size);
        if (len == 0) return *this;
        std::size_t last = first + len - 1;
        gf2_debug_assert(last < m_size, "len = " << len << " so last = " << last << ", m_size = " << m_size);

        // Locations of the first and last elements in our data
        std::size_t fi = block_index(first);
        std::size_t li = block_index(last);

        // Useful masks
        auto  f_mask = static_cast<Block>(ones() << bit_index(first));
        Block l_mask = ones() >> (bits_per_block - bit_index(last) - 1);

        // Perhaps the range of interest lies in a single block?
        if (fi == li) {
            m_blocks[fi] &= Block(~(f_mask & l_mask));
            return *this;
        }

        // The range of interest lies over more than one block
        m_blocks[fi] &= Block(~f_mask);
        for (std::size_t i = fi + 1; i < li; ++i) m_blocks[i] = 0;
        m_blocks[li] &= Block(~l_mask);

        return *this;
    }

    /// @brief Reset all the elements in the bit-vector to 0
    constexpr Vector &reset()
    {
        for (auto &block : m_blocks) block = 0;
        return *this;
    }

    /// @brief Flip the element at `i`
    constexpr Vector &flip(std::size_t i)
    {
        gf2_debug_assert(i < m_size, "index i = " << i << " must be < m_size which is " << m_size);
        block_ref(i) ^= block_mask(i);
        return *this;
    }

    /// @brief Flip `len` elements starting at `first`
    constexpr Vector &flip(std::size_t first, std::size_t len)
    {
        // Check index ranges if appropriate and also handle the trivial case where len == 0
        gf2_debug_assert(first < m_size, "first = " << first << ", m_size = " << m_size);
        if (len == 0) return *this;
        std::size_t last = first + len - 1;
        gf2_debug_assert(last < m_size, "len = " << len << " so last = " << last << ", m_size = " << m_size);

        // Locations of the first and last elements in our block store
        std::size_t fi = block_index(first);
        std::size_t li = block_index(last);

        // Useful masks
        auto  f_mask = static_cast<Block>(ones() << bit_index(first));
        Block l_mask = ones() >> (bits_per_block - bit_index(last) - 1);

        // Perhaps the range of interest lies in a single block?
        if (fi == li) {
            m_blocks[fi] ^= (f_mask & l_mask);
            return *this;
        }

        // The range of interest lies over more than one block
        m_blocks[fi] ^= f_mask;
        for (std::size_t i = fi + 1; i < li; ++i) m_blocks[i] = ~m_blocks[i];
        m_blocks[li] ^= l_mask;

        return *this;
    }

    /// @brief Flip all the elements in the bit-vector
    constexpr Vector &flip()
    {
        for (auto &block : m_blocks) block = ~block;
        clean();
        return *this;
    }

    /// @brief Set the element `i` to 1 if `f(i) != 0` otherwise set it to 0.
    /// @param f is function that we will call as `f(i)` for each index in the bit-vector.
    constexpr Vector &set_if(std::invocable<std::size_t> auto f)
    {
        reset();
        for (std::size_t i = 0; i < size(); ++i)
            if (f(i) != 0) set(i);
        return *this;
    }

    /// @brief Flip the value or element `i` if `f(i) != 0` otherwise leave it alone.
    /// @param f is function that we will call as `f(i)` for each index in the bit-vector.
    constexpr Vector &flip_if(std::invocable<std::size_t> auto f)
    {
        for (std::size_t i = 0; i < size(); ++i)
            if (f(i) != 0) flip(i);
        return *this;
    }

    /// @brief AND this bit-vector with another of equal size
    constexpr Vector &operator&=(const Vector &rhs)
    {
        gf2_debug_assert(size() == rhs.size(), "sizes don't match " << size() << " != " << rhs.size());
        for (std::size_t i = 0; i < m_blocks.size(); ++i) m_blocks[i] &= rhs.m_blocks[i];
        return *this;
    }

    /// @brief OR this bit-vector with another of equal size
    constexpr Vector &operator|=(const Vector &rhs)
    {
        gf2_debug_assert(size() == rhs.size(), "sizes don't match " << size() << " != " << rhs.size());
        for (std::size_t i = 0; i < m_blocks.size(); ++i) m_blocks[i] |= rhs.m_blocks[i];
        return *this;
    }

    /// @brief XOR this bit-vector with another of equal size
    constexpr Vector &operator^=(const Vector &rhs)
    {
        gf2_debug_assert(size() == rhs.size(), "sizes don't match " << size() << " != " << rhs.size());
        for (std::size_t i = 0; i < m_blocks.size(); ++i) m_blocks[i] ^= rhs.m_blocks[i];
        return *this;
    }

    /// @brief Get back a copy of this bit-vector with all the bits flipped
    constexpr Vector operator~() const
    {
        Vector retval{*this};
        retval.flip();
        return retval;
    }

    /// @brief "Add" another equal sized bit-vector to this one (in GF(2) addition == XOR)
    constexpr Vector &operator+(const Vector &rhs) { return operator^=(rhs); }

    /// @brief "Subtract" another equal sized bit-vector from this one (in GF(2) subtraction == XOR)
    constexpr Vector &operator-(const Vector &rhs) { return operator^=(rhs); }

    /// @brief Element-by-element "multiplication" of another equal sized bit-vector with this one (in GF(2) this is
    /// AND)
    constexpr Vector &operator*(const Vector &rhs) { return operator&=(rhs); }

    /// @brief Left-shift this bit-vector `p` places
    constexpr Vector &operator<<=(std::size_t p)
    {
        // Handle some trivial cases
        if (p == 0 || empty()) return *this;

        // Perhaps we have just shifted in all zeros?
        if (p >= m_size) {
            reset();
            return *this;
        }

        // Any shift is really (1) shift by whole blocks followed by (2) a shift by less than a whole block
        std::size_t block_shift = p / bits_per_block;
        std::size_t bit_shift = p % bits_per_block;

        // Do any whole block shifts first (pushing in whole blocks of zeros to fill the empty slots)
        std::size_t i = 0;
        if (block_shift > 0) {
            for (i = m_blocks.size() - 1; i >= block_shift; --i) m_blocks[i] = m_blocks[i - block_shift];
            for (i = 0; i < block_shift; ++i) m_blocks[i] = 0;
        }

        // Now handle any remaining shift of less than a whole block.
        if (bit_shift != 0) {
            std::size_t bit_shift_complement = bits_per_block - bit_shift;
            for (i = m_blocks.size() - 1; i > block_shift; --i) {
                auto l = static_cast<Block>(m_blocks[i] << bit_shift);
                auto r = static_cast<Block>(m_blocks[i - 1] >> bit_shift_complement);
                m_blocks[i] = l | r;
            }
            m_blocks[block_shift] <<= bit_shift;
        }

        return *this;
    }

    /// @brief Right-shift this bit-vector `p` places
    constexpr Vector &operator>>=(std::size_t p)
    {
        // Handle some trivial cases
        if (p == 0 || empty()) return *this;

        // Perhaps we have just shifted in all zeros?
        if (p >= m_size) {
            reset();
            return *this;
        }

        // The shift is really (1) shift by whole blocks followed by (2) a shift by less than a whole block
        std::size_t block_shift = p / bits_per_block;
        std::size_t bit_shift = p % bits_per_block;
        std::size_t block_end = m_blocks.size() - block_shift;

        // Do any whole block shifts first (pushing in whole blocks of zeros to fill the empty slots)
        std::size_t i = 0;
        if (block_shift > 0) {
            for (i = 0; i < block_end; ++i) m_blocks[i] = m_blocks[i + block_shift];
            for (i = block_end; i < m_blocks.size(); ++i) m_blocks[i] = 0;
        }

        // Now handle any remaining shift of less than a whole block.
        if (bit_shift != 0) {
            std::size_t bit_shift_complement = bits_per_block - bit_shift;
            for (i = 0; i < block_end - 1; ++i) {
                auto l = static_cast<Block>(m_blocks[i + 1] << bit_shift_complement);
                auto r = static_cast<Block>(m_blocks[i] >> bit_shift);
                m_blocks[i] = r | l;
            }
            m_blocks[block_end - 1] >>= bit_shift;
        }

        return *this;
    }

    /// @brief Get back a bit-vector that is this bit-vector left shifted by `p` places
    constexpr Vector operator<<(std::size_t p) const
    {
        Vector retval{*this};
        retval <<= p;
        return retval;
    }

    /// @brief Get back a bit-vector that is this bit-vector right shifted by `p` places
    constexpr Vector operator>>(std::size_t p) const
    {
        Vector retval{*this};
        retval >>= p;
        return retval;
    }

    /// @brief Starting at bit `i0` replace our values with those of a passed in replacement bit-vector.
    /// @param i0 is the location where we start putting the new sub-vector in place
    /// @param with The sub-vector we are putting in place -- it must fit in the existing structure!
    constexpr Vector &replace(std::size_t i0, const Vector &with)
    {
        // Trivial case?
        std::size_t ws = with.size();
        if (ws == 0) return *this;

        // Do a couple of optional sanity checks
        gf2_debug_assert(i0 < size(), "i0 = " << i0 << ", size() = " << size());
        gf2_debug_assert(i0 + ws - 1 < size(), "i0 = " << i0 << ", with.size() = " << ws << ", size() = " << size());

        // TODO: Replace this loop with something that works on blocks at a time!
        for (std::size_t i = 0; i < ws; ++i) operator[](i0 + i) = with[i];

        return *this;
    }

    /// @brief Starting at bit 0 replace our values with those of a passed in replacement bit-vector
    /// @param with The sub-vector we are putting in place -- it must fit in the existing structure!
    constexpr Vector &replace(const Vector &with) { return replace(0, with); }

    /// @brief Get a binary-string representation for this bit-vector using the given characters for set and unset.
    /// @param bit_order If true the the least significant bit v[0] will be on the right (default is the left).
    /// @param off The character to use for unset elements--defaults to '0'.
    /// @param on The character to use for set elements--defaults to '1'.
    std::string to_string(bool bit_order = false, char off = '0', char on = '1') const
    {
        // Trivial case?
        auto n = size();
        if (n == 0) return std::string{};

        // Otherwise we initialize a string of the correct size to all 'off'
        std::string retval(n, off);

        // Change the set bits to 'on'
        for (size_t i = 0; i < n; ++i)
            if (test(i)) retval[i] = on;

        // Perhaps the string is supposed to be in bit-order?
        if (bit_order) std::reverse(std::begin(retval), std::end(retval));

        return retval;
    }

    /// @brief Get a binary-string representation for this bit-vector in bit-order
    /// @param off The character to use for unset elements--defaults to '0'.
    /// @param on The character to use for set elements--defaults to '1'.
    std::string to_bit_order(char off = '0', char on = '1') const { return to_string(true, off, on); }

    /// @brief Get a hex-string representation for this bit-vector.
    /// @note If the size of the bit-vector is not a multiple of 4 there will be a suffix (one of "_2", "_4", or "_8").
    /// The suffix gives the base of the last character in the string. Thus the string "A0F1_4" says that the `A`, `0`,
    /// and `F` characters are hex (base 16) but that last `1` is actually base 4 so should be read as binary '01' as
    /// opposed to '0001' which is what it would be if you interpret the 1 as base 16 (i.e. no suffix)
    std::string to_hex() const
    {
        // Trivial case?
        auto n = size();
        if (n == 0) return std::string{};

        // Table of hex characters we use
        static char hex_char[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        // How is our size relative to multiples of 4 (each hex character naturally translates to 4 bits/elements)?
        std::size_t n_div_4 = n / 4;
        std::size_t n_mod_4 = n % 4;

        // Set up the return string with the correct amount of space
        std::string retval;
        retval.reserve(n_div_4 + 5);

        std::bitset<4> hex_bs;
        std::size_t    b = 0;
        while (b < n) {
            hex_bs.reset();
            for (std::size_t i = 0; i < 4 && b < n; ++i, ++b) hex_bs[i] = test(b);
            retval.push_back(hex_char[hex_bs.to_ulong()]);
        }

        // Will need a suffix if the size is not an even multiple of 4
        switch (n_mod_4) {
            case 1: retval += "_2"; break;
            case 2: retval += "_4"; break;
            case 3: retval += "_8"; break;
            default: break;
        }

        return retval;
    }

    /// @brief Convert the bit-vector to a single word (at least as much of it as fits in the word)
    /// @tparam Dst The (deduced) unsigned word-type you want as output (empty vectors give back Dst(0))
    /// @param dst E.g. if `dst` is a `uint32_t` it gets filled as far as is possible from the start of v
    template<std::unsigned_integral Dst>
    constexpr void to(Dst &dst) const
    {
        // Initialize (we also return 0 if this bit-vector is empty)
        dst = 0;
        if (empty()) return;

        // Handle different sized destinations (short ones take bits from one block, long ones can use more blocks)
        constexpr std::size_t bits_per_dst = std::numeric_limits<Dst>::digits;
        if constexpr (bits_per_dst == bits_per_block) {
            dst = static_cast<Dst>(m_blocks[0]);
            return;
        }
        else if constexpr (bits_per_dst < bits_per_block) {
            constexpr Block mask = ones<Dst>();
            dst = static_cast<Dst>(m_blocks[0] & mask);
            return;
        }
        else {
            // Long words should be an even multiple of the short ones or something very odd has happened!
            static_assert(bits_per_dst % bits_per_block == 0, "Cannot pack blocks evenly into the destination word!");

            // How many blocks will it take to fill dst? Of course that many may not be available
            std::size_t n = std::min(bits_per_dst / bits_per_block, size());
            std::size_t shift = 0;
            for (std::size_t i = 0; i < n; ++i, shift += bits_per_block) {
                auto src = static_cast<Dst>(m_blocks[i]);
                dst |= static_cast<Dst>(src << shift);
            }
            return;
        }
    }

    /// @brief Convert this bit-vector to a `std::vector<Dst>` of words of type `Dst`.
    /// @tparam Dst The output vector will be words of this type e.g. `uint32_t`
    template<std::unsigned_integral Dst>
    constexpr void to(std::vector<Dst> &v) const
    {
        // Correctly size the output vector (might be to 0)
        v.resize(words_needed<Dst>(size()));
        if (empty()) return;

        // Handle different sized destinations (short ones take bits from one block, long ones can use more blocks)
        constexpr std::size_t bits_per_dst = std::numeric_limits<Dst>::digits;
        if constexpr (bits_per_dst == bits_per_block) {
            for (std::size_t i = 0; i < m_blocks.size(); ++i) v[i] = static_cast<Dst>(m_blocks[i]);
        }
        else if constexpr (bits_per_dst < bits_per_block) {
            // Pushing the blocks into a a larger number of smaller destination words
            static_assert(bits_per_block % bits_per_dst == 0,
                          "Source blocks will not divide evenly into an array of destination words!");

            // How many Dst words fit in one Block?
            std::size_t dst_per_block = bits_per_block / bits_per_dst;

            // Create a Block word that masks out all but the rightmost bits_per_dst piece
            auto mask = static_cast<Block>(ones<Dst>());

            std::size_t i = 0;
            for (auto src : m_blocks) {
                for (std::size_t j = 0; j < dst_per_block; ++j, ++i, src >>= bits_per_dst)
                    v[i] = static_cast<Dst>(src & mask);
            }
        }
        else {

            // Pushing the blocks into a smaller number of larger destination words
            static_assert(bits_per_dst % bits_per_block == 0,
                          "Destination words are not each an even number of blocks!");

            // How many Blocks fit in one Dst word?
            std::size_t blocks_per_dst = bits_per_dst / bits_per_block;

            // Pop each block into an appropriate (word, bit) slot in the destination vector
            for (std::size_t i = 0; i < m_blocks.size(); ++i) {
                std::size_t index = i / blocks_per_dst;
                std::size_t shift = (i % blocks_per_dst) * bits_per_block;
                Dst         src = m_blocks[i];
                v[index] |= Dst(src << shift);
            }
        }
    }

    /// @brief Call `f(pos)` over just the set bits in the bit-vector in increasing order.
    /// @param f is a function that takes the the position of the set bit as its argument.
    constexpr void if_set_call(std::invocable<std::size_t> auto f) const
    {
        std::size_t pos = first();
        while (pos != npos) {
            f(pos);
            pos = next(pos);
        }
    }

    /// @brief Call `f(pos)` over just the set bits in the bit-vector in decreasing order.
    /// @param f is a function that takes the the position of the set bit as its argument.
    constexpr void reverse_if_set_call(std::invocable<std::size_t> auto f) const
    {
        std::size_t pos = last();
        while (pos != npos) {
            f(pos);
            pos = prev(pos);
        }
    }

    /// @brief Returns a std::vector that lists in order all the set indices in this bit-vector
    std::vector<std::size_t> set_indices() const
    {
        // Create a vector of indices that is the correct size.
        std::vector<std::size_t> retval(count());

        std::size_t i = 0;
        std::size_t pos = first();
        while (pos != npos) {
            retval[i] = pos;
            pos = next(pos);
            ++i;
        }

        return retval;
    }

    /// @brief Returns a std::vector that lists in order all the unset indices in this bit-vector
    std::vector<std::size_t> unset_indices() const
    {
        // Bit of a cheat for now at least ...
        auto tmp = *this;
        return tmp.flip().set_indices();
    }

    /// @brief Check for equality between two bit-vectors
    constexpr bool friend operator==(const Vector &lhs, const Vector &rhs)
    {
        if (&lhs != &rhs) {
            if (lhs.m_size != rhs.m_size) return false;
            for (std::size_t i = 0; i < lhs.m_blocks.size(); ++i)
                if (lhs.m_blocks[i] != rhs.m_blocks[i]) return false;
        }
        return true;
    }

    /// @brief A little debug utility to dump a whole bunch of descriptive data about a bit-vector to a stream
    constexpr void description(std::ostream &s, const std::string &header = "", const std::string &footer = "\n") const
    {
        if (!header.empty()) s << header << "::\n";
        s << "Vector: " << to_string() << "\n";
        s << "As Hex String:      " << to_hex() << "\n";
        s << "Number of Bits:     " << size() << "\n";
        s << "Number of Set Bits: " << count() << "\n";
        s << "Bit Capacity:       " << capacity() << "\n";
        s << "Unused Capacity:    " << unused() << "\n";

        if (!empty()) {
            s << "Bits per Block:     " << std::numeric_limits<Block>::digits << "\n";
            s << "Blocks Used:        " << m_blocks.size() << "\n";
            s << "Blocks Capacity:    " << m_blocks.capacity() << "\n";
        }
        s << footer;
    }

    /// @brief A little debug utility to dump a whole bunch of descriptive data about a bit-vector to `std::cout`
    constexpr void description(const std::string &header = "", const std::string &footer = "\n") const
    {
        description(std::cout, header, footer);
    }

private:
    /// @brief The number of bits in this bit-vector
    std::size_t m_size = 0;

    /// @brief The bits are stored in a vector of blocks of the given type
    std::vector<Block, Allocator> m_blocks;

    /// @brief A word of the given tye with all bits set to 1
    template<std::unsigned_integral T = Block>
    static constexpr T ones()
    {
        return std::numeric_limits<T>::max();
    }

    /// @brief How many words of the given type are needed to data the passed number of bits?
    template<std::unsigned_integral T = Block>
    static constexpr std::size_t words_needed(std::size_t nBits)
    {
        return (std::numeric_limits<T>::digits + nBits - 1) / std::numeric_limits<T>::digits;
    }

    /// @brief Number of @e bits of storage per word.
    static constexpr std::size_t bits_per_block = std::numeric_limits<Block>::digits;

    /// @brief Returns the number of blocks needed to data the given number of bits.
    static constexpr std::size_t blocks_needed(std::size_t nBits) { return words_needed(nBits); }

    /// @brief Returns the index of the word containing a target bit element.
    static constexpr std::size_t block_index(std::size_t bitPos) { return bitPos / bits_per_block; }

    /// @brief Returns the position of a target bit inside the word that contains it
    static constexpr std::size_t bit_index(std::size_t bitPos) { return bitPos % bits_per_block; }

    /// @brief Returns a bit-mask that isolates a single target bit element in the word that contains it
    static constexpr Block block_mask(std::size_t bitPos) { return Block(Block{1} << bit_index(bitPos)); }

    /// @brief Returns a read-write reference to the word containing a target bit element.
    constexpr Block &block_ref(std::size_t bitPos) { return m_blocks[block_index(bitPos)]; }

    /// @brief Returns a read-only reference to the word containing a target bit element.
    constexpr const Block &block_ref(std::size_t bitPos) const { return m_blocks[block_index(bitPos)]; }

    /// @brief Returns the index of the word where the next pushed bit will reside.
    constexpr std::size_t next_index() const { return block_index(m_size); }

    /// @brief Returns the location where the next pushed bit will be found inside its containing word.
    constexpr std::size_t next_bit() const { return bit_index(m_size); }

    /// @brief Returns true if the block at the passed index is full
    constexpr bool block_is_full(std::size_t i) { return (i + 1) * bits_per_block <= size(); }

    /// @brief Reset any excess bits in the last occupied word to 0.
    /// @note Words beyond the last occupied one are never contaminated by any of our methods above.
    constexpr void clean()
    {
        // NOTE: This works fine even if size() == 0
        std::size_t shift = m_size % bits_per_block;
        if (shift != 0) m_blocks[m_blocks.size() - 1] &= Block(~(ones() << shift));
    }

    /// @brief Returns the index for the least significant set bit in the argument or `npos` if none set.
    /// @tparam Src The (deduced) type of the argument which must be an unsigned integral of some sort.
    template<std::unsigned_integral Src>
    std::size_t static constexpr lsb(Src src)
    {
        if (src == 0) return npos;
        return static_cast<std::size_t>(std::countr_zero(src));
    }

    /// @brief Returns the index for the most significant set bit in the argument or `npos` if none set.
    /// @tparam Src The (deduced) type of the argument which must be an unsigned integral of some sort.
    template<std::unsigned_integral Src>
    std::size_t static constexpr msb(Src src)
    {
        if (src == 0) return npos;
        return static_cast<std::size_t>(std::numeric_limits<Src>::digits - std::countl_zero(src) - 1);
    }

    /// @brief Append bits encoded a binary string (all 0's & 1's) to this bit-vector
    /// @param src The source string which optionally can start with "0b", or "0B"
    /// @param bit_order If true (default is false) the string will have the lowest bit on its right.
    /// @throw This method throws a `std::invalid_argument` exception if the string is not recognized.
    Vector &append_binary_string(std::string_view src, bool bit_order = false)
    {
        // Copy the input string and erase any "0b" or "0B" prefix
        std::string s{src};
        if (s.length() > 1 && (s.starts_with("0B") || s.starts_with("0b"))) s.erase(0, 2);

        // String should now only contain "binary-digits" -- if not we throw an exception
        if (s.find_first_not_of("01") != std::string::npos) throw std::invalid_argument("Source string isn't binary!");

        // Have a valid binary string -- if necessary, reorder it from the least to the most significant bit
        if (bit_order) std::reverse(s.begin(), s.end());

        // Reserve the appropriate space up front
        reserve(size() + s.length());

        // Turn each character into the corresponding bool and push those in to our vector
        for (auto c : s) push(c == '1' ? true : false);

        return *this;
    }

    /// @brief Append bits encoded as a hex-string (e.g. "F27AD3" or "0xF2AD3" or "0xF2AD3_4")
    /// @param src The source string which optionally can be prefixed by a "0x" or "0X". There can also be a suffix "_x"
    /// where x is one of 2, 4, or 8. If present, the suffix is the alternate base for the last character. So 0x1 will
    /// get translated as 0001 (the default base is 16), 0x0_8 will give 001, 0x0_4 will give 01 and 0x0_1 will be 1.
    /// @note Any hex char translates to exactly 4 bits. This means that without a suffix on the last character, the
    /// vector size will always be a multiple of 4. By using a suffix you can encode arbitrary sized vectors.
    /// @throw This method throws a `std::invalid_argument` exception if the string is not recognized.
    Vector &append_hex_string(std::string_view src)
    {
        // Copy the input string and convert it to upper-case
        std::string s{src};
        std::transform(s.begin(), s.end(), s.begin(), [](int c) { return static_cast<char>(::toupper(c)); });

        // String might start with a redundant ""0X" which we remove
        if (s.length() > 1 && s.starts_with("0X")) s.erase(0, 2);

        // Anything left?
        auto len = s.length();
        if (len == 0) return *this;

        // The default base for the last character is 16 just like all the others.
        int last_char_base = 16;

        // Perhaps the string has a suffix (one of "_2", "_4", "_8", or "_X")
        if (len > 2 && s[len - 2] == '_') {
            switch (s[len - 1]) {
                case '2': last_char_base = 2; break;
                case '4': last_char_base = 4; break;
                case '8': last_char_base = 8; break;
                case 'X': last_char_base = 16; break;
                default: throw std::invalid_argument("Unrecognized base for last character in hex string!");
            }

            // We've captured the base of the last character so can now remove the suffix
            s.erase(len - 2, 2);
            len -= 2;
        }

        // String should now only contain "hex-digits" -- if not we throw an error
        if (s.find_first_not_of("0123456789ABCDEF") != std::string::npos)
            throw std::invalid_argument("Source string isn't hex!");

        // Reserve the space the bit-vector needs up front.
        std::size_t num_bits = 4 * len;
        if (last_char_base == 8) num_bits -= 1;
        if (last_char_base == 4) num_bits -= 2;
        if (last_char_base == 2) num_bits -= 3;
        reserve(size() + num_bits);

        // Tables of hex digit bit patterns as std::bitset<4>'s, octal's as std::bitset<3>'s, etc.
        static const std::bitset<4> hex_bs[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        static const std::bitset<3> octal_bs[] = {0, 1, 2, 3, 4, 5, 6, 7};
        static const std::bitset<2> base4_bs[] = {0, 1, 2, 3};
        static const std::bitset<1> binary_bs[] = {0, 1};

        // A lambda that turns any single hex character into an int
        auto hex_to_int = [](char c) { return (c >= 'A') ? (c - 'A' + 10) : (c - '0'); };

        // All but the last character in the string is hex for sure so grab each one, turn it into an index
        // and then append the corresponding hex bits. (Also note we know that len != 0)
        for (std::size_t i = 0; i < len - 1; ++i) append(hex_bs[hex_to_int(s[i])]);

        // Now handle the last character which potentially has a different base.
        char c = s[len - 1];
        int  i = hex_to_int(c);
        switch (last_char_base) {
            case 2:
                if (i > 1) throw std::invalid_argument("Last character is not valid in base 2!");
                append(binary_bs[i]);
                break;

            case 4:
                if (i > 3) throw std::invalid_argument("Last character is not valid in base 4!");
                append(base4_bs[i]);
                break;

            case 8:
                if (i > 7) throw std::invalid_argument("Last character is not valid in base 8!");
                append(octal_bs[i]);
                break;

            default: append(hex_bs[i]); break;
        }

        return *this;
    }

    /// @brief All the Vector instantiations are friends to each other no matter what the block type is
    template<std::unsigned_integral BlockType, typename AllocType>
    friend class Vector;
};

// --------------------------------------------------------------------------------------------------------------------
// NON-MEMBER FUNCTIONS ...
// --------------------------------------------------------------------------------------------------------------------

/// @brief Element by element AND of two equal sized bit-vectors
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator&(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.size() == rhs.size(), "sizes don't match " << lhs.size() << " != " << rhs.size());
    Vector<Block, Allocator> retval{lhs};
    retval &= rhs;
    return retval;
}

/// @brief Element by element XOR of two equal sized bit-vectors
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator^(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.size() == rhs.size(), "sizes don't match " << lhs.size() << " != " << rhs.size());
    Vector<Block, Allocator> retval{lhs};
    retval ^= rhs;
    return retval;
}

/// @brief Element by element OR of two equal sized bit-vectors
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator|(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.size() == rhs.size(), "sizes don't match " << lhs.size() << " != " << rhs.size());
    Vector<Block, Allocator> retval{lhs};
    retval |= rhs;
    return retval;
}

/// @brief Element by element "addition" of two equal sized bit-vectors (in GF(2) addition == XOR)
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator+(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    return operator^=(lhs, rhs);
}

/// @brief Element by element "subtraction" of two equal sized bit-vectors (in GF(2) subtraction == XOR)
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator-(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    return operator^=(lhs, rhs);
}

/// @brief Element by element "multiplication" of two equal sized bit-vectors (in GF(2) multiplication == AND)
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
operator*(const Vector<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    return operator&=(lhs, rhs);
}

/// @brief Computes the logical DIFF of two equal sized bit-vectors
/// @returns A bit-vector `w` where `w[i]` is 1 if `u[i] != v[i]` and 0 otherwise.
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
diff(const Vector<Block, Allocator> &u, const Vector<Block, Allocator> &v)
{
    gf2_debug_assert(u.size() == v.size(), "sizes don't match " << u.size() << " != " << v.size());

    Vector<Block, Allocator> w{u};
    auto                    &w_blocks = w.blocks();
    auto                    &v_blocks = v.blocks();
    for (std::size_t i = 0; i < w_blocks.size(); ++i) w_blocks[i] &= ~v_blocks[i];
    return w;
}

/// @brief Joins two bit-vectors (can be any size) to create a new longer one.
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
join(const Vector<Block, Allocator> &u, const Vector<Block, Allocator> &v)
{
    Vector<Block, Allocator> w{u};
    w.append(v);
    return w;
}

/// @brief Compute the dot product two bit-vectors using `&` for multiplication and `^` for addition
template<std::unsigned_integral Block, typename Allocator>
constexpr bool
dot(const Vector<Block, Allocator> &u, const Vector<Block, Allocator> &v)
{
    gf2_debug_assert(u.size() == v.size(), "sizes don't match " << u.size() << " != " << v.size());
    return (u & v).parity();
}

/// @brief Returns the convolution of two bit-vectors
/// @note The calculation here is done in a very naive manner! Faster algorithms exist for large bit-vectors,
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
convolution(const Vector<Block, Allocator> &a, const Vector<Block, Allocator> &b)
{
    std::size_t              N1 = a.size();
    std::size_t              N2 = b.size();
    std::size_t              N3 = N1 + N2 - 1;
    Vector<Block, Allocator> c(N3);

    for (std::size_t i = 0; i < N1; ++i) {
        bool ai = a[i];
        if (ai) {
            for (std::size_t j = 0; j < N2; ++j) c[i + j] = c[i + j] ^ (ai & b[j]);
        }
    }
    return c;
}

/// @brief Returns `p(x)` a polynomial over GF(2)
/// @param p The coefficients of the polynomial where p(x) = p_0 + p_1 x + p_2 x^2 + ... p_{n-1} x^{n-1}
/// @param x A boolean which defaults to true (the false case is even more trivial)
template<std::unsigned_integral Block, typename Allocator>
constexpr bool
polynomial_sum(const Vector<Block, Allocator> &p, bool x)
{
    // Handle empty vectors with an exception if we're in a `GF2_DEBUG` scenario
    gf2_debug_assert(!p.empty(), "Calling this method for an empty vector is likely an error!");

    // Handling of empty vectors otherwise is a bit arbitrary but needs must ...
    if (p.empty()) return false;

    return x ? p.parity() : p[0];
}

/// @brief The usual output stream operator for a bit-vector.
template<std::unsigned_integral Block, typename Allocator>
std::ostream &
operator<<(std::ostream &s, const Vector<Block, Allocator> &rhs)
{
    return s << rhs.to_string();
}

/// @brief Create a bit-vector by reading bits encoded as a binary or hex string from a stream.
/// @param s The stream to read from (e.g. might read "00111" or "0b00111" or "0x3DEFA3_2")
/// @param rhs The vector we overwrite with the new bits
/// @throws `std::invalid_argument` if the parse fails
/// @note Implementation uses a string buffer -- could probably do something more direct/efficient
template<std::unsigned_integral Block, typename Allocator>
std::istream &
operator>>(std::istream &s, Vector<Block, Allocator> &rhs)
{
    std::string buffer;
    std::getline(s, buffer);
    rhs.clear();
    rhs.append(buffer);
    return s;
}

} // namespace GF2
