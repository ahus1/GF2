#include <GF2/GF2.h>
int
main()
{
    std::size_t n = 10;
    auto        A = GF2::Matrix<>::random(n);
    auto        b = GF2::Vector<>::random(n);
    auto        x = GF2::solve(A, b);
    if (x) {
        std::cout << "Matrix A, solution x, right hand side b, and as a check A.x:\n";
        GF2::print(A, *x, b, GF2::dot(A, *x));
    }
    else {
        std::cout << "Matrix A with right hand side b has NO solution!:\n";
        GF2::print(A, b);
    }
}