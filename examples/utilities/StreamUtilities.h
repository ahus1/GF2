/// @file stream.h
/// @brief Some generally useful utility functions that act on streams
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include "StringUtilities.h"

#include <iostream>
#include <locale>
#include <string>
#include <vector>

/// @brief Force a stream to insert commas into large numbers for readability so that 23456.7 is printed as 23,456.7
/// @param strm The stream you want to have this property -- defaults to @c std::cout
/// @param on You can set this to @c false to return the stream to its default behaviour.
inline void
pretty_print_thousands(std::ios_base &strm = std::cout, bool on = true)
{
    // A locale facet that puts the commas in the right spots.
    struct commas_facet : public std::numpunct<char> {
        using numpunct::numpunct;
        ~commas_facet() override = default;
        char        do_thousands_sep() const override { return ','; }
        std::string do_grouping() const override { return "\003"; }
    };

    // We do our own memory management for the commas_facet (set the refs arg to 1).
    static commas_facet commas_facet(1);
    static std::locale  default_locale;
    static std::locale  commas_locale(default_locale, &commas_facet);

    if (on)
        strm.imbue(commas_locale);
    else
        strm.imbue(default_locale);
}

/// @brief Gets a line of data as a string from a stream. Unlike the @c std::getline(...) this version strips trailing
/// comments (starting with a '#' by default). Ignores blank lines and assumes that lines ending with a '\' continue.
/// @param s The input stream to read from.
/// @param line This is where we will put the string we read from @c in
/// @param comment_begin We ignore/strip out comments that start with this character ("#" by default)
/// @todo In the code below we should escape comment delimiters that are special (e.g. '*')
static std::istream &
get_line(std::istream &s, std::string &line, const std::string &comment_begin = "#")
{
    line.clear();
    while (line.empty() && !s.eof()) {
        std::getline(s, line);

        // Remove any trailing comment (starts with the comment_begin)
        if (!comment_begin.empty()) {
            auto begin = line.find_first_of(comment_begin);
            if (begin != std::string::npos) line.erase(begin, line.length() - begin);
        }

        // Remove trailing blanks
        trim(line);

        // Handle continuation lines
        std::size_t n = line.length();
        if (n && line[n - 1] == '\\') {
            // Replace the continuation character with a space
            line[n - 1] = ' ';

            // Trim--we'll add one space back if there is a non-empty continuation.
            trim(line);

            // Recurse ...
            std::string continuation;
            get_line(s, continuation, comment_begin);
            if (!continuation.empty()) {
                line += " ";
                line += continuation;
            }
        }
    }
    return s;
}

/// @brief Counts the number of lines in the input stream. If the comment start string is empty we use @c std::getline
/// to read the lines, otherwise we use our own version @c get_line so comment lines are excluded etc.
inline std::size_t
line_count(std::istream &is, const std::string &comment_begin)
{
    std::size_t retval = 0;
    std::string line;
    if (comment_begin.empty()) {
        while (std::getline(is, line)) ++retval;
    }
    else {
        while (get_line(is, line, comment_begin)) ++retval;
    }
    return retval;
}

/// @brief Rewind an input stream to the start
inline std::istream &
rewind(std::istream &is)
{
    is.clear();
    is.seekg(0, is.beg);
    return is;
}

