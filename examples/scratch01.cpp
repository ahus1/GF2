#include <GF2/GF2.h>
int main()
{
    auto ident = GF2::Matrix<>::identity(8);

    // Little lambda that turns a bool into a string
    auto b2s = [](bool x) { return x ? "YES" : "NO"; };

    std::cout << "Matrix is_zero?      " << b2s(ident.is_zero())      << "\n";
    std::cout << "Matrix is_ones?      " << b2s(ident.is_ones())      << "\n";
    std::cout << "Matrix is_identity?  " << b2s(ident.is_identity())  << "\n";
    std::cout << "Matrix is_symmetric? " << b2s(ident.is_symmetric()) << "\n";
}
