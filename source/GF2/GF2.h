/// @file GF2.h
/// @brief Boolean matrix and vector classes.
///
/// Classes and methods for linear algebra over GF(2) the Galois Field of two elements typically denoted by
/// {0,1} where all arithmetic is performed modulo 2.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include "Danilevsky.h"
#include "GaussSolver.h"
#include "LUDecompose.h"
#include "Matrix.h"
#include "Vector.h"