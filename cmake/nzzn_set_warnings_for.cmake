# CMake module that adds compiler flags on a per-target basis to ensure that at least some
# baseline warnings are issued for poor/sub-standard code.
#
#   Usage: nzzn_set_warnings_for(target)
#
# Of course valid flags vary by compiler. The ones here cover the GNU, clang, and Microsoft 
# offerings and come from various sources on the web.
#
# There are a couple of variables that can be set to alter how picky the compilers should be and
# whether warnings should be treatd as errors (by default we are picky and treat warnings as errors). 

# The next variable controls whether the compiler should perform extra checks (defaults to on).
option(NZZN_WARNINGS_ARE_PICKY "Make the compiler extra picky!" ON)

# The next variable controls whether the compiler should perform treat any warning as an error and barf!
# Defaults to on with the idea that it is best to be forced to be picky from the start.
option(NZZN_WARNING_ARE_ERRORS "Make the compiler barf completely if it encounters any warning!" ON)

# @brief Set some compiler flags to get at least baseline warnings emitted as it compiles a target. 
# If `NZZN_WARNINGS_ARE_PICKY` is set to a truthy value then some stricter extra checks are done.
# If `NZZN_WARNING_ARE_ERRORS` is set then the compiler will stop when it encounters any warning.
# The warnings are enabled on a target by target basis and should not propagate.
function(nzzn_set_warnings_for targetName)

    # Baseline enabled warnings for Microsoft Visual Studio Code
    set(MSVC_WARNINGS
          /w14242 # 'identifier': conversion from 'type1' to 'type1', possible loss of data
          /w14254 # 'operator': conversion from 'type1:field_bits' to 'type2:field_bits', possible loss of data
          /w14263 # 'function': member function does not override any base class virtual member function
          /w14265 # 'classname': class has virtual functions, but destructor is not virtual 
          /w14287 # 'operator': unsigned/negative constant mismatch
          /we4289 # 'variable': loop control variable declared in the for-loop is used outside the for-loop scope
          /w14296 # 'operator': expression is always 'boolean_value'
          /w14311 # 'variable': pointer truncation from 'type1' to 'type2'
          /w14545 # expression before comma evaluates to a function which is missing an argument list
          /w14546 # function call before comma missing argument list
          /w14547 # 'operator': operator before comma has no effect; expected operator with side-effect
          /w14549 # 'operator': operator before comma has no effect; did you intend 'operator'?
          /w14555 # expression has no effect; expected expression with side- effect
          /w14619 # pragma warning: there is no warning number 'number'
          /w14640 # Enable warning on thread un-safe static member initialization
          /w14826 # Conversion from 'type1' to 'type_2' is sign-extended. This may cause unexpected runtime behavior.
          /w14905 # wide string literal cast to 'LPSTR'
          /w14906 # string literal cast to 'LPWSTR'
          /w14928 # illegal copy-initialization; more than one user-defined conversion has been implicitly applied
          /permissive- # standards conformance mode for MSVC compiler.
    )

    # Baseline enabled warnings for clang
    set(CLANG_WARNINGS
          -Wshadow              # warn the user if a variable declaration shadows one from a parent context
          -Wnon-virtual-dtor    # warn the user if a class with virtual functions has a non-virtual destructor. This helps
                                # catch hard to track down memory errors
          -Wold-style-cast      # warn for c-style casts
          -Wcast-align          # warn for potential performance problem casts
          -Woverloaded-virtual  # warn if you overload (not override) a virtual function
          -Wconversion          # warn on type conversions that may lose data
          -Wsign-conversion     # warn on sign conversions
          -Wnull-dereference    # warn if a null dereference is detected
          -Wdouble-promotion    # warn if float is implicit promoted to double
          -Wformat=2            # warn on security issues around functions that format output (ie printf)
    )

    # Up the level of inspection done by the compiler if asked to do so
    if (NZZN_WARNINGS_ARE_PICKY)
        set(CLANG_WARNINGS ${CLANG_WARNINGS} -Wall -Wextra -pedantic)
        set(MSVC_WARNINGS ${MSVC_WARNINGS} /W4)
    endif ()

    # For library development we often have test/sample code that only exercises a small bit of the library
    set(CLANG_WARNINGS ${CLANG_WARNINGS}
        -Wno-unused-function # We will have non-inline functions that aren't used in the example programs
    )

    # Make the compiler treat warnings as errors (i.e. stop) if asked to do so. Forces developer to fix them.
    if(NZZN_WARNING_ARE_ERRORS)
        set(CLANG_WARNINGS ${CLANG_WARNINGS} -Werror)
        set(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
    endif()

    # GNU gcc has much the same interface as clang -- we can just add a few baseline items
    set(GCC_WARNINGS
        ${CLANG_WARNINGS}
        -Wmisleading-indentation    # warn if indentation implies blocks where blocks do not exist
        -Wduplicated-cond           # warn if if / else chain has duplicated conditions
        -Wduplicated-branches       # warn if if / else branches have duplicated code
        -Wlogical-op                # warn about logical operations being used where bitwise were probably wanted
        -Wuseless-cast              # warn if you perform a cast to the same type
    )

    if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        set(targetWarnings ${MSVC_WARNINGS})
    elseif(CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
        set(targetWarnings ${CLANG_WARNINGS})
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(targetWarnings ${GCC_WARNINGS})
    else()
        message(WARNING "No compiler warnings were set for unknown '${CMAKE_CXX_COMPILER_ID}' compiler.")
    endif()

    # Don't generally want the warnings to propagate except for header-only targets
    set(targetType "")
    get_target_property(targetType ${targetName} TYPE)
    if(${targetType} STREQUAL "INTERFACE_LIBRARY")
       target_compile_options(${targetName} INTERFACE ${targetWarnings})
    else()
        target_compile_options(${targetName} PRIVATE ${targetWarnings})
    endif()

endfunction()
