// Navigation for this particular directory of documentation (documentation module).
// Primarily for use by Antora but we have made it navigable outside of that environment.
:pages-dir: pages
ifdef::site-gen-antora[:pages-dir: .]

* xref:{pages-dir}/overview.adoc[Technical Notes]
** xref:{pages-dir}/danilevsky.adoc[Danilevsky's Method]
** xref:{pages-dir}/gf2.adoc[Some General Notes]
