/// @brief Parsing GF2::Vectors from strings and streams
#include "common.h"

int
main()
{
    // Read a string and convert to a GF2::Vector
    while (true) {
        std::string input;
        std::cout << "String input (q to move on)? ";
        std::getline(std::cin, input);
        if (input == "Q" || input == "q") break;

        try {
            GF2::Vector v(input);
            std::cout << "\"" << input << "\"  "
                 << "parsed as " << v << std::endl;
        }
        catch (...) {
            std::cout << "Failed to parse \"" << input << "\" as a GF2::Vector!" << std::endl;
        }
    }

    // Read a GF2::Vector directly from a stream
    while (true) {
        GF2::Vector<> v;
        std::cout << "Stream input (any invalid GF2::Vector to quit)? ";
        try {
            std::cin >> v;
            std::cout << "Parsed as " << v << std::endl;
        }
        catch (...) {
            std::cout << "Couldn't parse that input as a GF2::Vector! Quitting ..." << std::endl;
            break;
        }
    }

    return 0;
}
