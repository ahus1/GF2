/// @file StopWatch.h
/// @brief Header only simple utility class @c StopWatch that can time how long code blocks take to execute.
///
/// Might be used as follows:
/// @code
///      StopWatch sw("Timing for do_something_expensive()");
///      sw.start();
///      do_something_expensive();
///      sw.stop();
///      std::cout << sw << std::endl;
/// @endcode
///
/// Times are output as doubles and the units are micro-seconds.
/// Stopwatches can also handle the cumulative time spent between all the @c start() and @c stop() calls. For example:
/// @code
///     StopWatch sw("Timing for do_something_expensive()");
///     while(...)
///     {
///         sw.start()
///         do_something_expensive();
///         sw.stop();
///         doSomethingElse();
///     }
///     std::cout << sw << std::endl;
/// @endcode
/// When you print the @c StopWatch you get the last interval time (last duration between @c start() and @c stop()) and
/// the cumulative time of all the intervals.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

class StopWatch {
public:
    /// @brief Underlying clock should be @e steady. While @c <chrono> may provide several we use the default one!
    using clock = std::chrono::steady_clock;

    /// @brief A StopWatch can have a name to distinguish it from others you may have running
    explicit StopWatch(const std::string &str = "") : m_name(str) { m_start = clock::now(); }

    /// @brief Read access to the name
    std::string name() const { return m_name; }

    /// @brief Read-write access to the name
    std::string &name() { return m_name; }

    /// @brief Start the StopWatch
    void start() { m_start = clock::now(); }

    /// @brief Get the time in seconds that have elapsed since the StopWatch started
    double elapsed() const
    {
        std::chrono::duration<double> dt = clock::now() - m_start;
        return dt.count();
    }

    /// @brief  Stop the clock and capture the time interval since @c start() was called.
    void stop()
    {
        m_interval = elapsed();
        m_cumulative += m_interval;
    }

    /// @brief Get the measured time in seconds between the last calls to stop() and start()
    double interval() const { return m_interval; }

    /// @brief Get the measured time in seconds between the last calls to stop() and start() Synonym for @c interval()
    double operator()() const { return interval(); }

    /// @brief Reset the cumulative time counter.
    void reset() { m_cumulative = 0; }

    /// @brief Cumulative time in seconds  since the  start of play or perhaps since the last call to @c reset()
    double cumulative() const { return m_cumulative; }

    /// @brief Write the StopWatch data to a string
    std::string to_string() const
    {
        std::ostringstream ss;
        if (!m_name.empty()) ss << m_name << ": ";
        ss << std::fixed << std::setprecision(1);
        ss << 1000 * m_interval << "ms";
        if (m_cumulative > m_interval) ss << " (cumulatively " << 1000 * m_cumulative << "ms)";
        return ss.str();
    }

private:
    clock::time_point m_start;
    double            m_interval = 0;
    double            m_cumulative = 0;
    std::string       m_name;
};

/// @brief The usual output stream operator
inline std::ostream &
operator<<(std::ostream &os, const StopWatch &rhs)
{
    return os << rhs.to_string();
}
