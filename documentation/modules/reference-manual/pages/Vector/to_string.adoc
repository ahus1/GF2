include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Vector` -- Encode a Bit-Vector as a String.

Methods to encode a bit-vector as a string in a binary or hex format.

[source,cpp]
----
std::string to_string(bool bit_order = false, char off = '0', char on = '1') const; // <.>
std::string to_bit_order(char off = '0', char on = '1') const;                      // <.>
std::string to_hex() const;                                                         // <.>
----
<.> Get a binary-string representation for the bit-vector using the given characters for set and unset elements.
<.> Get a binary-string representation for the bit-vector in _bit-order_ using the given characters for set and unset elements.
<.> Get a hex-string representation for the bit-vector.

[cols="10,90"]
|===
2+h| Parameters

| `bit_order`
| Defaults to `false` but if `true` the bit-vector is encoded with v~0~, the least significant element on the right.
By default for a `GF2::Vector` it is printed on the left. This parameter is only for binary strings.

| `on`, `off`
| The characters to use for set and unset elements in binary encodings. Defaults to the usual 1 and 0. 
Characters for hex-strings are fixed to the standard set of 0123456789ABCDE.
|===

.Hex strings for bit-vectors whose size is _not_ a muliple of 4
include::encoding.adoc[]

.Example: Binary encodings
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Vector v(32, [&](size_t k) { return (k + 1) % 2; });   // <.>
    std::cout << "v: " << v.to_string() << '\n';                // <.>
    std::cout << "v: " << v.to_bit_order() << '\n';             // <.>
    std::cout << "v: " << v.to_bit_order('.', '-') << '\n';     // <.>
}
----
<.> `v` has all the even elements set to 1.
<.> Printing `v` in _vector_order_ using the default 0's and 1's for the element values. v~0~ is on the left.
<.> Printing `v` in _bit_order_ using the default 0's and 1's for the element values. v~0~ is on the right.
<.> Printing `v` in _bit_order_ using dots and dashes for the element values. v~0~ is on the right.

.Output
[source]
----
v: 10101010101010101010101010101010
v: 01010101010101010101010101010101
v: .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
----

.Example: Hex encodings
[source,cpp]
----
#include <GF2/GF2.h>

int main()
{   
    auto v5 = GF2::Vector<>::ones(5);
    auto v6 = GF2::Vector<>::ones(6);
    auto v7 = GF2::Vector<>::ones(7);
    auto v8 = GF2::Vector<>::ones(8);
    auto v9 = GF2::Vector<>::ones(9);
    std::cout << "v5: " << v5.to_string() << "\t hex: " << v5.to_hex() << '\n';
    std::cout << "v6: " << v6.to_string() << "\t hex: " << v6.to_hex() << '\n';
    std::cout << "v7: " << v7.to_string() << "\t hex: " << v7.to_hex() << '\n';
    std::cout << "v8: " << v8.to_string() << "\t hex: " << v8.to_hex() << '\n';
    std::cout << "v9: " << v9.to_string() << "\t hex: " << v9.to_hex() << '\n';
}
----

.Output
[source]
----
v5: 11111        hex: F1_2
v6: 111111       hex: F3_4
v7: 1111111      hex: F7_8
v8: 11111111     hex: FF
v9: 111111111    hex: FF1_2
----

.Example: Reconstituting bit-vectors from hex encodings
[source,cpp]
----
#include <GF2/GF2.h>

int main()
{   
    auto v5 = GF2::Vector<>::random(5);     // <.>
    auto v6 = GF2::Vector<>::random(6);
    auto v7 = GF2::Vector<>::random(7);
    auto v8 = GF2::Vector<>::random(8);
    auto v9 = GF2::Vector<>::random(9);

    GF2::Vector<> u5(v5.to_hex());          // <.>
    GF2::Vector<> u6(v6.to_hex());
    GF2::Vector<> u7(v7.to_hex());
    GF2::Vector<> u8(v8.to_hex());
    GF2::Vector<> u9(v9.to_hex());

    std::cout << "v5 " << v5 << "\t u5 " << u5 << (v5 == u5 ? "\t match " : "\t FAIL") << '\n';
    std::cout << "v6 " << v6 << "\t u6 " << u6 << (v6 == u6 ? "\t match " : "\t FAIL") << '\n';
    std::cout << "v7 " << v7 << "\t u7 " << u7 << (v7 == u7 ? "\t match " : "\t FAIL") << '\n';
    std::cout << "v8 " << v8 << "\t u8 " << u8 << (v8 == u8 ? "\t match " : "\t FAIL") << '\n';
    std::cout << "v9 " << v9 << "\t u9 " << u9 << (v9 == u9 ? "\t match " : "\t FAIL") << '\n';
}
----
<.> Set up some bit-vectors of various lengths with random 50-50 fills.
<.> Convert the bit-vectors to hex-strings and use those to construct bit-vectors.
    Check that the two sets of vectors match.

.Output
[source]
----
v5 01011         u5 01011        match 
v6 000111        u6 000111       match 
v7 1101011       u7 1101011      match 
v8 11010110      u8 11010110     match 
v9 110111001     u9 110111001    match 
----

.See Also
`xref:./stream.adoc[operator<<]` +
`xref:./stream.adoc[operator>>]`