include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Vector` -- Bit Counts

The number of set/unset elements in a bit-vector.

[source,cpp]
----
constexpr std::size_t count()  const;     // <.>
constexpr std::size_t count1() const;     // <.>
constexpr std::size_t count0() const;     // <.>
constexpr bool parity() const;            // <.>
----
<.> Returns the number of elements in the bit-vector that are set to 1.
<.> Returns the number of elements in the bit-vector that are set to 1 (synonym for `count()`)
<.> Returns the number of elements in the bit-vector that are unset at 0.
<.> Returns `count() % 2`--the number of set elements mod 2.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Vector<> v1("00000");
    GF2::Vector<> v2("01010");
    GF2::Vector<> v3("11111");
 
    std::cout
        << "Vector\t" << "count1\t" << "count0\t" << "parity\n"
        << v1 << '\t' << v1.count1() << '\t' << v1.count0() << '\t' << v1.parity() << '\n'
        << v2 << '\t' << v2.count1() << '\t' << v2.count0() << '\t' << v2.parity() << '\n'
        << v3 << '\t' << v3.count1() << '\t' << v3.count0() << '\t' << v3.parity() << '\n';
}
----

.Output
[source]
----
Vector  count1  count0  parity
00000   0       5       0
01010   2       3       0
11111   5       0       1
----

.See Also
`xref:./size.adoc[size]`
