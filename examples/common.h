/// @file common.h
/// @brief This just includes most headers that are ever needed and also exposes the common namespaces.
/// It is a common include for all the example programs in this directory.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)

#include "GF2/GF2.h"
#include "utilities/StopWatch.h"
#include "utilities/StreamUtilities.h"
#include "utilities/StringUtilities.h"
#include "utilities/VectorUtilities.h"

#include <iostream>
