include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Matrix` -- Alter All Elements at Once

Methods to set elements in a bit-matrix to 1, reset them to 0, or flip them from 0 to 1 and vice versa.
These methods can work on the entire bit-matrix, on individual elements, or on diagonal elements.

.The entire bit-matrix
[source,cpp]
----
constexpr Matrix &set();
constexpr Matrix &reset();
constexpr Matrix &flip();
----
Sets, resets, or flips the values of all the elements in the bit-matrix. 

.Individual elements in the bit-matrix
[source,cpp]
----
constexpr Matrix &set(std::size_t i, std::size_t j);
constexpr Matrix &reset(std::size_t i, std::size_t j);
constexpr Matrix &flip(std::size_t i, std::size_t j);
----
Sets, resets, or flips the value of the element at the index pair `(i, j)`.

.The diagonal of a *square* bit-matrix
[source,cpp]
----
constexpr Matrix &set_diagonal(int d = 0);
constexpr Matrix &reset_diagonal(int d = 0);
constexpr Matrix &flip_diagonal(int d = 0);
----
Sets, resets, or flips the value of the elements on a diagonal. +
By default the elements are on the main diagonal.
If `d > 0` we pick a super-diagonal instead, while if `d < 0` we pick the elements from a sub-diagonal.

.Returns
All these methods return a reference to `*this` so can be chained with other calls.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
    GF2::Matrix<> m(4); 
    std::cout << "Original:\n"            << m                    << '\n';
    std::cout << "set:\n"                 << m.set()              << '\n';
    std::cout << "reset:\n"               << m.reset()            << '\n';
    std::cout << "flip:\n"                << m.flip()             << '\n';
    std::cout << "reset_diagonal():\n"    << m.reset_diagonal()   << '\n';
    std::cout << "reset_diagonal(1):\n"   << m.reset_diagonal(1)  << '\n';
    std::cout << "flip_diagonal(-1):\n"   << m.flip_diagonal(-1)  << '\n';
}
----

.Output
[source]
----
Original:
0000
0000
0000
0000
set:
1111
1111
1111
1111
reset:
0000
0000
0000
0000
flip:
1111
1111
1111
1111
reset_diagonal():
0111
1011
1101
1110
reset_diagonal(1):
0011
1001
1100
1110
flip_diagonal(-1):
0011
0001
1000
1100
----

.See Also
`xref:./set_if.adoc[set_if]` +
`xref:./set_if.adoc[flip_if]`