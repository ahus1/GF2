#include "common.h"

int
main()
{
    // Number of trials
    std::size_t trials = 32;

    // Each trial will run on a bit-matrix of this size
    std::size_t N = 16;

    // Number of non-singular matrices
    std::size_t singular = 0;

    // Start the trials ...
    for (std::size_t n = 0; n < trials; ++n) {

        // Create a random matrix & vector
        auto A = GF2::Matrix<>::random(N);
        auto b = GF2::Vector<>::random(N);

        // LU decompose the matrix & solve A.x = b
        auto LU = GF2::lu_decompose(A);
        if (auto x = LU(b); x) {
            auto Ax = GF2::dot(A, *x);
            std::cout << "x: " << *x << "; ";
            std::cout << "A.x: " << Ax << "; ";
            std::cout << "b: " << b << "; ";
            std::cout << "A.x == b? " << (Ax == b ? "YES" : "NO") << "\n";
        }

        // Count the number of singular matrices we come across
        if (LU.singular()) singular++;
    }

    // Stats ...
    auto p = GF2::Matrix<>::probability_singular(N);
    std::cout << "\n"
              << "Singularity stats ...\n";
    std::cout << "Matrix size: " << N << " x " << N << "\n"
              << "P[singular]: " << 100 * p << "%\n"
              << "Trials:      " << trials << "\n"
              << "Singular:    " << singular << " times\n"
              << "Expected:    " << int(p * double(trials)) << " times\n";
    return 0;
}