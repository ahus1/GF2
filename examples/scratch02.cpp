#include "common.h"

int
main()
{
    // Number of trials
    std::size_t trials = 100;

    // Each trial will run on a bit-matrix of this size
    std::size_t N = 100;

    // Number of non-singular matrices
    std::size_t singular = 0;

    // Start the trials ...
    for (std::size_t n = 0; n < trials; ++n) {

        // Create a random matrix
        auto A = GF2::Matrix<>::random(N);

        // LU decompose the matrix
        GF2::LUDecompose LU(A);

        // Extract L & U as their own bit-matrices & compute their product
        auto U = LU.U();
        auto L = LU.L();
        auto lu = GF2::dot(L, U);

        // Check whether P.A = L.U as it should. Exit early if not!
        LU.permute(A);
        if (A != lu) {
            std::cout << "P.A, L.U:\n";
            print(A, lu);
            std::cout << "THEY DON'T MATCH -- CODING ERROR. EXITING ...\n";
            exit(1);
        }

        // Count the number of singular matrices we come across
        if (LU.singular()) singular++;
    }

    // Stats ...
    auto p = GF2::Matrix<>::probability_singular(N);
    std::cout << "Matrix size: " << N << " x " << N << "\n"
              << "P[singular]: " << 100 * p << "%\n"
              << "trials:      " << trials << "\n"
              << "Singular:    " << singular << " times\n"
              << "Expected:    " << int(p * double(trials)) << " times\n";

    // Solve some systems ...
    for (std::size_t n = 0; n < trials; ++n) {
        auto A = GF2::Matrix<>::random(N);
        auto b = GF2::Vector<>::random(N);

        // LU decompose the matrix
        GF2::LUDecompose LU(A);
        if (auto x = LU(b); x) {
            auto Ax = GF2::dot(A, *x);
            std::cout << "x:   " << *x << "\n";
            std::cout << "A.x: " << Ax << "\n";
            std::cout << "b:   " << b << "\n";
            std::cout << "A.x == b: " << b2s(Ax == b) << "\n";
        }
    }

    // Invert some systems ...
    for (std::size_t n = 0; n < trials; ++n) {
        auto A = GF2::Matrix<>::random(N);
        auto LU = GF2::lu_decompose(A);

        // See if we can invert the matrix and if so check A.A_inv == I
        if (auto A_inv = LU.invert(); A_inv) {
            auto I = GF2::dot(A, *A_inv);
            std::cout << "A.A_inv == I? " << b2s(I.is_identity()) << "\n";
        }
    }

    return 0;
}