include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Vector` -- Index Locations

Returns the indices of the set or unset bits in a bit-vector.

[source,cpp]
----
std::vector<std::size_t> set_indices() const;           // <.>
std::vector<std::size_t> unset_indices() const;         // <.>
----
<.> Returns the index locations of the set bits in order.
<.> Returns the index locations of the unset bits in order.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
#include <iterator>
int main()
{   
    auto v = GF2::Vector<>::checker_board(19);                  // <.>
    auto set_indices = v.set_indices();         
    auto unset_indices = v.unset_indices();     

    std::ostream_iterator<std::size_t> iter(std::cout," ");     // <.>

    std::cout << "Bit-vector " << v << " has set indices at locations:\n";
    std::copy (set_indices.begin(), set_indices.end(), iter);
    std::cout << '\n';

    std::cout << "Bit-vector " << v << " has unset indices at locations:\n";
    std::copy (unset_indices.begin(), unset_indices.end(), iter);
    std::cout << '\n';
}
----
<.> Creates a checker board patterned bit-vector of size 19 and then extracts the set & unset index locations.
<.> Use a stream iterator to print the those indices.

.Output
[source]
----
Bit-vector 0101010101010101010 has set indices at locations:
1 3 5 7 9 11 13 15 17 
Bit-vector 0101010101010101010 has unset indices at locations:
0 2 4 6 8 10 12 14 16 18 
----

.See Also
`xref:./if_set_call.adoc[if_set_call]`
