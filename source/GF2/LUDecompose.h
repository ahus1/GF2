/// @file LUDecompose.h
/// @brief LU decomposition object for a square bit-matrix A
///
/// If you have a square bit-matrix A, you might create an LU decomposition of A by
///
///     auto LU = lu_decompose(A);
///
/// Once you have LU you can can use it to solve systems A.x = b for as many right hand sides b as you want.
/// Various queries are supported. These include:
///
///     LU.singular()       -- Is the underlying matrix singular
///     LU.rank()           -- The rank of the underlying matrix
///     LU.L()              -- Get the L in the L.U
///     LU.U()              -- Get the U in the L.U
///     LU.row_swaps()      -- The set of row swap instructions needed to compute P.A (which should be = L.U)
///
/// Note: The row swaps vector might be something like [0,2,2,4,4] which you should read iteratively
///
///     Step 1. Leave row 0 alone
///     Step 2. Swap rows 1 & 2.
///     Step 3. Leave row 2 alone
///     Step 4. Swap rows 3 and 4
///     Step 5. Leave row 4 alone
///
/// Some literature instead uses the concept of a permutation vector which in this example would be [0, 2, 1, 4, 3].
/// The LUDecompose class has a method permutation_vector() that gives back that version.
/// However, the row-swap version is actually easier to use if you want to permute matrix or vector rows in place.
/// It is the version that is used in the venerable LAPACK software for linear algebra.
///
/// The LUDecompose class has several permute() methods to directly apply the row swaps vector.
///
///     LU.permute(Vector &b)  -- Permutes the elements of the vector b in-place
///     LU.permute(Matrix &B)  -- Permutes the rows of the matrix B in-place
///
/// Finally for non-singular matrices we have
///
///     LU.invert()             -- Returns the inverse of A or std::nullopt
///     LU(const Vector &b)     -- Returns a solution vector x or std::nullopt
///     LU(const Vector &B)     -- Returns a solution matrix X or std::nullopt
///
/// In the latter case each column of B is an independent right hand side so A.x = B.col(j) for each j.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include "Matrix.h"

#include <optional>

namespace GF2 {

template<std::unsigned_integral Block, typename Allocator>
class LUDecompose {
public:
    using vector_type = Vector<Block, Allocator>;
    using matrix_type = Matrix<Block, Allocator>;
    using permutation_type = std::vector<std::size_t>;

    explicit LUDecompose(const matrix_type &A) : m_lu(A), m_swap(A.rows()), m_rank(A.rows())
    {
        // Only handle square matrices
        gf2_always_assert(A.is_square(), "Oops, non-square matrix -- dimensions: " << A.rows() << " x " << A.cols());

        // Initialize the row swap instruction vector
        std::size_t N = m_lu.rows();
        for (std::size_t i = 0; i < N; ++i) m_swap[i] = i;

        // Iterate through the bit-matrix.
        for (std::size_t j = 0; j < N; ++j) {

            // Find a non-zero element on or below the diagonal in column -- a pivot.
            std::size_t p = j;
            while (p < N && m_lu(p, j) == 0) ++p;

            // Perhaps no such element exists in this column? If so the matrix is singular!
            if (p == N) {
                m_rank--;
                continue;
            }

            // If necessary, swap the pivot row into place & record the rows swap instruction
            if (p != j) {
                m_lu.swap_rows(p, j);
                m_swap[j] = p;
            }

            // At this point m_lu(i,i) == 1
            for (std::size_t i = j + 1; i < N; ++i) {
                if (m_lu(i, j))
                    for (std::size_t k = j + 1; k < N; ++k) m_lu(i, k) ^= m_lu(j, k);
            }
        }
    }

    /// @brief Read-only access to the rank of the underlying bit-matrix
    constexpr std::size_t rank() const { return m_rank; }

    /// @brief Is the system non-singular? (i.e. was the underlying bit-matrix of full rank?)
    constexpr bool non_singular() const { return m_rank == m_lu.rows(); }

    /// @brief Is the system singular? (i.e. was the underlying bit-matrix no of full rank?)
    constexpr bool singular() const { return !non_singular(); }

    /// @brief Returns the determinant of the underlying matrix
    constexpr bool determinant() const { return non_singular(); }

    /// @brief Read-only access to the LU form of the bit-matrix where A -> [L\U]
    constexpr matrix_type &LU() const { return m_lu; }

    /// @brief Copy of the L (in P.A = L.U) as a square bit-matrix
    constexpr matrix_type L() const { return m_lu.unit_lower(); }

    /// @brief Copy of the U (in P.A = L.U) as a square bit-matrix
    constexpr matrix_type U() const { return m_lu.upper(); }

    /// @brief Read-only access to the row swap instruction vector (the P in P.A = L.U)
    permutation_type row_swaps() const { return m_swap; }

    /// @brief Read-only access to the row swap instruction vector as a permutation vector (the P in P.A = L.U).
    permutation_type permutation_vector() const
    {
        std::size_t      N = m_swap.size();
        permutation_type retval(N);
        for (std::size_t i = 0; i < N; ++i) retval[i] = i;
        for (std::size_t i = 0; i < N; ++i)
            if (m_swap[i] != i) std::swap(retval[m_swap[i]], retval[i]);
        return retval;
    }

    /// @brief Permute the rows of the input matrix `B` in-place using our row-swap instruction vector
    constexpr void permute(matrix_type &B) const
    {
        std::size_t N = m_swap.size();
        gf2_assert(B.rows() == N, "Matrix has " << B.rows() << " rows but row-swap instruction vector has " << N);

        for (std::size_t i = 0; i < N; ++i)
            if (m_swap[i] != i) B.swap_rows(i, m_swap[i]);
    }

    /// @brief Permute the rows of the input vector `b` in-place using our row-swap instruction vector
    constexpr void permute(vector_type &b) const
    {
        std::size_t N = m_swap.size();
        gf2_assert(b.size() == N, "Vector has " << b.size() << " elements but row-swap instruction vector has " << N);

        for (std::size_t i = 0; i < N; ++i)
            if (m_swap[i] != i) b.swap_elements(i, m_swap[i]);
    }

    /// @brief Attempts to solve the system A.x = b. Returns `std::nullopt` if the system is singular
    std::optional<vector_type> operator()(const vector_type &b) const
    {
        auto N = m_lu.rows();
        gf2_assert(b.size() == N, "RHS b has " << b.size() << " elements but LHS matrix has " << N << " rows!");

        // Can we solve this at all?
        if (singular()) return std::nullopt;

        // Make a permuted copy of the the right hand side
        auto x = b;
        permute(x);

        // Forward substitution
        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < i; ++j)
                if (m_lu(i, j)) x[i] ^= x[j];
        }

        // Backward substitution
        for (std::size_t i = N; i--;) {
            for (std::size_t j = i + 1; j < N; ++j)
                if (m_lu(i, j)) x[i] ^= x[j];
        }

        return x;
    }

    /// @brief Attempts to solve the systems A.x = B. Returns `std::nullopt` if the system is singular.
    /// @param B Each column b of B is an independent right hand side for the system A.x = b
    std::optional<matrix_type> operator()(const matrix_type &B) const
    {
        auto N = m_lu.rows();
        gf2_assert(B.rows() == N, "RHS B has " << B.rows() << " rows but LHS matrix has " << N << " rows!");

        // Can we solve this at all?
        if (singular()) return std::nullopt;

        // Make a permuted copy of the the right hand side
        auto X = B;
        permute(X);

        // Solve for each column
        for (std::size_t j = 0; j < N; ++j) {

            // Forward substitution
            for (std::size_t i = 0; i < N; ++i) {
                for (std::size_t k = 0; k < i; ++k)
                    if (m_lu(i, k)) X(i, j) ^= X(k, j);
            }

            // Backward substitution
            for (std::size_t i = N; i--;) {
                for (std::size_t k = i + 1; k < N; ++k)
                    if (m_lu(i, k)) X(i, j) ^= X(k, j);
            }
        }

        return X;
    }

    /// @brief Attempts to invert the bit-matrix A. Returns `std::nullopt` if A is singular
    std::optional<matrix_type> invert() const
    {
        // We just solve the system A.A_inv = I for A_inv
        return operator()(matrix_type::identity(m_lu.rows()));
    }

private:
    matrix_type      m_lu;   // The LU decomposition stored in one bit-matrix.
    permutation_type m_swap; // The list of row swap instructions (a permutation in LAPACK format)
    std::size_t      m_rank; // The rank of the underlying matrix
};

// --------------------------------------------------------------------------------------------------------------------
// NON-MEMBER FUNCTIONS ...
// --------------------------------------------------------------------------------------------------------------------

/// @brief Factory function that returns an LUDecomposition object for a square bit-matrix A
template<std::unsigned_integral Block, typename Allocator>
constexpr LUDecompose<Block, Allocator>
lu_decompose(const Matrix<Block, Allocator> &A)
{
    return LUDecompose(A);
}

} // namespace GF2