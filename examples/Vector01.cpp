/// @brief Exercise some of the basic functionality for the `Vector` class.

/// @note Not interested in speed for this particular example but *do* want bounds checking ...
#ifndef GF2_DEBUG
    #define GF2_DEBUG
#endif
#include "common.h"

int
main()
{
    using Block = uint8_t;

    std::size_t n = 17;
    std::size_t i = 0;

    GF2::Vector<Block> v(n);
    v.description("construction");

    std::cout << "Setting successive bits:\n";
    for (i = 0; i < n; ++i) {
        v[i] = true;
        std::cout << v << "\n";
    }
    std::cout << std::endl;

    std::cout << "Resetting successive bits:\n";
    for (i = 0; i < n; ++i) {
        v[i] = false;
        std::cout << v << "\n";
    }
    std::cout << std::endl;

    std::cout << "Pushing bits:\n";
    for (i = 0; i < n; ++i) {
        v.push(true);
        std::cout << v << "\n";
    }
    v.description("after pushing lots of bits");

    std::cout << "Popping bits:\n";
    for (i = 0; i < n; ++i) {
        v.pop();
        std::cout << v << "\n";
    }
    v.description("after popping lots of bits");

    v.clear();
    v.description("after clear()");

    v.shrink_to_fit();
    v.description("after shrink_to_fit()");

    uint8_t w08 = 1;
    v.append(w08);
    v.description("appended 1 as 8 bits");

    uint16_t w16 = 3;
    v.append(w16);
    v.description("appended 3 as 16 bits");

    uint32_t w32 = 7;
    v.append(w32);
    v.description("appended 7 as 32 bits");

    uint64_t w64 = 15;
    v.append(w64);
    v.description("appended 15 as 64 bits");

    v.clear();
    v.append({1u, 3u, 7u, 15u, 31u});
    v.description("cleared and pushed an initializer list of unsigned's");

    v.set();
    v.description("set all bits");

    v.reset();
    v.description("reset all bits");

    v.flip();
    v.description("flipped all bits");

    std::size_t nRandom = 143;
    auto v00 =GF2::Vector<Block>::random(nRandom, 0.0);
    v00.description("random fill p=0.00");

    auto v25 = GF2::Vector<Block>::random(nRandom, 0.25);
    v25.description("random fill p=0.25");

    auto v50 = GF2::Vector<Block>::random(nRandom);
    v50.description("random fill p=0.50");

    auto v75 = GF2::Vector<Block>::random(nRandom, 0.75);
    v75.description("random fill p=0.75");

    auto v100 = GF2::Vector<Block>::random(nRandom, 1.0);
    v100.description("random fill p=1.00");

    std::cout << "Our 50-50 randomly filled & its flip\n";
    auto v50c = ~v50;
    std::cout << v50 << "\n";
    std::cout << v50c << "\n";
    std::cout << std::endl;

    std::cout << "Our 50-50 randomly filled & its flip as hex-strings\n";
    std::cout << v50.to_hex() << "\n";
    std::cout << v50c.to_hex() << "\n";
    std::cout << std::endl;

    std::cout << "AND'ed:\n";
    std::cout << (v50 & v50c) << "\n";
    std::cout << std::endl;

    std::cout << "OR'ed:\n";
    std::cout << (v50 | v50c) << "\n";
    std::cout << std::endl;

    std::cout << "XOR'ed:\n";
    std::cout << (v50 ^ v50c) << "\n";
    std::cout << std::endl;

    std::cout << "AND'ed:\n";
    std::cout << (v50 & v50c) << "\n";
    std::cout << std::endl;

    std::cout << "OR'ed:\n";
    std::cout << (v50 | v50c) << "\n";
    std::cout << std::endl;

    std::cout << "v50 & v25:\n";
    std::cout << "v50:       " << v50 << "\n";
    std::cout << "v25:       " << v25 << "\n";
    std::cout << "v50 & v25: " << (v50 & v25) << "\n";
    std::cout << std::endl;

    std::cout << "v50 | v25:\n";
    std::cout << "v50:       " << v50 << "\n";
    std::cout << "v25:       " << v25 << "\n";
    std::cout << "v50 | v25: " << (v50 | v25) << "\n";
    std::cout << std::endl;

    std::cout << "v50 ^ v25:\n";
    std::cout << "v50:       " << v50 << "\n";
    std::cout << "v25:       " << v25 << "\n";
    std::cout << "v50 ^ v25: " << (v50 ^ v25) << "\n";
    std::cout << std::endl;

    std::cout << "v50 - v25:\n";
    std::cout << "v50:       " << v50 << "\n";
    std::cout << "v25:       " << v25 << "\n";
    std::cout << "v50 ^ v25: " << (v50 ^ v25) << "\n";
    std::cout << std::endl;

    std::cout << "Left shift (vectors printed here in bit-order!):\n";
    std::cout << "v50:        " << v50.to_bit_order() << "\n";
    std::cout << "v50 << 1:   " << (v50 << 1).to_bit_order() << "\n";
    std::cout << "v50 << 9:   " << (v50 << 9).to_bit_order() << "\n";
    std::cout << "v50 << 19:  " << (v50 << 19).to_bit_order() << "\n";
    std::cout << "v50 << 21:  " << (v50 << 21).to_bit_order() << "\n";
    std::cout << "v50 << 23:  " << (v50 << 23).to_bit_order() << "\n";
    std::cout << "v50 << 259: " << (v50 << 259).to_bit_order() << "\n";
    std::cout << std::endl;

    std::cout << "Right shift (vectors printed here in bit-order!):\n";
    std::cout << "v50:        " << v50.to_bit_order() << "\n";
    std::cout << "v50 >> 1:   " << (v50 >> 1).to_bit_order() << "\n";
    std::cout << "v50 >> 9:   " << (v50 >> 9).to_bit_order() << "\n";
    std::cout << "v50 >> 19:  " << (v50 >> 19).to_bit_order() << "\n";
    std::cout << "v50 >> 21:  " << (v50 >> 21).to_bit_order() << "\n";
    std::cout << "v50 >> 23:  " << (v50 >> 23).to_bit_order() << "\n";
    std::cout << "v50 >> 259: " << (v50 >> 259).to_bit_order() << "\n";
    std::cout << std::endl;

    n = 411;
    GF2::Vector<Block> vPattern(n, [](size_t k) { return (k + 1) % 2; });
    std::cout << "Listing all the set indices in:\n";
    std::cout << vPattern << "\n\n";
    bool none = true;
    auto npos = GF2::Vector<Block>::npos;
    auto pos = vPattern.first();
    while (pos != npos) {
        none = false;
        std::cout << pos << ' ';
        pos = vPattern.next(pos);
    }
    std::cout << "\n";
    if (none) std::cout << "NO SET BITS!";
    std::cout << std::endl;

    std::cout << "Listing all the set indices using a function call instead:\n";
    vPattern.if_set_call([](std::size_t k) { std::cout << k << ' '; });
    std::cout << "\n" << std::endl;

    std::cout << "Listing all the set indices in reverse order:\n";
    pos = vPattern.last();
    while (pos != npos) {
        std::cout << pos << ' ';
        pos = vPattern.prev(pos);
    }
    std::cout << "\n";
    std::cout << std::endl;

    std::cout << "Listing all the set indices using the built-in bit-vector set_indices() method:\n";
    print(vPattern.set_indices());
    std::cout << "\n" << std::endl;

    std::cout << "Listing all the UNset indices using the built-in bit-vector unset_indices() method:\n";
    print(vPattern.unset_indices());
    std::cout << "\n" << std::endl;

    std::cout << "Listing all the set indices in reverse order using a function call instead:\n";
    vPattern.reverse_if_set_call([](std::size_t k) { std::cout << k << ' '; });
    std::cout << "\n" << std::endl;

    std::cout << "Bits to hex and back again (from hex the size may change)...\n";
    auto          s50 = v50.to_hex();
    GF2::Vector<Block> v50h(s50);
    auto          v50hs = v50h.sub(0, v50.size());
    std::string   msg = v50hs == v50 ? "YES" : "*** ERROR ERROR ERROR !!!no match!!! ERROR ERROR ERROR ***";
    std::cout << "v50:              " << v50 << "\n";
    std::cout << "s50:              " << s50 << "\n";
    std::cout << "From hex:         " << v50h << "\n";
    std::cout << "Sub-vector match: " << msg << "\n";
    std::cout << std::endl;

    std::cout << "Bits to binary string and back again ..\n";
    auto          b50 = v50.to_string();
    GF2::Vector<Block> v50b(b50);
    msg = (v50 == v50b) ? "YES" : "*** NO VECTORS DO NOT MATCH! ***";
    std::cout << "v50:       " << v50 << "\n";
    std::cout << "b50:       " << b50 << "\n";
    std::cout << "From bits: " << v50b << "\n";
    std::cout << "Match:     " << msg << "\n";
    std::cout << std::endl;

    // clang-format off
    std::cout << "Extracting first bits as words of various types:\n";
    uint8_t wd8;    v50.to(wd8);
    uint16_t wd16;  v50.to(wd16);
    uint32_t wd32;  v50.to(wd32);
    uint64_t wd64;  v50.to(wd64);
    std::cout << "uint8_t:  " << static_cast<int>(wd8) << "\n";
    std::cout << "uint16_t: " << wd16 << "\n";
    std::cout << "uint32_t: " << wd32 << "\n";
    std::cout << "uint64_t: " << wd64 << "\n";
    std::cout << std::endl;

    std::cout << "Extracting entire Vector as a vector of words of various types:\n";
    std::vector<uint8_t>  v08;  v50.to(v08);
    std::vector<uint16_t> v16;  v50.to(v16);
    std::vector<uint32_t> v32;  v50.to(v32);
    std::vector<uint64_t> v64;  v50.to(v64);
    std::cout << "uint8_t:  ";  print(v08);
    std::cout << "uint16_t: ";  print(v16);
    std::cout << "uint32_t: ";  print(v32);
    std::cout << "uint64_t: ";  print(v64);
    std::cout << std::endl;

    std::cout << "Construction/reconstruction from std::vector<T>:\n";
    GF2::Vector<Block> r08(v08);
    GF2::Vector<Block> r16(v16);
    GF2::Vector<Block> r32(v32);
    GF2::Vector<Block> r64(v64);
    std::cout << "Original vector:            " << v50 << "\n";
    std::cout << "Reconstructed from uint8_t:  " << r08 << "\n";
    std::cout << "Reconstructed from uint16_t: " << r16 << "\n";
    std::cout << "Reconstructed from uint32_t: " << r32 << "\n";
    std::cout << "Reconstructed from uint64_t: " << r64 << "\n";
    std::cout << std::endl;

    v.resize(4); v.set();
    GF2::Vector<Block> u1(3);  
    GF2::Vector<Block> u2(17);  u2.set();
    GF2::Vector<Block> u3(6);
    GF2::Vector<Block> u4(13);  u4.set(); 

    std::cout << "Starting with " << v << "\n";
    v.append(u1); std::cout << "Appended " << u1 << " to get " << v << "\n";
    v.append(u2); std::cout << "Appended " << u2 << " to get " << v << "\n";
    v.append(u3); std::cout << "Appended " << u3 << " to get " << v << "\n";
    v.append(u4); std::cout << "Appended " << u4 << " to get " << v << "\n";
    std::cout << std::endl;
    // clang-format on

    return 0;
}
