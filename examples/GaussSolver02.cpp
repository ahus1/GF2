#include "common.h"

int
main()
{
    std::size_t m = 16;

    auto A = GF2::Matrix<>::random(m);
    auto b = GF2::Vector<>::random(m);
    std::cout << "Solving the system A.x = b for the following A & b:\n";
    print(A, b);

    // Create a solver object for the system
    auto solver = GF2::gauss_solver(A, b);
    std::cout << "The echelon forms of A & b are:\n";
    print(solver.lhs(), solver.rhs());

    // Maybe there were no solutions?
    auto num_solutions = solver.solution_count();
    if (num_solutions == 0) {
        std::cout << "This system is inconsistent and has NO solutions!\n";
        return 0;
    }

    // Print some general information
    std::cout << "Rank of the matrix A:            " << solver.rank() << '\n';
    std::cout << "Number of free variables:        " << solver.free_count() << '\n';
    std::cout << "Free variables at indices:       " << to_string(solver.free_indices()) << '\n';
    std::cout << "Number of solutions to A.x = b:  " << num_solutions << '\n';

    // Iterate through all the solutions we can address & check each one is an actual solution
    for (std::size_t ns = 0; ns < num_solutions; ++ns) {
        auto x = solver(ns);
        auto Ax = GF2::dot(A, x);
        std::cout << "Solution: " << x << " has A.x = " << Ax << " ";
        std::cout << (b == Ax ? "which matches our rhs b." : "which DOES NOT match our rhs b!!!") << "\n";
    }

    return 0;
}
