/// @file Matrix.h
/// @brief  A bit-matrix class.
///
/// These are matrices over GF(2), the Galois field of two elements typically denoted 0 and 1 (or booleans
/// true, false, or bits set, unset). Arithmetic for GF(2) is defined mod 2 which has the effect that
/// addition/subtraction is replaced with `XOR` while multiplication/division becomes `AND`.
///
/// The main aim here is to facilitate efficient linear algebra over GF(2) where the vector class is `GF2::Vector`.
/// This matrix class is implemented as a `std::vector` of `GF2::Vector`'s. If instead, the primary aim was to minimize
/// storage, one would simply store the bit-matrix as a single long `GF2::Vector` with appropriate index operations --
/// however, in that case, matrix operations often need to be done element by element basis which is much, much slower
/// than doing things block by block as we do here.
///
/// This matrix is stored in row-major mode -- each row is a single `GF2::Vector`. This means that it is typically much
/// more efficient to arrange computations to work row by row as opposed to column by column. Most of the time you will
/// be using higher level methods already defined in the class or as free functions where that is already taken into
/// account.
///
/// Like `GF2::Vector` these matrices are sized dynamically at runtime and their row elements are packed into blocks
/// that can be any unsigned integral type -- that template parameter defaults to 64-bit words (it might be reasonable
/// to use a smaller type for small matrices). We allow for non-square `n x m` matrices but obviously some methods only
/// make sense for the case where `n == m`.
///
/// Most of the methods defined for `GF2::Vector` are also implemented for `GF2::Matrix`. There are also many other
/// methods and free functions for performing linear algebra. See the full documentation for details.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nessan.fitzmaurice@me.com)
#pragma once

#include "Vector.h"

#include <optional>
#include <regex>
#include <utility>

namespace GF2 {

/// @brief A class for matrices over GF(2), the space of two elements {0, 1} with arithmetic done mod 2.
/// @tparam Block We pack the elements of the bit-matrix by row into arrays of these (some unsigned integer type).
/// @tparam Allocator is the memory manager that is used to allocate/deallocate space for the store as needed.
template<std::unsigned_integral Block = uint64_t, typename Allocator = std::allocator<Block>>
class Matrix {
public:
    /// @brief Each row of a `GF2::Matrix` is a `GF2::Vector`
    using vector_type = Vector<Block, Allocator>;

    /// @brief Construct an `r x c` bit-matrix. If either parameter is zero the bit-matrix will be 0 x 0.
    constexpr Matrix(std::size_t r, std::size_t c)
    {
        if (r != 0 && c != 0) resize(r, c);
    }

    /// @brief Construct an `n x n` square bit-matrix. Default construction creates an empty 0 x 0 matrix.
    explicit constexpr Matrix(std::size_t n = 0) : Matrix(n, n) {}

    /// @brief Reshape a bit-vector into a bit-matrix with `r` rows (defaults to one row)
    /// @param v @b All the elements of `v` are used to create the bit-matrix
    /// @param r The bit-matrix will have `r` rows so `r` must divide `v.size()` evenly!
    /// By default `r = 1` so the returned matrix has a single row.
    /// If `r == 0` we will return a matrix with a single column instead.
    /// @param by_rows If true (the default) we assume that `v` has the bit-matrix stored by rows (if false, by
    /// columns).
    /// @throw `std::invalid_argument` if `r` does not divide `v.size()` evenly
    constexpr Matrix(const vector_type &v, std::size_t r = 1, bool by_rows = true) : Matrix()
    {
        // Trivial case?
        std::size_t s = v.size();
        if (s == 0) return;

        // We intepret r = 0 as a command to make a one colum bit-matrix (so v.size() rows)
        if (r == 0) r = s;

        // We only allow reshapes that consume all of v
        if (s % r != 0)
            throw std::invalid_argument("GF2::Vector size is not compatible with the requested number of matrix rows!");

        // Number of columns (for sure this is an even division)
        std::size_t c = s / r;
        resize(r, c);

        if (by_rows) {
            for (std::size_t i = 0; i < r; ++i) row(i) = v.sub(i * c, c);
        }
        else {
            std::size_t iv = 0;
            for (std::size_t j = 0; j < c; ++j)
                for (std::size_t i = 0; i < r; ++i) m_array[i][j] = v[iv++];
        }
    }

    /// @brief Construct an `n x m` bit-matrix from the outer product or outer sum of two bit-vectors.
    /// `mat(i,j) = u[i] & v[j]` or `mat(i,j) = u[i] ^ v[j]` depending on whether `product` parameter is true or false.
    /// @param u has n elements `u[i]` for `i = 0, ..., n-1`
    /// @param v has m elements `v[j]` for `j = 0, ..., m-1`
    /// @param product (defaults to true) means use the outer product. False means use the outer sum instead.
    constexpr Matrix(const vector_type &u, const vector_type &v, bool product = true) : Matrix(u.size(), v.size())
    {
        std::size_t r = u.size();
        std::size_t c = v.size();
        if (product) {
            for (std::size_t i = 0; i < r; ++i) {
                bool ui = u[i];
                for (std::size_t j = 0; j < c; ++j) m_array[i][j] = ui & v[j];
            }
        }
        else {
            for (std::size_t i = 0; i < r; ++i) {
                bool ui = u[i];
                for (std::size_t j = 0; j < c; ++j) m_array[i][j] = ui ^ v[j];
            }
        }
    }

    /// @brief Construct an `r x c` bit-matrix by calling `f(i, j)` for each index pair.
    /// @param f If `f(i, j) != 0` the corresponding element will be set to 1, otherwise it will be set to 0.
    explicit constexpr Matrix(std::size_t r, std::size_t c, std::invocable<std::size_t, std::size_t> auto f) :
        Matrix(r, c)
    {
        for (std::size_t i = 0; i < r; ++i)
            for (std::size_t j = 0; j < c; ++j)
                if (f(i, j) != 0) set(i, j);
    }

    /// @brief Construct an n x n square bit-matrix by calling `f(i, j)` for each index pair.
    /// @param f If `f(i, j) != 0` the corresponding element will be set to 1, otherwise it will be set to 0.
    explicit constexpr Matrix(std::size_t n, std::invocable<std::size_t, std::size_t> auto f) : Matrix(n, n, f)
    {
        // Empty body
    }

    /// @brief Construct a bit-matrix from a string (either all 0's & 1's or all hex characters)
    /// @param src The source string which should contain the bit-matrix elements in rows.
    /// The rows can be separated by newlines, white space, commas, or semi-colons.
    /// @see The constructor that creates a bit-vector from a string
    /// @param bit_order If true (default is false) the rows will all have their lowest bit on the right.
    /// This parameter is ignored for hex-strings.
    /// @throw This method throws a `std::invalid_argument` exception if the string is not recognized.
    explicit Matrix(std::string_view s, bool bit_order = false) : Matrix()
    {
        // Trivial case
        if (s.empty()) return;

        // We split the string into tokens using the standard regex library
        std::string                src(s);
        auto                       delims = std::regex(R"([\s|,|;]+)");
        std::sregex_token_iterator iter{src.cbegin(), src.cend(), delims, -1};
        std::vector<std::string>   tokens{iter, {}};

        // Zap any empty tokens & check there is something to do
        tokens.erase(std::remove_if(tokens.begin(), tokens.end(), [](std::string_view x) { return x.empty(); }),
                     tokens.end());
        if (tokens.empty()) return;

        // Iterate through the possible rows
        std::size_t n_rows = tokens.size();
        std::size_t n_cols = 0;
        for (std::size_t i = 0; i < n_rows; ++i) {
            // Convert the current token into a row for the bit-matrix
            vector_type r(tokens[i], bit_order);

            if (i == 0) {
                // First row sets the number of columns
                n_cols = r.size();
                resize(n_rows, n_cols);
            }
            else {
                // Subsequent rows better have the same number of elements!
                if (r.size() != n_cols) throw std::invalid_argument("Inconsistent row sizes for matrix!");
            }
            row(i) = r;
        }
    }

    /// @brief Factory method to construct an `r x c` bit-matrix where the elements are found by independent coin flips.
    /// @param p is the probability of any element being 1
    static Matrix random(std::size_t r, std::size_t c, double p)
    {
        return Matrix(r, c, [p](std::size_t, std::size_t) { return Vector<>::coin_flip(p); });
    }

    /// @brief Construct an `r x c` bit-matrix where the elements are found by independent fair coin flips.
    static Matrix random(std::size_t r, std::size_t c) { return random(r, c, 0.5); }

    /// @brief Construct an `n x n` square bit-matrix where the elements are found by independent fair coin flips.
    static Matrix random(std::size_t n) { return random(n, n, 0.5); }

    /// @brief Factory method to generate an `r x c` bit-matrix with all the elements set to 1.
    static constexpr Matrix ones(std::size_t r, std::size_t c)
    {
        Matrix retval(r, c);
        retval.set();
        return retval;
    }

    /// @brief Factory method to generate an `n x n` square bit-matrix with all the elements set to 1.
    static constexpr Matrix ones(std::size_t n) { return ones(n, n); }

    /// @brief Factory method to generate an `r x c` bit-matrix with all the elements set to 0.
    static constexpr Matrix zeros(std::size_t r, std::size_t c)
    {
        Matrix retval(r, c);
        return retval;
    }

    /// @brief Factory method to generate an `n x n` square bit-matrix with all the elements set to 0
    static constexpr Matrix zeros(std::size_t n) { return zeros(n, n); }

    /// @brief Factory method to generate an `r x c` bit-matrix with a checker-board pattern.
    static constexpr Matrix checker_board(std::size_t r, std::size_t c, int first = 1)
    {
        return first != 0 ? Matrix(r, c, [](size_t i, size_t j) { return (i + j + 1) % 2; })
                          : Matrix(r, c, [](size_t i, size_t j) { return (i + j) % 2; });
    }

    /// @brief Factory method to generate an `n x n` bit-matrix with a checker-board pattern.
    static constexpr Matrix checker_board(std::size_t n, int first = 1) { return checker_board(n, n, first); }

    /// @brief Factory method to generate the identity bit-matrix.
    /// @param n The size of the matrix to generate.
    /// @return An `n x n` bit-matrix with 1's on the diagonal and 0's everywhere else.
    static constexpr Matrix identity(std::size_t n)
    {
        Matrix retval(n);
        retval.set_diagonal();
        return retval;
    }

    /// @brief Factory method to generate the `n x n` shift by `p`-places bit-matrix
    /// @param p Number of places to shift: `p > 0` for right-shift, `p < 0` for left-shift.
    static constexpr Matrix shift(std::size_t n, int p = -1)
    {
        Matrix retval(n);
        retval.set_diagonal(p);
        return retval;
    }

    /// @brief Factory method to generate the `n x n` rotate by `p`-places bit-matrix
    /// @param p Number of places to rotate: `p > 0` for right-rotate, `p < 0` for left-rotate.
    static constexpr Matrix rotate(std::size_t n, int p = -1)
    {
        p %= static_cast<int>(n);
        if (p == 0) return identity(n);

        // Inelegant fix for `implicit conversion changes signedness` compiler warning that is meaningless here.
        auto n_plus_p = std::size_t(int(n) + p);

        Matrix retval(n);
        for (std::size_t i = 0; i < n; ++i) {
            std::size_t j = (n_plus_p + i) % n;
            retval(i, j) = 1;
        }
        return retval;
    }

    /// @brief Factory method to create a "companion" matrix (sub-diagonal all ones, arbitrary top row)
    /// @param top_row These are the elements we copy into the top row of our matrix
    static constexpr Matrix companion(const vector_type &top_row)
    {
        // Trivial case?
        auto n = top_row.size();
        if (n == 0) return Matrix();

        // General case
        Matrix retval(n);
        retval.row(0) = top_row;
        retval.set_diagonal(-1);
        return retval;
    }

    /// @brief Factory method to create a block diagonal bit-matrix
    /// @param blocks A std::vector of the blocks on the diagonal

    /// @brief Returns the number of rows in the bit-matrix
    constexpr std::size_t rows() const { return m_array.size(); }

    /// @brief Returns the number of columns in the bit-matrix
    constexpr std::size_t cols() const { return m_array.size() > 0 ? m_array[0].size() : 0; }

    /// @brief Returns the number of elements in the bit-matrix
    constexpr std::size_t size() const { return rows() * cols(); }

    /// @brief Is this a square bit-matrix? Note that empty bit-matrices are NOT considered square
    constexpr bool is_square() const { return rows() != 0 && rows() == cols(); }

    /// @brief Is this an empty bit-matrix?
    constexpr bool empty() const { return rows() == 0; }

    /// @brief What is the row-capacity of the bit-matrix (i.e. number of rows that be added without a memory alloc)?
    constexpr std::size_t row_capacity() const { return m_array.capacity(); }

    /// @brief What is the column-capacity of the bit-matrix (i.e. number of cols that be added without a memory alloc)?
    constexpr std::size_t col_capacity() const { return m_array.capacity() > 0 ? m_array.data()->capacity() : 0; }

    /// @brief Resize the bit-matrix, initializing any added elements as zeros.
    /// @param r The new number of rows -- if `r  < rows()` we lose the excess rows.
    /// @param c The new number of columns -- if `c  < cols()` we lose the excess columns.
    constexpr Matrix &resize(std::size_t r, std::size_t c)
    {
        // Trivial case ...
        if (rows() == r && cols() == c) return *this;

        // Resizes to zero in either dimension is taken as a zap the lot instruction
        if (r == 0 || c == 0) r = c = 0;

        // Rows, then columns
        m_array.resize(r);
        for (auto &bv : m_array) bv.resize(c);

        return *this;
    }

    /// @brief Resize the bit-matrix to square form, initializing any added values as zeros.
    /// @param n The new number of rows & columns
    constexpr Matrix &resize(std::size_t n) { return resize(n, n); }

    /// @brief Add rows to the bit-matrix at the bottom--defaults to adding a single row
    constexpr Matrix &add_row(std::size_t n = 1) { return resize(rows() + n, cols()); }

    /// @brief Add columns to the bit-matrix at the right--defaults to adding a single column
    constexpr Matrix &add_col(std::size_t n = 1) { return resize(rows(), cols() + n); }

    /// @brief Removes the last row of the bit-matrix
    constexpr Matrix &pop_row()
    {
        if (rows() > 0) m_array.pop_back();
        return *this;
    }

    /// @brief Removes the last column of the bit-matrix
    constexpr Matrix &pop_col()
    {
        if (cols() > 0) {
            for (auto &bv : m_array) bv.pop();
        }
        return *this;
    }

    /// @brief Removes all elements from the bit-matrix (does not change the capacity however!=)
    constexpr Matrix &clear()
    {
        resize(0, 0);
        return *this;
    }

    /// @brief Request to minimize any unused capacity
    constexpr Matrix &shrink_to_fit()
    {
        for (auto &bv : m_array) bv.shrink_to_fit();
        m_array.shrink_to_fit();
        return *this;
    }

    /// @brief Read-only element access
    constexpr bool element(std::size_t i, std::size_t j) const
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        return m_array[i][j];
    }

    /// @brief Read-write element access (returns a `GF2::Vector::reference`)
    constexpr auto element(std::size_t i, std::size_t j)
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        return m_array[i][j];
    }

    /// @brief Read-only element access by index pair
    constexpr bool operator()(std::size_t i, std::size_t j) const { return element(i, j); }

    /// @brief Read-write element access (returns a `GF2::Vector::reference`)
    constexpr auto operator()(std::size_t i, std::size_t j) { return element(i, j); }

    /// @brief Read-only access to the `i`'th row
    constexpr const vector_type &row(std::size_t i) const
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        return m_array[i];
    }

    /// @brief Read-write access to the `i`'th row
    constexpr vector_type &row(std::size_t i)
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        return m_array[i];
    }

    /// @brief Read-only access to the `i`'th row.  Synonym for `row(i)`
    constexpr const vector_type &operator[](std::size_t i) const { return row(i); }

    /// @brief Read-write access to the `i`'th row. Synonym for `row(i)`
    constexpr vector_type &operator[](std::size_t i) { return row(i); }

    /// @brief Read-only access to the `j`'th col
    constexpr vector_type col(std::size_t j) const
    {
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        vector_type retval(rows());
        for (std::size_t i = 0; i < rows(); ++i) retval[i] = m_array[i][j];
        return retval;
    }

    /// @brief Extract the `r x c` sub-matrix starting at `(i0, j0)`.
    /// @return A completely independent new bit-matrix of size `r x c`
    constexpr Matrix sub(std::size_t i0, std::size_t j0, std::size_t r, std::size_t c) const
    {
        // Start point OK?
        gf2_debug_assert(i0 < rows(), "i0 = " << i0 << ", rows() = " << rows());
        gf2_debug_assert(j0 < cols(), "j0 = " << j0 << ", cols() = " << cols());

        // Trivial case?
        if (r == 0 || c == 0) return Matrix{};

        // End point OK?
        gf2_debug_assert(i0 + r - 1 < rows(), "i0 + r - 1 = " << i0 + r - 1 << ", rows() = " << rows());
        gf2_debug_assert(j0 + c - 1 < cols(), "j0 + c - 1 = " << j0 + c - 1 << ", cols() = " << cols());

        Matrix retval(r, c);
        for (std::size_t i = i0; i < i0 + r; ++i) retval.row(i - i0) = m_array[i].sub(j0, c);

        return retval;
    }

    /// @brief Extract the `r x c` sub-matrix starting at `(0, 0)`.
    /// @return A completely independent new bit-matrix of size `r x c`
    constexpr Matrix sub(std::size_t r, std::size_t c) const { return sub(0, 0, r, c); }

    /// @brief Extract the `r x r` sub-matrix starting at `(0, 0)`.
    /// @return A completely independent new bit-matrix of size `r x r`
    constexpr Matrix sub(std::size_t r) const { return sub(0, 0, r, r); }

    /// @brief Extract the lower triangular part of this matrix
    /// @param strict If true we exclude the diagonal -- default is to include it
    /// @return A completely independent new bit-matrix.
    constexpr Matrix lower(bool strict = false) const
    {
        // Trivial case?
        if (empty()) return Matrix();

        // Make a copy of the full matrix
        Matrix retval = *this;

        // Zero out an appropriate portion of each row in that copy
        std::size_t nr = retval.rows();
        std::size_t nc = retval.cols();
        std::size_t di = (strict ? 0 : 1);
        for (std::size_t i = 0; i < nr; ++i) {

            // First element to zero out (may be done already if matrix is rectangular)
            auto first = i + di;
            if (first >= nc) break;
            retval.row(i).reset(first, nc - first);
        }
        return retval;
    }

    /// @brief Extract the strictly lower triangular part of this matrix (excludes the diagonal)
    /// @return A completely independent new bit-matrix.
    constexpr Matrix strictly_lower() const { return lower(true); }

    /// @brief Extract the lower triangular part of this matrix (with ones on the diagonal)
    /// @return A completely independent new bit-matrix.
    constexpr Matrix unit_lower() const
    {
        auto retval = lower(true);
        retval.set_diagonal();
        return retval;
    }

    /// @brief Extract the upper triangular part of this matrix
    /// @param strict If true we exclude the diagonal -- default is to include it
    /// @return A completely independent new bit-matrix.
    constexpr Matrix upper(bool strict = false) const
    {
        // Trivial case?
        if (empty()) return Matrix();

        // Make a copy of the full matrix
        Matrix retval = *this;

        // Zero out an appropriate portion of each row in that copy
        std::size_t nr = retval.rows();
        std::size_t nc = retval.cols();
        std::size_t di = (strict ? 1 : 0);
        for (std::size_t i = 0; i < nr; ++i) {

            // Number of elements to zero out
            auto len = std::min(i + di, nc);
            retval.row(i).reset(0, len);
        }
        return retval;
    }

    /// @brief Extract the strictly upper triangular part of this matrix (excludes the diagonal)
    /// @return A completely independent new bit-matrix.
    constexpr Matrix strictly_upper() const { return upper(true); }

    /// @brief Extract the strictly upper triangular part of this matrix (with ones on the diagonal)
    /// @return A completely independent new bit-matrix.
    constexpr Matrix unit_upper() const
    {
        auto retval = upper(true);
        retval.set_diagonal();
        return retval;
    }

    /// @brief Transpose a square matrix in-place
    /// @note There is a non-member function `transpose(GF2::Matrix` that handles arbitrary rectangular matrices
    constexpr Matrix &to_transpose()
    {
        gf2_assert(is_square(), "Oops, non-square matrix -- dimensions: " << rows() << " x " << cols());
        for (std::size_t i = 0; i < rows(); ++i) {
            for (std::size_t j = i + 1; j < cols(); ++j) {
                bool tmp = m_array[i][j];
                m_array[i][j] = m_array[j][i];
                m_array[j][i] = tmp;
            }
        }
        return *this;
    }

    /// @brief Swap rows `i0` and `i1`
    constexpr Matrix &swap_rows(std::size_t i0, std::size_t i1)
    {
        gf2_debug_assert(i0 < rows(), "i0 = " << i0 << ", rows() = " << rows());
        gf2_debug_assert(i1 < rows(), "i1 = " << i0 << ", rows() = " << rows());
        if (i0 == i1) return *this;
        auto tmp = m_array[i0];
        m_array[i0] = m_array[i1];
        m_array[i1] = tmp;
        return *this;
    }

    /// @brief Swap columns `j0` and `j1`
    constexpr Matrix &swap_cols(std::size_t j0, std::size_t j1)
    {
        gf2_debug_assert(j0 < cols(), "j0 = " << j0 << ", cols() = " << cols());
        gf2_debug_assert(j1 < cols(), "j1 = " << j1 << ", cols() = " << cols());
        if (j0 == j1) return *this;
        for (std::size_t i = 0; i < rows(); ++i) {
            bool tmp = m_array[i][j0];
            m_array[i][j0] = m_array[i][j1];
            m_array[i][j1] = tmp;
        }
        return *this;
    }

    /// @brief Test whether a specific element is set
    constexpr bool test(std::size_t i, std::size_t j) const
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        return m_array[i].test(j);
    }

    /// @brief Test whether all the elements are set
    constexpr bool all() const
    {
        // Handle empty matrices with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty matrix is likely an error!");

        for (const auto &row : m_array)
            if (!row.all()) return false;
        return true;
    }

    /// @brief Test whether any of the elements are set
    constexpr bool any() const
    {
        // Handle empty matrices with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty matrix is likely an error!");

        for (const auto &row : m_array)
            if (row.any()) return true;
        return false;
    }

    /// @brief Test whether none of the elements are set
    constexpr bool none() const
    {
        // Handle empty matrices with an exception if we're in a `GF2_DEBUG` scenario
        gf2_debug_assert(!empty(), "Calling this method for an empty matrix is likely an error!");

        for (const auto &row : m_array)
            if (!row.none()) return false;
        return true;
    }

    /// @brief Returns the number of set elements in the bit-matrix
    constexpr std::size_t count() const
    {
        std::size_t retval = 0;
        for (const auto &row : m_array) retval += row.count();
        return retval;
    }

    /// @brief Compute the number of set elements on the bit-matrix diagonal
    constexpr std::size_t count_diagonal() const
    {
        gf2_debug_assert(is_square(), "Oops, non-square matrix -- dimensions: " << rows() << " x " << cols());
        std::size_t retval = 0;
        for (std::size_t i = 0; i < rows(); ++i)
            if (m_array[i][i]) ++retval;
        return retval;
    }

    /// @brief Compute the trace (the "sum" of the diagonal elements) of this bit-matrix
    constexpr bool trace() const { return count_diagonal() % 2 == 1; }

    /// @brief Is this the  zero matrix?
    constexpr bool is_zero() const { return !any(); }

    /// @brief Is this the all ones matrix?
    constexpr bool is_ones() const { return all(); }

    /// @brief Is this the identity matrix?
    constexpr bool is_identity() const
    {
        if (!is_square()) return false;
        for (std::size_t i = 0; i < rows(); ++i) {
            auto r = row(i);
            r.flip(i);
            if (r.any()) return false;
        }
        return true;
    }

    /// @brief Is this bit-matrix symmetric?
    constexpr bool is_symmetric() const
    {
        if (!is_square()) return false;
        std::size_t N = rows();
        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < i; ++j)
                if (m_array[i][j] != m_array[j][i]) return false;
        }
        return true;
    }

    /// @brief Set the element at a specific bit-matrix location
    constexpr Matrix &set(std::size_t i, std::size_t j)
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        m_array[i].set(j);
        return *this;
    }

    /// @brief Set all the elements in the bit-matrix
    constexpr Matrix &set()
    {
        for (auto &row : m_array) row.set();
        return *this;
    }

    /// @brief Set the elements on a diagonal
    /// @param d `d > 0` for a super-diagonal, `d < 0` for a sub-diagonal, `d == 0` for the main diagonal (default).
    constexpr Matrix &set_diagonal(int d = 0)
    {
        gf2_assert(is_square(), "Oops, non-square matrix -- dimensions: " << rows() << " x " << cols());

        // Method (silently) does nothing if the diagonal is out of range
        if (const auto ad = static_cast<std::size_t>(abs(d)); ad < rows()) {
            std::size_t iLo = d >= 0 ? 0 : ad;
            std::size_t iHi = d >= 0 ? rows() - ad : rows();
            std::size_t j = d >= 0 ? iLo + ad : iLo - ad;
            for (std::size_t i = iLo; i < iHi; ++i) m_array[i][j++] = 1;
        }
        return *this;
    }

    /// @brief Reset the element in a specific bit-matrix location
    constexpr Matrix &reset(std::size_t i, std::size_t j)
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        m_array[i].reset(j);
        return *this;
    }

    /// @brief Reset all the elements in the bit-matrix
    constexpr Matrix &reset()
    {
        for (auto &row : m_array) row.reset();
        return *this;
    }

    /// @brief Reset the elements on a diagonal
    /// @param d `d > 0` for a super-diagonal, `d < 0` for a sub-diagonal, `d == 0` for the main diagonal (default).
    constexpr Matrix &reset_diagonal(int d = 0)
    {
        gf2_assert(is_square(), "Oops, non-square matrix -- dimensions: " << rows() << " x " << cols());

        // Method (silently) does nothing if the diagonal is out of range
        if (const auto ad = static_cast<std::size_t>(abs(d)); ad < rows()) {
            std::size_t iLo = d >= 0 ? 0 : ad;
            std::size_t iHi = d >= 0 ? rows() - ad : rows();
            std::size_t j = d >= 0 ? iLo + ad : iLo - ad;
            for (std::size_t i = iLo; i < iHi; ++i) m_array[i][j++] = 0;
        }
        return *this;
    }

    /// @brief Flip a the element in a specific bit-matrix location
    constexpr Matrix &flip(std::size_t i, std::size_t j)
    {
        gf2_debug_assert(i < rows(), "i = " << i << ", rows() = " << rows());
        gf2_debug_assert(j < cols(), "j = " << j << ", cols() = " << cols());
        m_array[i].flip(j);
        return *this;
    }

    /// @brief Flip all the elements in the bit-matrix
    constexpr Matrix &flip()
    {
        for (auto &row : m_array) row.flip();
        return *this;
    }

    /// @brief Flips the elements on a diagonal
    /// @param d `d > 0` for a super-diagonal, `d < 0` for a sub-diagonal, `d == 0` for the main diagonal (default).
    constexpr Matrix &flip_diagonal(int d = 0)
    {
        gf2_assert(is_square(), "Oops, non-square matrix -- dimensions: " << rows() << " x " << cols());

        // Method (silently) does nothing if the diagonal is out of range
        if (const auto ad = static_cast<std::size_t>(abs(d)); ad < rows()) {
            std::size_t iLo = d >= 0 ? 0 : ad;
            std::size_t iHi = d >= 0 ? rows() - ad : rows();
            std::size_t j = d >= 0 ? iLo + ad : iLo - ad;
            for (std::size_t i = iLo; i < iHi; ++i) m_array[i][j++].flip();
        }
        return *this;
    }

    /// @brief Set element `(i, j)` to 1 if `f(i,j) != 0` otherwise set it to 0.
    /// @param f is function that we expect to call as `f(i, j)` for each index pair in the bit-matrix.
    constexpr Matrix &set_if(std::invocable<std::size_t, std::size_t> auto f)
    {
        reset();
        for (std::size_t i = 0; i < rows(); ++i)
            for (std::size_t j = 0; j < cols(); ++j)
                if (f(i, j) != 0) set(i, j);
        return *this;
    }

    /// @brief Flips the value of element `(i, j)` if `f(i,j) != 0` otherwise leaves it alone.
    /// @param f is function that we expect to call as `f(i, j)` for each index pair in the bit-matrix.
    constexpr Matrix &flip_if(std::invocable<std::size_t, std::size_t> auto f)
    {
        for (std::size_t i = 0; i < rows(); ++i)
            for (std::size_t j = 0; j < cols(); ++j)
                if (f(i, j) != 0) flip(i, j);
        return *this;
    }

    /// @brief Element by element AND'ing
    constexpr Matrix &operator&=(const Matrix &rhs)
    {
        gf2_debug_assert(rhs.rows() == rows(), "rhs.rows() = " << rhs.rows() << ", rows() = " << rows());
        gf2_debug_assert(rhs.cols() == cols(), "rhs.cols() = " << rhs.cols() << ", cols() = " << cols());
        for (std::size_t i = 0; i < rows(); ++i) row(i) &= rhs.row(i);
        return *this;
    }

    /// @brief Element by element OR'ing
    constexpr Matrix &operator|=(const Matrix &rhs)
    {
        gf2_debug_assert(rhs.rows() == rows(), "rhs.rows() = " << rhs.rows() << ", rows() = " << rows());
        gf2_debug_assert(rhs.cols() == cols(), "rhs.cols() = " << rhs.cols() << ", cols() = " << cols());
        for (std::size_t i = 0; i < rows(); ++i) row(i) |= rhs.row(i);
        return *this;
    }

    /// @brief Element by element XOR'ing
    constexpr Matrix &operator^=(const Matrix &rhs)
    {
        gf2_debug_assert(rhs.rows() == rows(), "rhs.rows() = " << rhs.rows() << ", rows() = " << rows());
        gf2_debug_assert(rhs.cols() == cols(), "rhs.cols() = " << rhs.cols() << ", cols() = " << cols());
        for (std::size_t i = 0; i < rows(); ++i) row(i) ^= rhs.row(i);
        return *this;
    }

    /// @brief Element by element addition (in GF(2) addition is just XOR)
    constexpr Matrix &operator+=(const Matrix &rhs) { return operator^=(rhs); }

    /// @brief Element by element subtraction (in GF(2) subtraction is just XOR)
    constexpr Matrix &operator-=(const Matrix &rhs) { return operator^=(rhs); }

    /// @brief Element by element multiplication (in GF(2) multiplication is just AND)
    constexpr Matrix &operator*=(const Matrix &rhs) { return operator&=(rhs); }

    /// @brief Get a copy of this bit-matrix with all the elements flipped
    constexpr Matrix operator~() const
    {
        Matrix retval{*this};
        retval.flip();
        return retval;
    }

    /// @brief Left shift all the rows by `p` places
    constexpr Matrix &operator<<=(std::size_t p)
    {
        for (auto &row : m_array) row <<= p;
        return *this;
    }

    /// @brief Right shift all the rows by `p` places
    constexpr Matrix &operator>>=(std::size_t p)
    {
        for (auto &row : m_array) row >>= p;
        return *this;
    }

    /// @brief Get a copy of this bit-matrix where all the rows are left shifted by `p` places
    constexpr Matrix operator<<(std::size_t p) const
    {
        Matrix retval{*this};
        retval <<= p;
        return retval;
    }

    /// @brief Get a copy of this bit-matrix where all the rows are right shifted by `p` places
    constexpr Matrix operator>>(std::size_t p) const
    {
        Matrix retval{*this};
        retval >>= p;
        return retval;
    }

    /// @brief Augment this matrix in-place by appending a vector as a new column to the right so M -> M|v
    /// @param v The vector must have the same size as there are rows in the matrix.
    constexpr Matrix &append(const Vector<Block, Allocator> &v)
    {
        if (v.size() != rows()) throw std::invalid_argument("Incompatible input vector -- different number of rows!");
        for (std::size_t i = 0; i < rows(); ++i) m_array[i].append(v[i]);
        return *this;
    }

    /// @brief Augment this matrix in-place by appending another matrix as new columns to the right so M -> M|V
    /// @param V The matrix must have the same number of rows as there are rows in the matrix.
    constexpr Matrix &append(const Matrix<Block, Allocator> &V)
    {
        if (V.rows() != rows()) throw std::invalid_argument("Incompatible input matrix -- different number of rows!");
        for (std::size_t i = 0; i < rows(); ++i) m_array[i].append(V.m_array[i]);
        return *this;
    }

    /// @brief Starting at `(i0, j0)` replace our values with those from a passed in replacement bit-matrix `with`
    constexpr Matrix &replace(std::size_t i0, std::size_t j0, const Matrix &with)
    {
        // Start point OK?
        gf2_debug_assert(i0 < rows(), "i0 = " << i0 << ", rows() = " << rows());
        gf2_debug_assert(j0 < cols(), "j0 = " << j0 << ", cols() = " << cols());

        // Size of replacement OK?
        gf2_debug_assert(i0 + with.rows() - 1 < rows(), "i1 = " << i0 + with.rows() - 1 << ", rows() = " << rows());
        gf2_debug_assert(j0 + with.cols() - 1 < cols(), "j1 = " << j0 + with.cols() - 1 << ", cols() = " << cols());

        // Just use the bit-vector replacement mechanism for each effected row ...
        for (std::size_t i = 0; i < with.rows(); ++i) row(i0 + i).replace(j0, with.row(i));

        return *this;
    }

    /// @brief Starting on the diagonal at `(i0, i0)` replace our values with those from a passed in bit-matrix `with`
    constexpr Matrix &replace(std::size_t i0, const Matrix &with) { return replace(i0, i0, with); }

    /// @brief Starting on the diagonal at `(0, 0)` replace our values with those from a passed in bit-matrix `with`
    constexpr Matrix &replace(const Matrix &with) { return replace(0, 0, with); }

    /// @brief Turn this bit-matrix into a bit-vector
    /// @param by_rows If true (the default) then we merge by rows, otherwise we merge by columns
    constexpr auto to_vector(bool by_rows = true) const
    {
        Vector<Block, Allocator> retval;
        retval.reserve(size());
        if (by_rows) {
            for (std::size_t i = 0; i < rows(); ++i) retval.append(row(i));
        }
        else {
            for (std::size_t j = 0; j < cols(); ++j) retval.append(col(j));
        }
        return retval;
    }

    /// @brief Get a bit-string representation for this bit-matrix using the given characters for set and unset
    /// @param bit_order If true then the row-elements are in order ...3210 instead of 0123...
    std::string to_string(std::string_view delim = "\n", bool bit_order = false, char off = '0', char on = '1') const
    {
        // Handle a trivial case ...
        std::size_t r = rows();
        if (r == 0) return std::string{};

        std::size_t c = cols();
        std::string retval;
        retval.reserve(r * (c + delim.length()));

        for (std::size_t i = 0; i < r; ++i) {
            retval += m_array[i].to_string(bit_order, off, on);
            if (i + 1 < r) retval += delim;
        }
        return retval;
    }

    /// @brief Get a binary-string representation for this bit-matrix in bit-order
    std::string to_bit_order(std::string_view delim = "\n", char off = '0', char on = '1') const
    {
        return to_string(delim, true, off, on);
    }

    /// @brief Get a hex-string representation for this bit-matrix.
    std::string to_hex(std::string_view delim = "\n") const
    {
        // Handle a trivial case ...
        std::size_t r = rows();
        if (r == 0) return std::string{};

        // Otherwise we set up the return string with the correct amount of space
        std::size_t c = cols();
        std::string retval;
        retval.reserve(r * (c / 4 + 4));

        for (std::size_t i = 0; i < r; ++i) {
            retval += m_array[i].to_hex();
            if (i + 1 < r) retval += delim;
        }
        return retval;
    }

    /// @brief Probability that an `n x n` bit matrix with elements picked by flipping fair coins, is invertible.
    /// @note For large `n` the value is about 29% and that holds even for n as low as 10.
    static double probability_invertible(std::size_t n)
    {
        // Trivial case
        if (n == 0) return 0;

        double product = 1;
        double pow2 = pow(2.0, -double(n));
        for (std::size_t j = 0; j < n; ++j) {
            product *= (1 - pow2);
            pow2 *= 2.0;
        }
        return product;
    }

    /// @brief Probability that an `n x n` bit matrix with elements picked by flipping fair coins, is singular.
    /// @note For large `n` the value is about 71% and that holds even for n as low as 10.
    static double probability_singular(std::size_t n) { return 1.0 - probability_invertible(n); }

    /// @brief Check for equality between two bit-matrices
    constexpr bool friend operator==(const Matrix &lhs, const Matrix &rhs)
    {
        if (&lhs != &rhs) {
            if (lhs.rows() != rhs.rows()) return false;
            for (std::size_t i = 0; i < lhs.rows(); ++i)
                if (lhs[i] != rhs[i]) return false;
        }
        return true;
    }

    /// @brief Converts this matrix in-place to row-echelon form.
    /// @param pivot_col If present, on return this will have a set bit for every column with a pivot in this matrix.
    Matrix &to_echelon_form(vector_type *pivot_col = 0)
    {
        gf2_always_assert(!empty(), "empty matrix is not supported by this method!");

        // Were we asked to track which columns contain pivots?
        if (pivot_col) {
            pivot_col->resize(cols());
            pivot_col->reset();
        }

        // Working on row number ...
        std::size_t nr = rows();
        std::size_t r = 0;

        // Iterate through the columns
        for (std::size_t j = 0; j < cols(); ++j) {

            // Find a non-zero element on or below the diagonal in this column -- a pivot.
            std::size_t p = r;
            while (p < nr && m_array[p][j] == 0) ++p;

            // Perhaps no such element exists in this column? If so move on to the next one.
            if (p == nr) continue;

            // Getting to here means that column j does have a pivot
            if (pivot_col) pivot_col->element(j) = true;

            // If necessary, swap the pivot row into place
            if (p != r) swap_rows(p, r);

            // Below the working row make sure column j is all zeros by elimination as necessary
            for (std::size_t i = r + 1; i < nr; ++i) {
                if (m_array[i][j] == 1) row(i) ^= row(r);
            }

            // Move on to the next row if there is one
            if (++r == nr) break;
        }
        return *this;
    }

    /// @brief Converts this matrix in place to reduced-row-echelon-form
    /// @param pivot_col If present, on return this will have a set bit for every column with a pivot in this matrix.
    Matrix &to_reduced_echelon_form(vector_type *pivot_col = 0)
    {
        // Start by going to echelon form
        to_echelon_form(pivot_col);

        // Then iterate from the bottom row up, keeping at most one non-zero entry per row.
        for (std::size_t i = rows(); i--;) {

            // Find the pivot column -- i.e. the first set bit in row i & skip the row if it is all zeros
            auto p = row(i).first();
            if (p == Vector<>::npos) continue;

            // Zap everything in column p above row i
            for (std::size_t e = 0; e < i; ++e) {
                if (m_array[e][p] == 1) row(e) ^= row(i);
            }
        }
        return *this;
    }

    /// @brief A little debug utility to dump a whole bunch of descriptive data about a bit-matrix to a stream
    constexpr void description(std::ostream &s, const std::string &header = "", const std::string &footer = "\n") const
    {
        if (!header.empty()) s << header << ":\n";
        s << "Matrix dimension:       " << rows() << " x " << cols() << "\n";
        s << "Matrix capacity:        " << row_capacity() << " x " << col_capacity() << "\n";
        s << "Number of set elements: " << count() << "\n";
        std::size_t r = rows();
        for (std::size_t i = 0; i < r; ++i) {
            s << "    " << m_array[i].to_string() << "  =  0x" << m_array[i].to_hex() << "\n";
        }
        s << footer;
    }

    /// @brief A little debug utility to dump a whole bunch of descriptive data about a bit-matrix to `std::cout`
    constexpr void description(const std::string &header = "", const std::string &footer = "\n") const
    {
        description(std::cout, header, footer);
    }

private:
    /// @brief The matrix data
    std::vector<vector_type> m_array;
};

// --------------------------------------------------------------------------------------------------------------------
// NON-MEMBER FUNCTIONS ...
// --------------------------------------------------------------------------------------------------------------------

/// @brief Matrix-vector multiplication (so if matrix is `r x c` the vector must have `c` elements, a `c x 1`
/// column)
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
dot(const Matrix<Block, Allocator> &lhs, const Vector<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.cols() == rhs.size(), "matrix cols = " << lhs.cols() << ", vector size = " << rhs.size());
    std::size_t              r = lhs.rows();
    Vector<Block, Allocator> retval(r);
    for (std::size_t i = 0; i < r; ++i) retval[i] = GF2::dot(lhs.row(i), rhs);
    return retval;
}

/// @brief Vector-matrix multiplication (if bit-matrix is `r x c` the bit-vector must have `r` elements, a `1 x r`
/// row)
/// @note We store the bit-matrix by rows so `dot(Matrix, Vector)` will generally be faster than this.
template<std::unsigned_integral Block, typename Allocator>
constexpr Vector<Block, Allocator>
dot(const Vector<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.size() == rhs.rows(), "matrix rows = " << rhs.rows() << ", vector size = " << lhs.size());
    std::size_t              c = rhs.cols();
    Vector<Block, Allocator> retval(c);
    for (std::size_t j = 0; j < c; ++j) retval[j] = GF2::dot(lhs, rhs.col(j));
    return retval;
}

/// @brief Matrix-matrix multiplication (bit-matrices must have compatible dimensions)
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
dot(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    gf2_debug_assert(lhs.cols() == rhs.rows(), "lhs.cols() = " << lhs.cols() << ", rhs.rows() = " << rhs.rows());
    std::size_t              r = lhs.rows();
    std::size_t              c = rhs.cols();
    Matrix<Block, Allocator> retval(r, c);
    for (std::size_t j = 0; j < c; ++j) {
        auto rhsCol = rhs.col(j);
        for (std::size_t i = 0; i < r; ++i) retval(i, j) = GF2::dot(lhs.row(i), rhsCol);
    }
    return retval;
}

/// @brief Given a matrix M and a compatible vector v we return the augmented matrix M|v
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
join(const Matrix<Block, Allocator> &M, const Vector<Block, Allocator> &v)
{
    auto retval = M;
    return retval.append(v);
}

/// @brief Given a matrix M and a compatible matrix V we return the augmented matrix M|V
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
join(const Matrix<Block, Allocator> &M, const Matrix<Block, Allocator> &V)
{
    auto retval = M;
    return retval.append(V);
}

/// @brief Returns the transpose of an arbitrary bit-matrix. Does not alter the input matrix.
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
transpose(const Matrix<Block, Allocator> &M)
{
    std::size_t              r = M.rows();
    std::size_t              c = M.cols();
    Matrix<Block, Allocator> retval(c, r);
    for (std::size_t i = 0; i < r; ++i) {
        for (std::size_t j = 0; j < c; ++j) retval(j, i) = M(i, j);
    }
    return retval;
}

/// @brief Raise a square bit-matrix to any power `n`
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
pow(const Matrix<Block, Allocator> &M, std::size_t n)
{
    gf2_assert(M.is_square(), "Oops, non-square matrix -- dimensions: " << M.rows() << " x " << M.cols());

    // Note the M^0 is the identity matrix so we start with that
    Matrix<Block, Allocator> retval = Matrix<Block, Allocator>::identity(M.rows());

    // Make a copy of the input matrix which we will square as needed
    Matrix<Block, Allocator> A{M};

    // And we are off ...
    while (n > 0) {

        // At the odd values of n we just do straight multiply
        if ((n & 1) == 1) retval = dot(retval, A);

        // Are we done?
        n >>= 1;
        if (n == 0) break;

        // Square the input matrix once again
        A = dot(A, A);
    }
    return retval;
}

/// @brief Raise a square bit-matrix to the power `2^n` (e.g. 2^128 which is not representable as an `uint64_t`)
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
pow2(const Matrix<Block, Allocator> &M, std::size_t n)
{
    gf2_assert(M.is_square(), "Oops, non-square matrix -- dimensions: " << M.rows() << " x " << M.cols());

    // Note the 2^0 = 1 so we can start with a straight copy of M
    Matrix<Block, Allocator> retval{M};

    // Square as often as requested ...
    for (uint64_t i = 0; i < n; ++i) retval = dot(retval, retval);

    return retval;
}

/// @brief Element by element matrix-matrix &'ing
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator&(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval &= rhs;
    return retval;
}

/// @brief Element by element matrix-matrix |'ing
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator|(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval |= rhs;
    return retval;
}

/// @brief Element by element matrix-matrix ^'ing
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator^(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval ^= rhs;
    return retval;
}

/// @brief Element by element matrix-matrix addition (in GF(2) addition is just XOR)
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator+(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval ^= rhs;
    return retval;
}

/// @brief Element by element matrix-matrix subtraction (in GF(2) subtraction is just XOR)
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator-(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval ^= rhs;
    return retval;
}

/// @brief Element by element matrix-matrix multiplication (in GF(2) multiplication is just AND)
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
operator*(const Matrix<Block, Allocator> &lhs, const Matrix<Block, Allocator> &rhs)
{
    Matrix<Block, Allocator> retval{lhs};
    retval &= rhs;
    return retval;
}

/// @brief Computes the result of using a square bit-matrix as the argument in a polynomial
/// @param p The polynomial coefficients as a bit-vector where the polynomial sum is p0 + p1 x + p2 x^2 + ...
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
polynomial_sum(const Vector<Block, Allocator> &p, const Matrix<Block, Allocator> &M)
{
    // Matrix must be square
    gf2_assert(M.is_square(), "Oops, non-square matrix -- dimensions: " << M.rows() << " x " << M.cols());

    // Matrix is N x N
    auto N = M.rows();

    // Handle empty vectors with an exception if we're in a `GF2_DEBUG` scenario
    gf2_debug_assert(!p.empty(), "Calling this method for an empty vector is likely an error!");

    // Handling of empty vectors otherwise is a bit arbitrary but needs must ...
    if (p.empty() || p.none()) return Matrix<Block, Allocator>(N);

    // Trivial case where only p[0] is set?
    auto n = p.last();
    if (n == 0) return Matrix<Block, Allocator>::identity(N);

    // Non-trivial case p[n] is set for some n > 0 -- use Horner's method ...
    Matrix<Block, Allocator> retval{M};
    while (n > 0) {

        // Add the identity if previous coefficient is 1
        if (p[n - 1])
            for (std::size_t i = 0; i < N; ++i) retval(i, i) ^= 1;
        retval = dot(M, retval);
        --n;
    }

    return retval;
}

/// @brief Returns the row-echelon form of a bit-matrix. Does not alter the input matrix.
/// @param pivot_col If present, on return this will have a set bit for every column with a pivot in this matrix.
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
echelon_form(const Matrix<Block, Allocator> &M, Vector<Block, Allocator> *pivot_col = 0)
{
    auto A = M;
    return A.to_echelon_form(pivot_col);
}

/// @brief Returns the reduced-row-echelon form of a bit-matrix. Does not alter the input matrix.
/// @param pivot_col If present, on return this will have a set bit for every column with a pivot in this matrix.
template<std::unsigned_integral Block, typename Allocator>
constexpr Matrix<Block, Allocator>
reduced_echelon_form(const Matrix<Block, Allocator> &M, Vector<Block, Allocator> *pivot_col = 0)
{
    auto A = M;
    return A.to_reduced_echelon_form(pivot_col);
}

/// @brief Returns the inverse of a bit-matrix or `std::nullopt` if the matrix is singular.
template<std::unsigned_integral Block, typename Allocator>
std::optional<Matrix<Block, Allocator>>
invert(const Matrix<Block, Allocator> &M)
{
    gf2_assert(M.is_square(), "Oops, non-square matrix -- dimensions: " << M.rows() << " x " << M.cols());

    // Create the augmented matrix M|I
    auto n = M.rows();
    auto A = join(M, Matrix<Block, Allocator>::identity(n));

    // Compute the reduced row echelon form for A
    A.to_reduced_echelon_form();

    // If all went well A is now I|M^(-1)
    if (A.sub(n).is_identity()) return A.sub(0, n, n, n);

    // Apparently things did not go well -- A is not invertible
    return std::nullopt;
}

/// @brief Prints a matrix A and a vector b side-by-side.
/// @param strm  The matrix and vector appear on this stream
/// @param delim The matrix and vector are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(std::ostream &strm, const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b,
      std::string_view delim = "\t")
{
    // If either is empty there was likely a bug somewhere!
    gf2_debug_assert(!A.empty(), "Matrix A is empty which is likely an error!");
    gf2_debug_assert(!b.empty(), "Vector b is empty which is likely an error!");

    // Most rows we ever print
    auto nr = std::max(A.rows(), b.size());

    // Space filler might be needed if the matrix has fewer rows than the vector
    std::string A_fill(A.cols(), ' ');

    // Little lambda that turns a bool into a string
    auto b2s = [](bool x) { return x ? "1" : "0"; };

    // Print away ...
    // clang-format off
    for (std::size_t r = 0; r < nr; ++r) {
        strm << (r < A.rows() ? A.row(r).to_string() : A_fill) << delim 
             << (r < b.size() ? b2s(b[r]) : " ") << "\n";
    }
    // clang-format on
    return;
}

/// @brief Prints a matrix A and a vector b side-by-side to std::cout
/// @param delim The matrix and vector are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b, std::string_view delim = "\t")
{
    print(std::cout, A, b, delim);
}

/// @brief Prints a matrix A, and vectors b & x side-by-side.
/// @param strm  The matrix and vectors appear on this stream
/// @param delim The matrix and vectors are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(std::ostream &strm, const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b,
      const GF2::Vector<Block, Alloc> &x, std::string_view delim = "\t")
{
    // If any are empty there was likely a bug somewhere!
    gf2_debug_assert(!A.empty(), "Matrix A is empty which is likely an error!");
    gf2_debug_assert(!b.empty(), "Vector b is empty which is likely an error!");
    gf2_debug_assert(!x.empty(), "Vector x is empty which is likely an error!");

    // Most rows we ever print
    auto nr = std::max({A.rows(), b.size(), x.size()});

    // Space filler might be needed if the matrix has fewer rows than the vector
    std::string A_fill(A.cols(), ' ');

    // Little lambda that turns a bool into a string
    auto b2s = [](bool z) { return z ? "1" : "0"; };

    // Print away ...
    // clang-format off
    for (std::size_t r = 0; r < nr; ++r) {
        strm << (r < A.rows() ? A.row(r).to_string() : A_fill) << delim 
             << (r < b.size() ? b2s(b[r]) : " ") << delim
             << (r < x.size() ? b2s(x[r]) : " ") << "\n";
    }
    // clang-format on

    return;
}

/// @brief Prints a matrix A and vectors b & x side-by-side to std::cout
/// @param delim The matrix and vectors are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b, const GF2::Vector<Block, Alloc> &x,
      std::string_view delim = "\t")
{
    print(std::cout, A, b, x, delim);
}

/// @brief Prints a matrix A, and three vectors b, x, & y side-by-side.
/// @param strm  The matrix and vectors appear on this stream
/// @param delim The matrix and vectors are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(std::ostream &strm, const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b,
      const GF2::Vector<Block, Alloc> &x, const GF2::Vector<Block, Alloc> &y, std::string_view delim = "\t")
{
    // If any are empty there was likely a bug somewhere!
    gf2_debug_assert(!A.empty(), "Matrix A is empty which is likely an error!");
    gf2_debug_assert(!b.empty(), "Vector b is empty which is likely an error!");
    gf2_debug_assert(!x.empty(), "Vector x is empty which is likely an error!");
    gf2_debug_assert(!y.empty(), "Vector y is empty which is likely an error!");

    // Most rows we ever print
    auto nr = std::max({A.rows(), b.size(), x.size(), y.size()});

    // Space filler might be needed if the matrix has fewer rows than the vector
    std::string A_fill(A.cols(), ' ');

    // Little lambda that turns a bool into a string
    auto b2s = [](bool z) { return z ? "1" : "0"; };

    // Print away ...
    // clang-format off
    for (std::size_t r = 0; r < nr; ++r) {
        strm << (r < A.rows() ? A.row(r).to_string() : A_fill) << delim 
             << (r < b.size() ? b2s(b[r]) : " ") << delim
             << (r < x.size() ? b2s(x[r]) : " ") << delim 
             << (r < y.size() ? b2s(y[r]) : " ") << "\n";
    }
    // clang-format on

    return;
}

/// @brief Prints a matrix A and three vectors b, x, & y side-by-side to std::cout
/// @param delim The matrix and vectors are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(const GF2::Matrix<Block, Alloc> &A, const GF2::Vector<Block, Alloc> &b, const GF2::Vector<Block, Alloc> &x,
      const GF2::Vector<Block, Alloc> &y, std::string_view delim = "\t")
{
    print(std::cout, A, b, x, y, delim);
}

/// @brief Prints two matrices A, B side-by-side.
/// @param strm  The matrices appear on this stream
/// @param delim The matrices are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(std::ostream &strm, const GF2::Matrix<Block, Alloc> &A, const GF2::Matrix<Block, Alloc> &B,
      std::string_view delim = "\t")
{
    // If any matrix is empty there was likely a bug somewhere!
    gf2_debug_assert(!A.empty(), "Matrix A is empty which is likely an error!");
    gf2_debug_assert(!B.empty(), "Matrix B is empty which is likely an error!");

    // Most rows we ever print
    auto nr = std::max(A.rows(), B.rows());

    // Space fillers might be needed if the matrices have different row dimensions
    std::string A_fill(A.cols(), ' ');
    std::string B_fill(B.cols(), ' ');

    // Print away ...
    for (std::size_t r = 0; r < nr; ++r) {
        strm << (r < A.rows() ? A.row(r).to_string() : A_fill) << delim
             << (r < B.rows() ? B.row(r).to_string() : B_fill) << "\n";
    }
    return;
}

/// @brief Prints two matrices A, B side-by-side to std::cout.
/// @param delim The matrices are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(const GF2::Matrix<Block, Alloc> &A, const GF2::Matrix<Block, Alloc> &B, std::string_view delim = "\t")
{
    print(std::cout, A, B, delim);
}

/// @brief Prints three matrices A, B, C side-by-side to std::cout.
/// @param delim The matrices are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(const GF2::Matrix<Block, Alloc> &A, const GF2::Matrix<Block, Alloc> &B, const GF2::Matrix<Block, Alloc> &C,
      std::string_view delim = "\t")
{
    print(std::cout, A, B, C, delim);
}

/// @brief Prints three matrices A, B, C side-by-side.
/// @param strm  The matrices appear on this stream
/// @param delim The matrices are separated by this which defaults to a tab.
template<std::unsigned_integral Block, typename Alloc>
void
print(std::ostream &strm, const GF2::Matrix<Block, Alloc> &A, const GF2::Matrix<Block, Alloc> &B,
      const GF2::Matrix<Block, Alloc> &C, std::string_view delim = "\t")
{
    // If any matrix is empty there was likely a bug somewhere!
    gf2_debug_assert(!A.empty(), "Matrix A is empty which is likely an error!");
    gf2_debug_assert(!B.empty(), "Matrix B is empty which is likely an error!");
    gf2_debug_assert(!C.empty(), "Matrix C is empty which is likely an error!");

    // Most rows we ever print
    auto nr = std::max({A.rows(), B.rows(), C.rows()});

    // Space fillers might be needed if the matrices have different row dimensions
    std::string A_fill(A.cols(), ' ');
    std::string B_fill(B.cols(), ' ');
    std::string C_fill(C.cols(), ' ');

    // Print away ...
    for (std::size_t r = 0; r < nr; ++r) {
        strm << (r < A.rows() ? A.row(r).to_string() : A_fill) << delim
             << (r < B.rows() ? B.row(r).to_string() : B_fill) << delim
             << (r < C.rows() ? C.row(r).to_string() : C_fill) << "\n";
    }
    return;
}

/// @brief Usual output stream operator for a bit-matrix
template<std::unsigned_integral Block, typename Allocator>
std::ostream &
operator<<(std::ostream &s, const Matrix<Block, Allocator> &rhs)
{
    return s << rhs.to_string();
}

/// @brief Attempt to read a bit-matrix from a stream
/// @param s The stream to read from. Matrix should in rows separated by white space, commas, semi-colons.
/// @param rhs If the read/parse succeeds we will overwrite this with the new bit-matrix
template<std::unsigned_integral Block, typename Allocator>
std::istream &
operator>>(std::istream &s, GF2::Matrix<Block, Allocator> &rhs)
{
    std::string buffer;
    std::getline(s, buffer);
    GF2::Matrix<Block, Allocator> mat(buffer);
    rhs = mat;
    return s;
}

} // namespace GF2
