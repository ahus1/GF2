/// @file assert.h
/// @brief Three replacements for the standard `assert(condition)` macro.
///
///     @code gf2_assert(condition, error_message)
///     @code gf2_debug_assert(condition, error_message)
///     @code gf2_always_assert(condition, error_message)
///
/// The first version is always on unless the GF2_NDEBUG flag is defined at compile time.
/// The second version is only on if the GF_DEBUG flag is set at compile time.
/// The third version is always on bi what flags are set at compiler time.
///
/// In all cases (assuming the asserts are on) if `condition` evaluates to `false` an exception is thrown. The
/// exception message includes the location where the failure occurred along with extra dynamic content from the
/// second `error_message`  parameter. That parameter is typically used to print the values of the variables that
/// triggered the failure.
///
/// Here is an example from the `GF2::invert(const Matrix& M)` function:
///
///   @code gf2_assert(M.is_square(), "Oops, non-square matrix -- dimensions: " << M.rows() << " x " << M.cols()!");
///
/// We can only invert square matrices which is checked by that `M.is_square()` call. If that fails an exception is
/// thrown. This particular check is always on by default and you need to do something special (i.e. define the
/// GF2_NDEBUG flag at compile time) to disable it. In this case the cost of the check is very slight compared to the
/// work done by the `invert(...)` method so leaving it on even in production code is probably not a problem.
///
/// On the other hand, here is a pre-condition/requirement from the function `dot(GF2::Vector u, GF2::Vector::v)
///
///   @code gf2_debug_assert(u.size() == v.size(), "Vector sizes don't match " << u.size() << " != " << v.size());
///
/// You can only take the dot product of two equal length vectors which is what is checked by the above assertion. If
/// the requirement is not satisfied an exception is thrown with an informative message that includes the size of the
/// two vectors in question. If that exception is not caught then program execution will halt with that message.
/// The check here is OFF by default and you need to do something special (i.e. define the GF2_DEBUG flag at compile
/// time) to enable it. The idea is that production code may be doing a huge number of these dot products and we do not
/// generally want to pay for th check to be performed. However, we recognize that during development enabling these
/// sort of checks may be very useful indeed.
///
/// So `gf2_debug_assert(...)` is a macro that expands to nothing at all unless the `GF2_DEBUG` flag is set at compile
/// time. On the other hand, `gf2_assert(...)` is a macro that  expands to nothing at all if the `GF2_NDEBUG` flag is
/// set at compile time.
///
/// The decision to one vs. the other is predicated on the cost of doing the check versus the work done in the method in
/// question. A primary use case for `gf2_debug_assert` is to do things like bounds checking on indices -- from
/// experience this is vital during development. However, generally doing bounds checking on every index operation
/// incurs a huge performance penalty and can easily slow down numerical code by orders of magnitude. So it makes sense
/// to have the checks in place for development but to make sure they are never there in release builds.
///
/// @note: We are in macro land here so no namespaces -- instead we use the `gf2_` prefix. Typically macros are given
/// names that are all in caps but the standard `assert` does not follow that custom so neither does these.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice (nzznfitz+gf2@icloud.com)
#pragma once

#include <cstring>
#include <iostream>
#include <sstream>

/// @brief Our macros throw a custom exception -- though for now all that is custom is the name of the exception itself.
namespace GF2 {
class assert_failure : public std::logic_error {
    using std::logic_error::logic_error;
};
} // namespace GF2

/// @brief The assert failure message includes the name of the offending file. The standard @c __FILE__ macro puts out
/// the full path for the filename which is generally an overkill. All we usually want is the name itself.
#define GF2_FILE (std::strrchr(__FILE__, '/') ? std::strrchr(__FILE__, '/') + 1 : __FILE__)

/// @brief The assert macros have a condition to check and an error message to use on failure. We wrap those up with
/// location information to create the full message for our GF2::assert_failure exception.
#define GF2_ASSERT_MESSAGE(condition, error_message)                                                \
    (std::ostringstream{} << "\n"                                                                   \
                          << "ASSERTION FAILURE:\n"                                                 \
                          << "Assertion `" #condition << "` is NOT true: " << error_message << "\n" \
                          << "Failure triggered in function: '" << __func__ << "' "                 \
                          << "[" << GF2_FILE << " line " << __LINE__ << "]")                        \
        .str()

// For sure if GF2_NDEBUG is set the GF2_DEBUG should not be set!
#if defined(GF2_NDEBUG) and defined(GF2_DEBUG)
    #undef GF2_DEBUG
#endif

/// @def `gf2_debug_assert` expands to a no-op *unless* the `GF2_DEBUG` flag is set.
#ifdef GF2_DEBUG
    #define gf2_debug_assert(cond, msg) \
        if (!(cond)) throw GF2::assert_failure(GF2_ASSERT_MESSAGE(cond, msg))
#else
    #define gf2_debug_assert(cond, msg) void(0)
#endif

/// @def `gf2_assert` expands to a no-op *only if* the `GF2_NDEBUG` flag is set.
#ifdef GF2_NDEBUG
    #define gf2_assert(cond, msg) void(0)
#else
    #define gf2_assert(cond, msg) \
        if (!(cond)) throw GF2::assert_failure(GF2_ASSERT_MESSAGE(cond, msg))
#endif

/// @def `gf2_always_assert` *never* becomes an no-op
#define gf2_always_assert(cond, msg) \
    if (!(cond)) throw GF2::assert_failure(GF2_ASSERT_MESSAGE(cond, msg))
