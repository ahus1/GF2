include::attributes-settings.adoc[]
include::attributes-links.adoc[]

= `GF2::Vector` -- Add/Remove ELements

Adds/removes single elements from the end of the bit-vector.

[source,cpp]
----
constexpr Vector &push(bool one = false);    // <.>
constexpr Vector &append(bool);              // <.>
constexpr Vector &pop();                     // <.>
----
<.> Adds a single element to the end of the bit-vector. Element will default to 0 unless `one == true`.
<.> This is a synonym for `push()` and adds a single `bool` to the end of the bit-vector. 
There are several other `xref:./append.adoc[append]` methods so the synonym seems natural.
<.> Removes the last element from the bit-vector & shrinks it if possible. Does nothing if the bit-vector is empty.

These methods both return a reference to `*this` so can be chained with other calls.

.Example
[source,cpp]
----
#include <GF2/GF2.h>
int main()
{   
   GF2::Vector<> v;
   v.push(true);                        // <.>
   std::cout << "v: " << v << '\n';
   v.push();                            // <.>
   std::cout << "v: " << v << '\n';
   v.pop();
   std::cout << "v: " << v << '\n';
   v.pop();
   std::cout << "v: " << v << '\n';
   v.pop();                             // <.>
   std::cout << "v: " << v << '\n';
}
----
<.> Adding a 1 element to the end of the bit-vector.
<.> Adding the default element of 0 to the end of the bit-vector.
<.> Calling `pop()` on an empty bit-vector does nothing.

.Output
[source]
----
v: 1
v: 10
v: 1
v: 
v:     <.>
----
<.> Calling `pop()` on an empty vector does nothing.

.See Also
`xref:./append.adoc[append]` +
`xref:./clear.adoc[clear]`