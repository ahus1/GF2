#include "common.h"
int
main()
{
    auto M1 = GF2::Matrix<>::identity(7);
    auto poly = GF2::characteristic_polynomial(M1);
    poly.description("characteristic_polynomial() returned");
    return 0;
}
